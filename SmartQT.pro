#-------------------------------------------------
#
# Project created by QtCreator 2018-04-26T10:07:54
#
#-------------------------------------------------

QT += core gui
QT += axcontainer
QT += serialport serialbus
QT += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SmartQT

TEMPLATE = app

FORMS += mainwindow.ui

HEADERS += include/mainwindow.h \
        include/publicdatatype.h \
        include/snmpvardefine.h \
        include/snmperrcode.h \
        include/snmpversion.h \
        ui/combox1.h \
        ui/namelabel.h \
        ui/valuetext.h \
        ui/valuetext2.h \
        commmibmapping.h \
        comm/icommTCP.h \
        comm/icommBase.h \
        comm/icommSnmp.h \        
        comm/icommPower.h \
        comm/icommSerial.h \
        parsefile/MgrDataBase.h \
        parsefile/MgrSerialSlaver.h \
        parsefile/MgrPowerDevice.h \        
        parsefile/MgrTcpServer.h \
        parsefile/MgrSnmpServer.h \
        parsefile/MgrMibMapping.h \
    parsefile/mgrasciiserver.h \
    parsefile/MgrModbusASCII.h \
    include/transferstdmib.h

SOURCES += main.cpp \
        mainwindow.cpp \
        ui/namelabel.cpp \
        ui/valuetext.cpp \
        ui/valuetext2.cpp \
        comm/icommSnmp.cpp \
        parsefile/MgrMibMapping.cpp \
        comm/icommPower.cpp \
        parsefile/MgrSnmpServer.cpp \
        parsefile/MgrPowerDevice.cpp \
        parsefile/MgrDataBase.cpp \
        comm/icommSerial.cpp \
        parsefile/MgrSerialSlaver.cpp \
        parsefile/MgrTcpServer.cpp \
        comm/icommTCP.cpp \
        comm/icommBase.cpp \
        ui/combox1.cpp \
    parsefile/MgrModbusASCII.cpp

RESOURCES += img.qrc

RC_FILE += ./img/logo.rc

#QMAKE_CXXFLAGS +=  -Wno-unused-parameter
