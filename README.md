## SmartQT (Smart Quick Test)

A communication simulation software for RDU device driver testing

```Ada
   _________
  /          \                                                                                        
 |   ______   |                                                                                       
 |  |      |__|                                                                                       
 |  |                                                                                                 
 |  \_______                                                                                          
  \         \     __  _      _  __      ____   __   _______    __________     _______     __________  
   \_______  \   |  |/ \    / \|  |    /    \_/ /  |   __  \  |___    ___|   / _____ \   |___    ___| 
           \  |  |   __ \  / __   |   /  __    /   |  |  |  |     |  |      | /     \ |      |  |     
  __       |  |  |  /  \ \/ /  \  |  |  |  |   |   |  |__|_/      |  |      | |     | |      |  |     
 |  \______|  |  | |    |  |    | |  |  |__|   |   |  |\  \       |  |      | |     | |      |  |     
 |            |  | |    |  |    | |   \        \   |  | \  \      |  |      | \_____/\ \     |  |     
  \__________/   |_|    |__|    |_|    \___/\___\  |__|  \__\     |__|       \_______/\_\    |__|     

```

### Instruction

> 原名:   QTtest
>
> 原作者: 丁伟建 (d95886)
>
> 维护者: 刘 航 (l96130) 
>
> SG.Alex (s96766)

### Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!


```Ada
                      +------------------------------------------+                   
                      |                                          |             +----+
+----------+      Set |     +------------+                       |   Socket    |    |
|          |----------+---->|            |  Ctrl  +-----------+  |  (UDP/TCP)  |    |
|  Bowser  |          |     | Mgr Server |<------>| Comm Base |<-+ - - - - - ->| PC |
|          |<---------+-----|            |  Get   +-----------+  |   Serial    |    |
+----------+      Get |     +------------+                       |   (COM)     |    |
                      |                                          |             +----+
                      +------------------------------------------+                   
```


### Directory

| Directory               | Describe                     |
| ----------------------- | ---------------------------- |
| bin                     | 可执行程序(Window环境下运行) |
| comm                    | 通信接口代码                 |
| config                  | 配置文件备份                 |
| doc                     | 操作指导                     |
| img                     | 资源图片                     |
| include                 | 相关头文件代码               |
| parsefile               | 执行管理代码                 |
| ui                      | 控件封装文件                 |
| reference_template_xlsx | 文件示例                     |



### Software-Architecture


           +----------------------------------------------------------------------------------------------+
           |                                                                                              |
           |                            +---> MGR_MIB_MAPPING ---------------+                            |
           |                            |                                    |                            |
           |                            +---> MGR_POWER_DEVICE --------------|-----+                      |
     [VIEW]|            [MODEL]         |                                    |     |                      |
     MAIN_WINDOWS +---> MGR_DATA_BASE --+---> MGR_SERIAL_SLAVER -------------|-----|-----+                |
           |                  |         |                                    |     |     |                |
           |                  |         +---> MGR_SNMP_SERVER ---------------|-----|-----|-----+          |
           |                  |         |                                    |     |     |     |          |
           |                  |         +---> MGR_TCP_SERVER ----------------|-----|-----|-----|-----+    |
           |                  |                                              |     |     |     |     |    |
           +------------------|----------------------------------+           |     |     |     |     |    |
           |                  |                                              |     |     |     |     |    |
           |                  |        +---> NULL----------------------------+     |     |     |     |    |
           |                  |        |                                           |     |     |     |    |
           |                  |        +---> I_COMM_POWER--------------------------+     |     |     |    |
           |                  v        |                                                 |     |     |    |
           |             I_COMM_BASE --+---> I_COMM_SERIAL-------------------------------+     |     |    |
           |             [CONTROL]     |                                                       |     |    |
           |                           +---> I_COMM_SNMP---------------------------------------+     |    |
           |                           |                                                             |    |
           |                           +---> I_COMM_TCP----------------------------------------------+    |
           |                                                                                              |
           +----------------------------------------------------------------------------------------------+


### Packaging Tutorial

- [x] [ 打包教程入口 ](bin/packaging_tutorial_instructions.md)

### Add your files

- [x] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [x] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/vertiv-co/apac/rdu501/smartqt.git
git branch -M main
git push -uf origin main
```

