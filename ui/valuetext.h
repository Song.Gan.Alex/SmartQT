#ifndef VALUETEXT_H
#define VALUETEXT_H

#include <QDoubleSpinBox>

class valuetext : public QDoubleSpinBox
{
    Q_OBJECT
public:
    explicit valuetext(int iItemIndex, int regAddr, float fValue, QDoubleSpinBox *parent = 0);

signals:
    void valueChanged(int, int, float);

public slots:
    void sendSignals(double new_value);

private:
    int     m_iItemIdex; //SheetIndex
    int     m_regAddr;   //RegAddr
    float   m_fValue;    //ChangeValue
};

#endif // VALUETEXT_H
