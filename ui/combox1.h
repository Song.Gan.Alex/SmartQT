#ifndef COMBOX1_H
#define COMBOX1_H

#include <QCheckBox>
#include <QComboBox>

class combox1 : public QComboBox
{
    Q_OBJECT
public:
    explicit combox1(int EquipId, char oid[10], int SigType, int SigId, int Entry, QCheckBox* p_chk, QComboBox *parent = 0);

signals:
    void comboxChanged(int, int, int, int, char*);

public slots:
    void sendSignals(int);

private:
    int     m_EquipId;
    int     m_SigType;
    int     m_SigId;
    int     m_Entry;
    char    m_oid[10];
    QCheckBox *pm_chk;
};

#endif // COMBOX1_H
