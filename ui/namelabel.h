#ifndef NAMELABEL_H
#define NAMELABEL_H

#include <QLabel>

class namelabel : public QLabel
{
    Q_OBJECT
public:
    explicit namelabel(QString name);
    explicit namelabel(QString name, int x, int y);

signals:

public slots:

};

#endif // NAMELABEL_H
