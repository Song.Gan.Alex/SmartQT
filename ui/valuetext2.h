#ifndef VALUETEXT2_H
#define VALUETEXT2_H

#define textmodel 1

#include <QCheckBox>

#if textmodel    
    #include <QSpinBox>
#else
    #include <QTextEdit>
#endif

#if textmodel
    class valuetext2 : public QSpinBox
#else
    class valuetext2 : public QTextEdit
#endif
{
    Q_OBJECT
public:
#if textmodel
    explicit valuetext2(int EquipId, char oid[10], int SigType, int SigId, int Entry, QCheckBox *p_chk, QSpinBox *parent = 0);
#else
    explicit valuetext2(int EquipId, char oid[10], int SigType, int SigId, int Entry, QCheckBox *p_chk, QTextEdit *parent = 0);
#endif

signals:
    void valueChanged2(int, int, int, int, char*);

public slots:
    void sendSignals2(int new_value);

private:
    int     m_EquipId;
    int     m_SigType;
    int     m_SigId;
    int     m_Entry;
    char    m_oid[10];

    QCheckBox *pm_chk;
};

#endif // VALUETEXT2_H
