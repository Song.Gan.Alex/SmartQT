/*
 *  ui/valuetest.cpp
 *
 *  (C) 2021  Vertiv
 */

#include "valuetext.h"

valuetext::valuetext(int iItemIdex, int regAddr, float fValue, QDoubleSpinBox *parent)
{
    m_iItemIdex = iItemIdex;
    m_regAddr   = regAddr;
    m_fValue    = fValue;

    //qDebug("[%d] RegAddr=%04d - m_fValue=%6.2f", iItemIdex, regAddr, m_fValue);
    this->setFixedSize(65,25);

    this->setMinimum(-99999);
    this->setMaximum(+99999);

    this->setValue(fValue);

    connect(this, SIGNAL(valueChanged(double)), this, SLOT(sendSignals(double)));
}

void valuetext::sendSignals(double new_value)
{
    //m_fValue = this->document()->toPlainText().toFloat();
    m_fValue = (float)new_value;
    qDebug(">> emit fValue = %6.2f", m_fValue);

    emit valueChanged(m_iItemIdex, m_regAddr, m_fValue);
}
