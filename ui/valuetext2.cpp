#include "valuetext2.h"

#if textmodel
    valuetext2::valuetext2(int EquipId, char oid[], int SigType, int SigId, int Entry, QCheckBox *p_chk, QSpinBox *parent)
#else
    valuetext2::valuetext2(int EquipId, char oid[], int SigType, int SigId, int Entry, QCheckBox *p_chk, QTextEdit *parent)
#endif
{
    this->setFixedSize(65,25);

    m_EquipId = EquipId;
    m_SigType = SigType;
    m_SigId   = SigId;
    m_Entry   = Entry;

    pm_chk    = p_chk;

    memcpy(&m_oid[0], &oid[0], sizeof(m_oid));

    this->setMinimum(-99999);
    this->setMaximum(+99999);

    this->setValue(SigId);

    //textChanged
    //valueChanged
    //textEdited
    connect(this, SIGNAL(valueChanged(int)), this, SLOT(sendSignals2(int)));
}

void valuetext2::sendSignals2(int new_value)
{    
    qDebug("\n\nTrigger EquipId(%d) oid[%s]", m_EquipId, m_oid);
    pm_chk->setChecked(true);
    pm_chk->setStyleSheet("selection-background-color: rgb(255, 0, 127)");

    emit valueChanged2(m_EquipId, 0, new_value, m_Entry, &m_oid[0]);
}

