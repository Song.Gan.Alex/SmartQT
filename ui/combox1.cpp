#include "combox1.h"

combox1::combox1(int EquipId, char oid[], int SigType, int SigId, int Entry, QCheckBox *p_chk, QComboBox *parent)
{
    this->setFixedSize(65,25);

    m_EquipId = EquipId;
    m_SigType = SigType;
    m_SigId   = SigId;
    m_Entry   = Entry;

    pm_chk    = p_chk;

    memcpy(&m_oid[0], &oid[0], sizeof(m_oid));

    this->addItem("0-采集");
    this->addItem("1-控制");
    this->addItem("2-设置");
    this->setCurrentIndex(SigType);

    connect(this, SIGNAL(currentIndexChanged(int)), this, SLOT(sendSignals(int)));
}

void combox1::sendSignals(int sigtype)
{
    qDebug("\n#### Combox Trigger\n");

    qDebug("Trigger %d %d %d %d %s", m_EquipId, sigtype, m_SigId, m_Entry, &m_oid[0]);

    pm_chk->setChecked(true);

    //ui.checkBox->setStyleSheet("QCheckBox{color:rgb(105,170,238)}");

    emit comboxChanged(m_EquipId, sigtype, 0, m_Entry, &m_oid[0]);
}
