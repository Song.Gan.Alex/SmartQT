/*
 *  ui/namelable.cpp
 *
 *  (C) 2021  Vertiv
 */

#include "namelabel.h"

namelabel::namelabel(QString name) : QLabel(name)
{
    this->setText(name);
    this->setFixedSize(195,25); //字体框的大小
    this->setStyleSheet("background-color: rgb(255, 255, 255); \
                         border: 1px solid #000000;");

}

namelabel::namelabel(QString name, int x, int y) : QLabel(name)
{
    this->setText(name);
    this->setFixedSize(x,y); //字体框的大小
    this->setStyleSheet("background-color: rgb(255, 255, 255); \
                         border: 1px solid #000000;");
}
