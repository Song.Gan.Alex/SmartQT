
## SmartQT (Smart Quick Test) 打包教程




#### 步骤一：进入SmartQT项目，选择编译选项【1】，【2】指定为Release版本编译，最后点击【3】执行编译操作;


<img src="../img/packet_imges/packet_step1.png" alt="packet_step1" style="zoom:90%;" />







#### 步骤二：在SmartQT项目【1】中查看构建目录【2】，然后在文件目录对应位置【3】中找到生成的可执行应用程【4】SmartQT.exe;


<img src="../img/packet_imges/packet_step2.png" alt="packet_step2" style="zoom:90%;" />








#### 步骤三：在 "D盘" 下新建文件夹，并起名为"packge"【1】，将刚刚生成的可执行应用程SmartQT.exe 复制到该文件目录下【2】;


<img src="../img/packet_imges/packet_step3.png" alt="packet_step3" style="zoom:90%;" />






#### 步骤四：在 ''开始'' 菜单中选择点击【1】，右侧将弹出命令行，我们根据命令【2】和命令【3】进入到新建文件夹 ''D:/packge' 中， 最后执行以下命令【4】 完成依赖文件的抓取;


  执行这条命令  ==>  windeployqt SmartQT.exe


<img src="../img/packet_imges/packet_step5.png" alt="packet_step4" style="zoom:90%;" />





--------------------------



#### 生成结果


<img src="../img/packet_imges/output.png" alt="Snipaste_2022-01-12_00-01-10" style="zoom:50%;" />


#### 最后将 "SmartQT.exe" 创建快捷方式至桌面








