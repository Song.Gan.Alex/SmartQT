#ifndef PUBLICDATATYPE_H
#define PUBLICDATATYPE_H

#include <QString>

#define _OK 		(0)
#define _ERROR      (-1)

#define _HEX_ 		0x0010
#define _DEC_       10

#define IN
#define OUT

#define HIBYTEOFWORD(w)       (((w) >> 8) & 0xff)
#define LOBYTEOFWORD(w)       (((w) & 0xff))

//#define HIBYTE(w)           (((w) >> 4))
//#define LOBYTE(w)           (((w) & 0x0f))

#define MAKE_BYTE(a,b) ((((unsigned char)(a))<<4) + ((unsigned char)(b)))
#define MAKE_WORD(a,b) ((((unsigned char)(a))<<8) + ((unsigned char)(b)))
#define MAKE_LONG(a,b,c,d) ((((unsigned char)(a))<<12) + (((unsigned char)(b))<<8) \
                        + (((unsigned char)(c))<<4) + ((unsigned char)(d)))

#define GET_STD_MIB_TYPEID(TypeId) ((TypeId>1000)?(TypeId/1000):((TypeId>100)?(TypeId/100):(-1)))

//Modbus Data Value Type
#define MODBUS_VALUE_TYPE_BYTE         0
#define MODBUS_VALUE_TYPE_BYTE_LE      1
#define MODBUS_VALUE_TYPE_WORD         2
#define MODBUS_VALUE_TYPE_WORD_LE      3
#define MODBUS_VALUE_TYPE_WORD_L2      4   //返回长度2字节
#define MODBUS_VALUE_TYPE_FLOAT_LSW    5
#define MODBUS_VALUE_TYPE_FLOAT_BE     6
#define MODBUS_VALUE_TYPE_FLOAT_LE     7


#define DEFAULT_TCP_LISTEN_PORT        502
#define DEFAULT_SNMP_LISTEN_PORT       161
#define DEFAULT_SNMP_TRAP_PORT         162

//Modbus-TCP
#define RequestSize      12 //const bytes in each req
#define RequestAddr      6  //addr
#define RequestCode      7  //req code
#define RequestRegHigh   8  //
#define RequestRegLow    9  //
#define RequestLenHigh   10 //
#define RequestLenLow    11 //

//Mgr Type
enum EMgrType {
    mgr_type_null = 0,
    mgr_type_serial,
    mgr_type_tcp,
    mgr_type_udp,
    mgr_type_mib_mapping,
    mgr_type_ascii,
};

//Combox list Type
enum CmbType {
    type_Modbus_RTU = 0,
    type_Modbus_ASCII,
    type_Modbus_TCP,
    type_YDN23,
    type_SNMP,
    type_MAPPING,
    type_Null
};

//Message Type
enum EMessageType {
    type_connectNotify = 1,
    type_disconnectNotify,
    type_infoNotify,
    type_alarmNotify,
};

//Color Type
enum e_ColorType {
    e_DarkRed = 0,
    e_DarkBlue,
    e_DarkGreen,
    e_DarkBlack
};

//
enum SelectType {
    type_init = 0,
    type_clear,
    type_select_all,
    type_select_specify,
};

//Error code
enum e_Errcode {
    err_recv_null,
    err_recv_request_info_length = 1,
    err_recv_address,
    err_recv_crc_check,
};

//=================================== UI展示 =================
struct _ST_DISPLAY_STRUCT{
    QString RegName;
    float value;
};
typedef struct _ST_DISPLAY_STRUCT ST_DISPLAY_STRUCT;

//MIB映射
struct _ST_DISPLAY_MIB_NODE{
    int  TypeId;
    int  EquipTypeId; //+++RDU优化功能加入+++
    char strSigCHName[254]; //中文名称用于查询
    char strSigENName[254]; //英文名称用于对比
    char oid[10];
    int  Entry;
    //-------------
    int  EquipId;
    int  SigType;
    int  SigId;
    int  Flag;
};
typedef struct _ST_DISPLAY_MIB_NODE ST_DISPLAY_MIB_NODE;
//=============================================================


//=================================== 通讯端口信息(串口/网口) =====
typedef struct
{
    char PortName[50];
    char PortDescription[100];
}ST_COMM_PORT_INFO;
//=============================================================


//=================================== 端口配置 =================
//COM端口配置
typedef struct{
    QString strPortName; //->char[]
    int iBaudRate;
    int iStopBit;
    int iDeviceAddr;
}ST_PORT_PARAM;

//TCP/IP
typedef struct{
    QString IP; //->char[]
    int nPort;
}ST_NETWORK_PORT_PARAM;
//==============================================================


//================================= 接口修改 ====================
//MODBUS Modify Value Interface
typedef struct{    
    int iPageIndex;
    int index;
    float fValue;
}ST_MODIFY_VALUE_OF_MODBUS;

//Std Mib Modify Value Interface
typedef struct{
    int iEquipId;
    int iSigType;
    int iSigId;
    int iEntry;    
    char pOid[10];
}ST_MODIFY_VALUE_OF_STD_MIB;

//oid节点名称翻译 英转汉
typedef struct {
    int  typeId;
    char szCHName[254];
    char szENName[254];
    char szOID[10];
} ST_TRANSLATE_INFO_STRUCT;
//==============================================================


#endif // PUBLICDATATYPE_H
