#ifndef SNMPVERSION_H
#define SNMPVERSION_H

#define SNMP_PORT           161 /* standard UDP port for SNMP agents
                                 * to receive requests messages */
#define SNMP_TRAP_PORT	    162 /* standard UDP port for SNMP
                                 * managers to receive notificaion
                                 * (trap and inform) messages */

#define SNMP_MAX_LEN	    1500 /* typical maximum message size */
#define SNMP_MIN_MAX_LEN    484  /* minimum maximum message size */

#define MIN_OID_LEN         2
#define MAX_OID_LEN         128
#define MAX_NAME_LEN        MAX_OID_LEN
#define MAX_MIB_DEPTH       50

typedef unsigned int oid;
#define OID_LENGTH(x)       (sizeof(x)/sizeof(oid))

#define SNMP_MAX_PACKET_LEN (0x7fffffff)

/*
 * There currently exists the following SNMP versions.
 * * (Note that only SNMPv1 is in widespread usage, and this code supports
 * *  only SNMPv1, SNMPv2c, and SNMPv3.
 * *
 * *  SNMPv1 - (full) the original version, defined by RFC 1157
 * *  SNMPsec - (historic) the first attempt to add strong security
 * *             to SNMPv1, defined by RFCs 1351, 1352, and 1353.
 * *  SNMPv2p - (historic) party-based SNMP, which was another
 * *             attempt to add strong security to SNMP, defined
 * *             by RFCs 1441, 1445, 1446, 1448, and 1449.
 * *  SNMPv2c - (experimental) community string-based SNMPv2,
 * *             which was an attempt to combine the protocol
 * *             operations of SNMPv2 with the security of
 * *             SNMPv1, defined by RFCs 1901, 1905, and 1906.
 * *  SNMPv2u - (experimental) user-based SNMPv2, which provided
 * *             security based on user names and protocol
 * *             operations of SNMPv2, defined by RFCs 1905,
 * *             1909, and 1910.
 * *  SNMPv2* (or SNMPv2star) - (experimental) an attempt to add the
 * *             best features of SNMPv2p and SNMPv2u, defined
 * *             by unpublished documents found at WEB site
 * *             owned by SNMP Research (a leading SNMP vendor)
 * *  SNMPv3 - the current attempt by the IETF working group to merge
 * *             the SNMPv2u and SNMPv2* proposals into a more widly
 * *             accepted SNMPv3.  It is defined by not yet published
 * *             documents of the IETF SNMPv3 WG.
 * *
 * * SNMPv1, SNMPv2c, SNMPv2u, and SNMPv3 messages have a common
 * * form, which is an ASN.1 sequence containing a message version
 * * field, followed by version dependent fields.
 * * SNMPsec, SNMPv2p, and SNMPv2* messages have a common form,
 * * which is a tagged ASN.1 context specific sequence containing
 * * message dependent fields.
 * *
 * * In the #defines for the message versions below, the value
 * * for SNMPv1, SNMPv2c, SNMPv2u, and SNMPv3 messages is the
 * * value of the message version field. Since SNMPsec, SNMPv2p,
 * * and SNMPv2* messages do not have a message version field,
 * * the value in the defines for them is choosen to be a large
 * * arbitrary number.
 * *
 * * Note that many of the version ID's are defined below purely for
 * * documentational purposes.  At this point the only protocol planned
 * * for future implementations is SNMP3, as the other v2 protocols will
 * * not be supported by the IETF (ie, v2u, v2sec, v2star) or used by
 * * the snmp community at large (at the time of this writing).
 */

/*
 * versions based on version field
 */
#define SNMP_VERSION_1	   0
#define SNMP_VERSION_2c    1
#define SNMP_VERSION_2u    2    /* not (will never be) supported by this code */
#define SNMP_VERSION_3     3

/*
 * versions not based on a version field
 */
#define SNMP_VERSION_sec   128  /* not (will never be) supported by this code */
#define SNMP_VERSION_2p	   129  /* no longer supported by this code (> 4.0) */
#define SNMP_VERSION_2star 130  /* not (will never be) supported by this code */

#endif // SNMPVERSION_H
