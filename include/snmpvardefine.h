#ifndef SNMPVARDEFINE_H
#define SNMPVARDEFINE_H

enum asnType // RFC1213 subset of ASN.1
{
    BER_TYPE_EndMarker          = 0x00,
    BER_TYPE_BOOLEAN            = 0x01,
    BER_TYPE_INTEGER			= 0x02, // INTEGER type. (SMIv1, SMIv2)
    BER_TYPE_BIT_STRING			= 0x03,
    BER_TYPE_OCTET_STRING		= 0x04, // OCTET STRING type. X690.OctetString
    BER_TYPE_NULL				= 0x05, // NULL type. (SMIv1)
    BER_TYPE_OID				= 0x06, // OBJECT IDENTIFIER type. (SMIv1)
    BER_TYPE_SEQUENCE			= 0x30, // RFC1213 sequence for whole SNMP packet beginning
    BER_TYPE_IPADDR             = 0x40, // IpAddress type. (SMIv1)
    BER_TYPE_COUNTER			= 0x41, // Counter32 type. (SMIv1, SMIv2)
    BER_TYPE_GAUGE				= 0x42, // Gauge32 type. (SMIv1, SMIv2)
    BER_TYPE_TIME_TICKS			= 0x43, // TimeTicks type. (SMIv1)
    BER_TYPE_OPAQUE             = 0X44,
    BER_TYPE_NETADDR            = 0x45,
    BER_TYPE_COUNTER64          = 0x46, // Counter64 type. (SMIv2)
    BER_TYPE_UNSIGNED32         = 0x47, // Unsigned32 type. (Use this code in RFC 1442)

    BER_TYPE_NO_SUCH_OBJECT		= 0x80, // No such object exception.
    BER_TYPE_NO_SUCH_INSTANCE	= 0x81, // No such instance exception.
    BER_TYPE_END_OF_MIB_VIEW	= 0x82, // End of MIB view exception.

    BER_TYPE_SNMP_GET			= 0xA0, // Get request PDU.
    BER_TYPE_SNMP_GETNEXT		= 0xA1, // Get Next request PDU.
    BER_TYPE_SNMP_RESPONSE		= 0xA2, // Response PDU.
    BER_TYPE_SNMP_SET			= 0xA3, // Set request PDU.
    BER_TYPE_SNMP_TRAPV1PDU     = 0xA4, // Trap v1 PDU.

    BER_TYPE_SNMP_GETBULK		= 0xA5, // Get Bulk PDU.
    BER_TYPE_SNMP_INFORM		= 0xA6, // Inform PDU.
    BER_TYPE_SNMP_TRAP			= 0xA7, // Trap v2 PDU.
    BER_TYPE_SNMP_REPORT		= 0xA8, // Report PDU. SNMP v3.

    Unknown = 0xFFFF  // Defined by #SNMP for unknown type.
};

//union {
//    AsnInteger32 number;
//    AsnUnsigned32 unsigned32;
//    AsnCounter64 counter64;
//    AsnOctetString string;
//    AsnBits bits;
//    AsnObjectIdentifier object;
//    AsnSequence sequence;
//    AsnIPAddress address;
//    AsnCounter32 counter;
//    AsnGauge32 gauge;
//    AsnTimeticks ticks;
//    AsnOpaque arbitrary;
//}asnValue;

//Syntax Type
#define ASN_BOOLEAN        ((u_char)0x01)
#define ASN_INTEGER        ((u_char)0x02)
#define ASN_BIT_STR        ((u_char)0x03)
#define ASN_OCTET_STR      ((u_char)0x04)
#define ASN_NULL           ((u_char)0x05)
#define ASN_OBJECT_ID      ((u_char)0x06)
#define ASN_SEQUENCE       ((u_char)0x10)
#define ASN_SET            ((u_char)0x11)

//#define ASN_BOOLEAN	        0x01U
//#define ASN_INTEGER	        0x02U
//#define ASN_BIT_STR	        0x03U
//#define ASN_OCTET_STR	    0x04U
//#define ASN_NULL	        0x05U
//#define ASN_OBJECT_ID	    0x06U
//#define ASN_SEQUENCE	    0x10U
//#define ASN_SET		        0x11U

#define ASN_UNIVERSAL	    0x00U
#define ASN_APPLICATION     0x40U
#define ASN_CONTEXT	        0x80U
#define ASN_PRIVATE	        0xC0U

#define ASN_PRIMITIVE	    0x00U
#define ASN_CONSTRUCTOR	    0x20U

#define ASN_LONG_LEN	    0x80U
#define ASN_EXTENSION_ID    0x1FU
#define ASN_BIT8	        0x80U


/*
 * PDU types in SNMPv1, SNMPsec, SNMPv2p, SNMPv2c, SNMPv2u, SNMPv2*, and SNMPv3
 */
#define SNMP_MSG_GET        (ASN_CONTEXT | ASN_CONSTRUCTOR | 0x0) /* a0=160 */
#define SNMP_MSG_GETNEXT    (ASN_CONTEXT | ASN_CONSTRUCTOR | 0x1) /* a1=161 */
#define SNMP_MSG_RESPONSE   (ASN_CONTEXT | ASN_CONSTRUCTOR | 0x2) /* a2=162 */
#ifndef NETSNMP_NO_WRITE_SUPPORT
#define SNMP_MSG_SET        (ASN_CONTEXT | ASN_CONSTRUCTOR | 0x3) /* a3=163 */
#endif /* !NETSNMP_NO_WRITE_SUPPORT */

/*
 * PDU types in SNMPv1 and SNMPsec
 */
#define SNMP_MSG_TRAP       (ASN_CONTEXT | ASN_CONSTRUCTOR | 0x4) /* a4=164 */

/*
 * PDU types in SNMPv2p, SNMPv2c, SNMPv2u, SNMPv2*, and SNMPv3
 */
#define SNMP_MSG_GETBULK    (ASN_CONTEXT | ASN_CONSTRUCTOR | 0x5) /* a5=165 */
#define SNMP_MSG_INFORM     (ASN_CONTEXT | ASN_CONSTRUCTOR | 0x6) /* a6=166 */
#define SNMP_MSG_TRAP2      (ASN_CONTEXT | ASN_CONSTRUCTOR | 0x7) /* a7=167 */

/*
 * PDU types in SNMPv2u, SNMPv2*, and SNMPv3
 */
#define SNMP_MSG_REPORT     (ASN_CONTEXT | ASN_CONSTRUCTOR | 0x8) /* a8=168 */


#define IS_CONSTRUCTOR(byte)	((byte) & ASN_CONSTRUCTOR)
#define IS_EXTENSION_ID(byte)	(((byte) & ASN_EXTENSION_ID) == ASN_EXTENSION_ID)


#endif // SNMPVARDEFINE_H
