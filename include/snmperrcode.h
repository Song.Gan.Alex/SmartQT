#ifndef SNMPERRCODE_H
#define SNMPERRCODE_H

/*
 * Error codes (the value of the field error-status in PDUs)
 */

/*
 * in SNMPv1, SNMPsec, SNMPv2p, SNMPv2c, SNMPv2u, SNMPv2*, and SNMPv3 PDUs
 */
#define SNMP_ERR_NOERROR                (0)
#define SNMP_ERR_TOOBIG	                (1)
#define SNMP_ERR_NOSUCHNAME             (2)
#define SNMP_ERR_BADVALUE               (3)
#define SNMP_ERR_READONLY               (4)
#define SNMP_ERR_GENERR	                (5)

/*
 * in SNMPv2p, SNMPv2c, SNMPv2u, SNMPv2*, and SNMPv3 PDUs
 */
#define SNMP_ERR_NOACCESS               (6)
#define SNMP_ERR_WRONGTYPE              (7)
#define SNMP_ERR_WRONGLENGTH            (8)
#define SNMP_ERR_WRONGENCODING          (9)
#define SNMP_ERR_WRONGVALUE             (10)
#define SNMP_ERR_NOCREATION             (11)
#define SNMP_ERR_INCONSISTENTVALUE      (12)
#define SNMP_ERR_RESOURCEUNAVAILABLE	(13)
#define SNMP_ERR_COMMITFAILED           (14)
#define SNMP_ERR_UNDOFAILED             (15)
#define SNMP_ERR_AUTHORIZATIONERROR 	(16)
#define SNMP_ERR_NOTWRITABLE            (17)


/*
 * Error return values.
 *
 * SNMPERR_SUCCESS is the non-PDU "success" code.
 *
 * XXX  These should be merged with SNMP_ERR_* defines and confined
 *      to values < 0.  ???
 */
#define SNMPERR_SUCCESS                 (0)     /* XXX  Non-PDU "success" code. */
#define SNMPERR_GENERR                  (-1)
#define SNMPERR_BAD_LOCPORT             (-2)
#define SNMPERR_BAD_ADDRESS             (-3)
#define SNMPERR_BAD_SESSION             (-4)
#define SNMPERR_TOO_LONG                (-5)
#define SNMPERR_NO_SOCKET               (-6)
#define SNMPERR_V2_IN_V1                (-7)
#define SNMPERR_V1_IN_V2                (-8)
#define SNMPERR_BAD_REPEATERS           (-9)
#define SNMPERR_BAD_REPETITIONS         (-10)
#define SNMPERR_BAD_ASN1_BUILD          (-11)
#define SNMPERR_BAD_SENDTO              (-12)
#define SNMPERR_BAD_PARSE               (-13)
#define SNMPERR_BAD_VERSION             (-14)
#define SNMPERR_BAD_SRC_PARTY           (-15)
#define SNMPERR_BAD_DST_PARTY           (-16)
#define SNMPERR_BAD_CONTEXT             (-17)
#define SNMPERR_BAD_COMMUNITY           (-18)
#define SNMPERR_NOAUTH_DESPRIV          (-19)
#define SNMPERR_BAD_ACL                 (-20)
#define SNMPERR_BAD_PARTY               (-21)
#define SNMPERR_ABORT                   (-22)
#define SNMPERR_UNKNOWN_PDU             (-23)
#define SNMPERR_TIMEOUT                 (-24)
#define SNMPERR_BAD_RECVFROM            (-25)
#define SNMPERR_BAD_ENG_ID              (-26)
#define SNMPERR_BAD_SEC_NAME            (-27)
#define SNMPERR_BAD_SEC_LEVEL           (-28)
#define SNMPERR_ASN_PARSE_ERR           (-29)
#define SNMPERR_UNKNOWN_SEC_MODEL       (-30)
#define SNMPERR_INVALID_MSG             (-31)
#define SNMPERR_UNKNOWN_ENG_ID          (-32)
#define SNMPERR_UNKNOWN_USER_NAME       (-33)
#define SNMPERR_UNSUPPORTED_SEC_LEVEL 	(-34)
#define SNMPERR_AUTHENTICATION_FAILURE 	(-35)
#define SNMPERR_NOT_IN_TIME_WINDOW      (-36)
#define SNMPERR_DECRYPTION_ERR          (-37)
#define SNMPERR_SC_GENERAL_FAILURE      (-38)
#define SNMPERR_SC_NOT_CONFIGURED       (-39)
#define SNMPERR_KT_NOT_AVAILABLE        (-40)
#define SNMPERR_UNKNOWN_REPORT          (-41)
#define SNMPERR_USM_GENERICERROR		(-42)
#define SNMPERR_USM_UNKNOWNSECURITYNAME		(-43)
#define SNMPERR_USM_UNSUPPORTEDSECURITYLEVEL	(-44)
#define SNMPERR_USM_ENCRYPTIONERROR		(-45)
#define SNMPERR_USM_AUTHENTICATIONFAILURE	(-46)
#define SNMPERR_USM_PARSEERROR			(-47)
#define SNMPERR_USM_UNKNOWNENGINEID		(-48)
#define SNMPERR_USM_NOTINTIMEWINDOW		(-49)
#define SNMPERR_USM_DECRYPTIONERROR		(-50)
#define SNMPERR_NOMIB                   (-51)
#define SNMPERR_RANGE                   (-52)
#define SNMPERR_MAX_SUBID               (-53)
#define SNMPERR_BAD_SUBID               (-54)
#define SNMPERR_LONG_OID                (-55)
#define SNMPERR_BAD_NAME                (-56)
#define SNMPERR_VALUE                   (-57)
#define SNMPERR_UNKNOWN_OBJID           (-58)
#define SNMPERR_NULL_PDU                (-59)
#define SNMPERR_NO_VARS                 (-60)
#define SNMPERR_VAR_TYPE                (-61)
#define SNMPERR_MALLOC                  (-62)
#define SNMPERR_KRB5                    (-63)
#define SNMPERR_PROTOCOL                (-64)
#define SNMPERR_OID_NONINCREASING       (-65)
#define SNMPERR_JUST_A_CONTEXT_PROBE    (-66)
#define SNMPERR_TRANSPORT_NO_CONFIG     (-67)
#define SNMPERR_TRANSPORT_CONFIG_ERROR  (-68)
#define SNMPERR_TLS_NO_CERTIFICATE      (-69)

#endif // SNMPERRCODE_H
