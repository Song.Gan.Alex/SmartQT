#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QScrollArea>
#include <QWidget>
#include <QGridLayout>
#include <QTextBrowser>

#include "parsefile/MgrDataBase.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    bool eventFilter(QObject *watched, QEvent *event);

private:
    Ui::MainWindow *ui;    
    QVector<ST_COMM_PORT_INFO> m_PortInfoList;
    int szEquipListBuf[100][3]; //(equipid,type,equiptypeid)

    //QString::number(QDateTime::currentMSecsSinceEpoch())
    qint64 time_start;
    qint64 time_end;

    bool b_EnablePrintlog;

    int m_indexPortInfoList;
    int m_iDisplayType;

    int m_iSamplerCurrIndex_sheet1;
    int m_iSamplerCurrIndex_sheet2;
    int m_iSamplerCurrIndex_sheet3;

    MgrDataBase *m_pCurrMgr; /* mgr base */
    MgrDataBase *pMgrModbusSlaver;
    MgrDataBase *pMgrModbusASCII;
    MgrDataBase *pMgrPowerDevice;
    MgrDataBase *pMgrModbusTcpServer;
    MgrDataBase *pMgrSnmpServer;
    MgrDataBase *pMgrMibMapping;

    bool openMgrPort();
    bool closeMgrPort();

    void clearDisplayContent(int type);
    void displayContent_1();
    void displayContent_2();
    void loading_Mapping_TypeId_Style(SelectType type, int EquipIDBuf[][3]);
    int  displayContent_1_mibmapping(SelectType select_type);

    void setSerialsDisplay(bool flag);
    void setNetworkDisplay(bool flag);

    QScrollArea*    scroll_1;
    QWidget*        widget_1;
    QGridLayout*    layout_1;

    QScrollArea*    scroll_2;
    QWidget*        widget_2;
    QGridLayout*    layout_2;

    QScrollArea*    scroll_3;
    QWidget*        widget_3;
    QGridLayout*    layout_3;

signals:
    void valueChanged(int, int, float);
    void PrintData(QByteArray,int,QString);
    void refreshDisplay(int ,int, float);

private slots:    

    void on_title_cmb_workmode_currentIndexChanged(int index);
    void on_title_Btn_SelectFile_clicked();

    void on_Btn_SelectSheet1_clicked();
    void on_Btn_SelectSheet2_clicked();

    //comm value change
    void onValueChanged(int iItemIndex, int iRegAddr, float fValue);
    //mib mapping value change
    void onValueChanged2(int iEquipId, int iSigType, int iSigId, int iEntry, char* pOid);

    QTextBrowser *pickTextBrowser(MgrDataBase* pCurrMgr);
    void onPrintfData(QByteArray buf, QString str);    
    void onPrintMessage(EMgrType type, EMessageType MessageType, QString str);
    void onRefreshDisplay(int iPageIndex, int iIndex, float fValue);

    void doPrintWithColor(QTextBrowser* qControl, e_ColorType color, QString str);

    void on_Btn_openPort_clicked();
    void on_Btn_openNetPort_clicked();

    void on_Btn_SaveLog_clicked();
    void on_Btn_ControlEnablePrintlog_clicked();
    void on_Btn_ClearLog_clicked();
    void on_txt_SelectSheet1_textChanged();
    void on_txt_SelectSheet2_textChanged();

    void on_Btn_ClearLog_2_clicked();
    void on_Btn_ShowMode_clicked();
    void on_Btn_SaveLog_2_clicked();

    void on_Btn_head3_file_output_clicked();
    void on_Btn_head3_filter_clicked();
    void on_Btn_head3_selectAll_clicked();    

    void on_cmb_SerialPort_currentIndexChanged(const QString &arg1);
    void on_cmb_head3_type_currentIndexChanged(const QString &arg1);
    void on_cmb_head3_equipid_currentTextChanged(const QString &arg1);
    void on_chk_page2_Synchronize_clicked(bool checked);
};

#endif // MAINWINDOW_H
