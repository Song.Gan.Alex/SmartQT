/*==========================================================================*
 *    Copyright(c) 2001-2021, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : SmartQT (Smart Quick Test)
 *
 *  FILENAME : ./mainwindow.c
 *  CREATOR  : lh.Hank & SG.Alex             DATE: 2020-05-27 23:53
 *  VERSION  : V1.2.0
 *  PURPOSE  : Definations for the interface of ui service module. [MCV model]
 *
 *  HISTORY  :
 *==========================================================================*/

#include <QFileDialog>
#include <QDebug>
#include <QDateTime>
#include <QFile>
#include <QMessageBox>
#include <QScrollBar>
#include <QKeyEvent>
#include <QPropertyAnimation> //test
#include <QSettings>

#include "include/publicdatatype.h"
#include "ui/combox1.h"
#include "ui/namelabel.h"
#include "ui/valuetext.h"
#include "ui/valuetext2.h"

#include "parsefile/MgrSerialSlaver.h"
#include "parsefile/MgrModbusASCII.h"
#include "parsefile/MgrPowerDevice.h"
#include "parsefile/MgrSNMPServer.h"
#include "parsefile/MgrTcpServer.h"
#include "parsefile/MgrMibMapping.h"

#include "include/mainwindow.h"
#include "ui_mainwindow.h"

int trigger_count = 0;
QString file_path; //文件加载路径

MainWindow::MainWindow(QWidget *parent): QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //setFixedSize(350,180);    //初始化时窗口显示固定大小
    this->setWindowIcon(QIcon(":/img/aghm8-au1us-006.ico"));

    qDebug() << " Welcome to Vertiv.co SmartQT(Smart Quick Test) :-) ";
    this->doPrintWithColor(ui->tail_txt_log, e_DarkGreen,"Welcome to Vertiv.co SmartQT(Smart Quick Test) :-)");    
    this->doPrintWithColor(ui->tail_txt_log, e_DarkGreen,"############## Loading Data Start ###############");

    //后期关闭
    ui->lbl_Sheet1->hide();
    ui->Btn_SelectSheet1->hide();
    ui->txt_SelectSheet1->hide();

    ui->lbl_Sheet2->hide();
    ui->Btn_SelectSheet2->hide();
    ui->txt_SelectSheet2->hide();

    ui->txt_SelectSheet1->installEventFilter(this);
    ui->txt_SelectSheet2->installEventFilter(this);
    ui->txt_head3_selectsheet2->installEventFilter(this);

    m_iSamplerCurrIndex_sheet1 = -1;
    m_iSamplerCurrIndex_sheet2 = -1;
    m_iSamplerCurrIndex_sheet3 = -1;

    QIcon icon;
    icon.addFile(tr(":/img/newtask@1.25x.png"));
    ui->title_Btn_SelectFile->setIcon(icon);
    ui->title_Btn_SelectFile->setText("选择文件");

    pMgrModbusSlaver = new MgrModbusSlaver();
    pMgrPowerDevice = new MgrPowerDevice();
    pMgrModbusASCII = new MgrModbusASCII();
    pMgrModbusTcpServer = new MgrModbusTcpServer(DEFAULT_TCP_LISTEN_PORT);
    pMgrSnmpServer = new MgrSNMPServer(DEFAULT_SNMP_LISTEN_PORT);
    pMgrMibMapping = new MgrMibMapping();

    on_title_cmb_workmode_currentIndexChanged(0);


    connect(pMgrModbusSlaver,SIGNAL(mgrMessageTip(EMgrType,EMessageType,QString)), this,SLOT(onPrintMessage(EMgrType,EMessageType,QString)));
    connect(pMgrModbusSlaver,SIGNAL(mgrReadWriteData(QByteArray,QString)), this,SLOT(onPrintfData(QByteArray,QString)));
    connect(pMgrModbusSlaver,SIGNAL(mgrRefreshDisplay(int,int,float)), this,SLOT(onRefreshDisplay(int,int,float)));

    connect(pMgrModbusASCII,SIGNAL(mgrMessageTip(EMgrType,EMessageType,QString)), this,SLOT(onPrintMessage(EMgrType,EMessageType,QString)));

    connect(pMgrModbusTcpServer,SIGNAL(mgrMessageTip(EMgrType,EMessageType,QString)), this,SLOT(onPrintMessage(EMgrType,EMessageType,QString)));
    connect(pMgrModbusTcpServer,SIGNAL(mgrReadWriteData(QByteArray,QString)), this,SLOT(onPrintfData(QByteArray,QString)));

    connect(pMgrSnmpServer,SIGNAL(mgrReadWriteData(QByteArray,QString)), this,SLOT(onPrintfData(QByteArray,QString)));

    connect(pMgrMibMapping,SIGNAL(mgrMessageTip(EMgrType,EMessageType,QString)), this,SLOT(onPrintMessage(EMgrType,EMessageType,QString)));

    //Get system port information for display Drop-down Menu-List
    m_pCurrMgr = pMgrModbusSlaver;
    m_PortInfoList = m_pCurrMgr->getPortInfo();
    QVector<ST_COMM_PORT_INFO>::iterator itor = m_PortInfoList.begin();
    this->doPrintWithColor(ui->tail_txt_log, e_DarkGreen, "### Loading Port Description.");

    for(int i=0; itor != m_PortInfoList.end(); itor++, i++)
    {
        ui->cmb_SerialPort->addItem(itor->PortName);

        QString strInfo = QString::fromUtf8(itor->PortName) +
                " (" + QString::fromUtf8(itor->PortDescription) + ")";
        this->doPrintWithColor(ui->tail_txt_log, e_DarkGreen, strInfo);
    }

    m_iDisplayType = _HEX_;
    b_EnablePrintlog = true;    

    ui->lbl_page2_1->hide();
    ui->lbl_page2_2->hide();
    ui->lbl_page2_3->hide();
    ui->lbl_page2_4->hide();
    ui->lbl_page2_5->hide();
    ui->lbl_page2_7->hide();

    //    /* 先检查一下有没有该文件，没有的化就创建；有的话读取；内容应该是由结构体记录的 */
    //    //写
    //    //Qt中使用QSettings类读写ini文件
    //    //QSettings构造函数的第一个参数是ini文件的路径,第二个参数表示针对ini文件,第三个参数可以缺省
    //    QSettings *configIniWrite = new QSettings("_backup.ini", QSettings::IniFormat);
    //    //向ini文件中写入内容,setValue函数的两个参数是键值对
    //    //向ini文件的第一个节写入内容,ip节下的第一个参数
    //    configIniWrite->setValue("/ip/first", "192.168.0.1");
    //    //向ini文件的第一个节写入内容,ip节下的第二个参数
    //    configIniWrite->setValue("ip/second", "127.0.0.1");
    //    //向ini文件的第二个节写入内容,port节下的第一个参数
    //    configIniWrite->setValue("port/open", "2222");
    //    //写入完成后删除指针
    //    delete configIniWrite;

    //    //读
    //    QSettings *configIniRead = new QSettings("_backup.ini", QSettings::IniFormat);
    //    //将读取到的ini文件保存在QString中，先取值，然后通过toString()函数转换成QString类型
    //    QString ipResult = configIniRead->value("/ip/second").toString();
    //    QString portResult = configIniRead->value("/port/open").toString();
    //    //打印得到的结果
    //    qDebug() << "ipResult " << ipResult;
    //    qDebug() << portResult;
    //    //读入入完成后删除指针
    //    delete configIniRead;

    //显示2000行数据
    ui->tail_txt_log->document()->setMaximumBlockCount(200);
    ui->tail_txt_log_2->document()->setMaximumBlockCount(200);
    //ui->tail_txt_log_3->document()->setMaximumBlockCount(500);

    this->doPrintWithColor(ui->tail_txt_log, e_DarkGreen, "############# Loading End #############\n");
}

MainWindow::~MainWindow()
{
    delete pMgrModbusSlaver;
    delete pMgrModbusTcpServer;
    delete pMgrPowerDevice;
    delete pMgrSnmpServer;
    delete pMgrMibMapping;

    delete ui;        
}


/**
 * @brief MainWindow::clearDisplayContent 清空加载显示
 * @param type (0=RTU 1=ASCIII 2=TCP 3=YDN23 4=SNMP 5=MibMapping)
 */
void MainWindow::clearDisplayContent(int ctype)
{
    switch(ctype)
    {
    case type_Modbus_RTU:
    case type_YDN23:
    case type_Modbus_TCP:
        //清空horizontalLayout布局内的所有元素
        QLayoutItem *child;
        while ((child = layout_1->takeAt(0)) != 0)
        {
            //setParent为NULL，防止删除之后界面不消失
            if(child->widget())
            {
                child->widget()->setParent(NULL);
            }

            delete child;
        }
        delete layout_1;
        delete widget_1;
        delete scroll_1;

        //清空horizontalLayout布局内的所有元素
        QLayoutItem *child2;
        while ((child2 = layout_2->takeAt(0)) != 0)
        {
            //setParent为NULL，防止删除之后界面不消失
            if(child2->widget())
            {
                child2->widget()->setParent(NULL);
            }

            delete child2;
        }
        delete layout_2;
        delete widget_2;
        delete scroll_2;
        break;
    case type_Modbus_ASCII:

        break;
    case type_SNMP:

        break;
    case type_MAPPING:
        //清空horizontalLayout布局内的所有元素
        QLayoutItem *child3;
        while ((child3 = layout_3->takeAt(0)) != 0)
        {
            //setParent为NULL，防止删除之后界面不消失
            if(child3->widget())
            {
                child3->widget()->setParent(NULL);
            }

            delete child3;
        }
        delete layout_3;
        delete widget_3;
        delete scroll_3;
        ui->cmb_head3_equipid->clear();
        this->loading_Mapping_TypeId_Style(type_init, this->szEquipListBuf);
        break;
    default:
        break;
    }
}

bool MainWindow::eventFilter(QObject *watched, QEvent *event)
{
    if(event->type() == QEvent::KeyPress)
    {
        QKeyEvent *keyEvent = static_cast <QKeyEvent *>(event);
        if(keyEvent->key() == Qt::Key_Return)
        {
            if (watched == ui->txt_SelectSheet1)
            {
                on_Btn_SelectSheet1_clicked();
            }
            else if(watched == ui->txt_SelectSheet2)
            {
                on_Btn_SelectSheet2_clicked();
            }
            else if(watched == ui->txt_head3_selectsheet2)
            {
                on_Btn_head3_filter_clicked();
            }

            return true;
        }
    }
    else if(event->type() == QEvent::Wheel)
    {
        //qDebug() << "do nothing";
        return true;
    }

    return false;
}

void MainWindow::onPrintfData(QByteArray buf, QString str)
{
    //this->doPrintWithColor(ui->tail_txt_log, e_DarkBlack, "MainWindow::onPrintfData");
    if(b_EnablePrintlog == false)
    {
        return;
    }

    QDateTime current_date_time = QDateTime::currentDateTime();
    QString current_date = current_date_time.toString("[hh:mm:ss.zzz] ");
    QString strPrint;
    strPrint.clear();

    strPrint = current_date + str + " ";

    if (m_iDisplayType == _HEX_)
    {
        QString data = buf.toHex();
        for(int i = 0;i < data.length()/2;i++)
        {
            strPrint.append(data.at(i * 2).toUpper());
            strPrint.append(data.at(i * 2 + 1).toUpper());
            strPrint += " ";
        }
    }
    else if (m_iDisplayType == _DEC_)
    {
        QString data = buf.data();
        strPrint += data;
    }

    if("Send" == str)
    {
        this->doPrintWithColor(ui->tail_txt_log, e_DarkBlack, strPrint);
    }
    else if("Recv" == str)
    {
        this->doPrintWithColor(ui->tail_txt_log, e_DarkGreen, strPrint);
    }
}


void MainWindow::doPrintWithColor(QTextBrowser* qControl, e_ColorType color, QString str)
{
    QString strPrint;
    strPrint.clear();

    if(b_EnablePrintlog == false)
    {
        return;
    }

    switch(color)
    {
    case e_DarkRed:
        qControl->append("<font color=\"#FF0000\">" + str + "</font>"); //BC1717
        break;
    case e_DarkBlue:
        qControl->append("<font color=\"#00FF00\">" + str + "</font>"); //00009C
        break;
    case e_DarkGreen:
        qControl->append("<font color=\"#00FF00\">" + str + "</font>"); //426F42
        break;
    case e_DarkBlack:
        qControl->append("<font color=\"#00FF00\">" + str + "</font>"); //000000
        break;
    default:
        qControl->append(str);
        break;
    }

    return;
}

QTextBrowser *MainWindow::pickTextBrowser(MgrDataBase *pCurrMgr)
{
    QTextBrowser *pControls;

    if (pCurrMgr == pMgrModbusASCII)
    {
        pControls = ui->tail_txt_log_3;
    }
    else if (pCurrMgr == pMgrMibMapping)
    {
        pControls = ui->tail_txt_log_2;
    }
    else
    {
        pControls = ui->tail_txt_log;
    }
    return pControls;
}

void MainWindow::onPrintMessage(EMgrType type, EMessageType MessageType, QString str)
{
    switch(type)
    {
    case mgr_type_serial:
    case mgr_type_ascii:
    {
        if (MessageType == type_infoNotify)
        {
            this->doPrintWithColor(pickTextBrowser(m_pCurrMgr), e_DarkGreen, str);
        }
        else if (MessageType == type_alarmNotify)
        {
            this->doPrintWithColor(pickTextBrowser(m_pCurrMgr), e_DarkRed, str);
        }
    }
    break;
    case mgr_type_tcp:
    case mgr_type_udp:
    {
        if (MessageType == type_connectNotify)
        {
            ui->cmb_ListClientIP->addItem(str);
        }
        else if (MessageType == type_disconnectNotify)
        {
            ui->cmb_ListClientIP->clear(); //连接断开，清除客户端Socket显示
        }
        else if (MessageType == type_infoNotify)
        {
            this->doPrintWithColor(pickTextBrowser(m_pCurrMgr), e_DarkGreen, str);
        }
        else
        {
            this->doPrintWithColor(pickTextBrowser(m_pCurrMgr), e_DarkRed, str);
        }
    }
    break;
    case mgr_type_mib_mapping:
        this->doPrintWithColor(pickTextBrowser(m_pCurrMgr), e_DarkGreen, str);
    break;
    default:
        ;
    }
}

void MainWindow::setSerialsDisplay(bool flag)
{
    ui->title_cmb_workmode->setEnabled(flag);
    ui->cmb_SerialPort->setEnabled(flag);
    ui->cmb_SerialBaudRate->setEnabled(flag);
    ui->cmb_SerialDataBits->setEnabled(flag);
    ui->cmb_SerialParity->setEnabled(flag);
    ui->cmb_SerialStopBits->setEnabled(flag);
    ui->txt_com_addr->setEnabled(flag);
}

void MainWindow::setNetworkDisplay(bool flag)
{
    ui->title_cmb_workmode->setEnabled(flag);
    ui->txt_net_port->setEnabled(flag);
    ui->txt_net_ip->setEnabled(flag);
}


void MainWindow::on_cmb_SerialPort_currentIndexChanged(const QString &arg1)
{
    static bool bLock = false; //避免窗体加载过程中的重复显示
    if(bLock == false)
    {
        bLock = true;
    }
    else
    {
        QVector<ST_COMM_PORT_INFO>::iterator itor = m_PortInfoList.begin();

        for(int i=0; itor != m_PortInfoList.end(); itor++, i++)
        {
            if(arg1 == itor->PortName)
            {
                QString strInfo = QString::fromUtf8(itor->PortName) +
                        " (" + QString::fromUtf8(itor->PortDescription) + ")";

                this->doPrintWithColor(ui->tail_txt_log, e_DarkGreen, strInfo);
            }
        }
    }
}

void MainWindow::on_txt_SelectSheet1_textChanged()
{
    if(-1 != m_iSamplerCurrIndex_sheet1)
    {
        namelabel* pname = (namelabel*)layout_1->itemAtPosition(m_iSamplerCurrIndex_sheet1,0)->widget();
        pname->setStyleSheet("background-color: rgb(255, 255, 255); \
                              border: 1px solid #000000;color:black");
    }

    m_iSamplerCurrIndex_sheet1 = -1;
}


void MainWindow::on_txt_SelectSheet2_textChanged()
{
    if(-1 != m_iSamplerCurrIndex_sheet2)
    {
        namelabel* pname = (namelabel*)layout_1->itemAtPosition(m_iSamplerCurrIndex_sheet2,0)->widget();
        pname->setStyleSheet("background-color: rgb(255, 255, 255);\
                              border: 1px solid #000000;color:black");
    }

    m_iSamplerCurrIndex_sheet2 = -1;
}


/**********************************************
 * Step1: 工作模式选择
 *********************************************/
void MainWindow::on_title_cmb_workmode_currentIndexChanged(int index)
{
    // TODO:切换工作模式后，删除之前的实例化+++

    qDebug() << "index tmp : " << index;
    switch(index)
    {
    case 0 : //Modbus-RTU
    {
        qDebug() << "==== Select Modbus-RTU Server Mode ====";
        ui->stackedWidget->setCurrentIndex(0);
        ui->stackedWidget_tail->setCurrentIndex(0);
        m_pCurrMgr = pMgrModbusSlaver;
        this->doPrintWithColor(pickTextBrowser(m_pCurrMgr), e_DarkGreen, "切换模式 Modbus-RTU");
        QIcon icon;
        icon.addFile(tr(":/img/NavOverFlow_Start.png"));
        ui->Btn_openPort->setIcon(icon);
        ui->Btn_openPort->setText("打开端口");
    }
    break;
    case 1 : //Modbus-ASCII
    {
        qDebug() << "==== Select Modbus-ASCII Server Mode ====";
        ui->stackedWidget->setCurrentIndex(0);
        ui->stackedWidget_tail->setCurrentIndex(2);
        m_pCurrMgr = pMgrModbusASCII;
        this->doPrintWithColor(pickTextBrowser(m_pCurrMgr), e_DarkGreen, "切换模式 Modbus-ASCII");
        QIcon icon;
        icon.addFile(tr(":/img/NavOverFlow_Start.png"));
        ui->Btn_openPort->setIcon(icon);
        ui->Btn_openPort->setText("打开端口");
    }
    break;
    case 2 : //Modbus-TCP Server
    {
        qDebug() << "==== Select Modbus-TCP Server Mode ====";
        ui->stackedWidget->setCurrentIndex(1);
        ui->stackedWidget_tail->setCurrentIndex(0);
        m_pCurrMgr = pMgrModbusTcpServer;
        this->doPrintWithColor(pickTextBrowser(m_pCurrMgr), e_DarkGreen, "切换模式 Modbus-TCP Server");
        m_PortInfoList = m_pCurrMgr->getPortInfo();
        QVector<ST_COMM_PORT_INFO>::iterator itor = m_PortInfoList.begin();
        ui->txt_net_ip->setText(QString::fromUtf8(itor->PortName));
        ui->txt_net_port->setText(QString::number(DEFAULT_TCP_LISTEN_PORT));
        QIcon icon;
        icon.addFile(tr(":/img/NavOverFlow_Start.png"));
        ui->Btn_openNetPort->setIcon(icon);
        ui->Btn_openNetPort->setText("打开服务");
    }
    break;
    case 3 : //YDN23
    {
        qDebug() << "==== Select YDN23 Mode ====";
        ui->stackedWidget->setCurrentIndex(0);
        ui->stackedWidget_tail->setCurrentIndex(0);
        m_pCurrMgr = pMgrPowerDevice;
        this->doPrintWithColor(pickTextBrowser(m_pCurrMgr), e_DarkGreen, "切换模式 YDN23");
        QIcon icon;
        icon.addFile(tr(":/img/NavOverFlow_Start.png"));
        ui->Btn_openPort->setIcon(icon);
        ui->Btn_openPort->setText("打开端口");
    }
    break;
    case 4 : //SNMP
    {
        qDebug() << "==== Select SNMP Mode ====";
        ui->stackedWidget->setCurrentIndex(1);
        ui->stackedWidget_tail->setCurrentIndex(0);
        m_pCurrMgr = pMgrSnmpServer;
        this->doPrintWithColor(pickTextBrowser(m_pCurrMgr), e_DarkGreen, "切换模式 SNMP");
        m_PortInfoList = m_pCurrMgr->getPortInfo();
        QVector<ST_COMM_PORT_INFO>::iterator itor = m_PortInfoList.begin();
        ui->txt_net_ip->setText(QString::fromUtf8(itor->PortName));
        ui->txt_net_port->setText(QString::number(DEFAULT_SNMP_LISTEN_PORT));

        QIcon icon;
        icon.addFile(tr(":/img/NavOverFlow_Start.png"));
        ui->Btn_openNetPort->setIcon(icon);
        ui->Btn_openNetPort->setText("打开服务");        
    }
    break;
    case 5 : //Mib-Mapping
    {
        qDebug() << "==== Select Mib-Mapping Mode ====";
        ui->stackedWidget->setCurrentIndex(2);
        ui->stackedWidget_tail->setCurrentIndex(1);
        m_pCurrMgr = pMgrMibMapping;
        this->doPrintWithColor(pickTextBrowser(m_pCurrMgr), e_DarkGreen, "切换模式 Mib映射");
        memset(this->szEquipListBuf, 0, sizeof(this->szEquipListBuf));
        this->loading_Mapping_TypeId_Style(type_init, this->szEquipListBuf);
    }
    break;
    default : //default is modbus-rtu
        qDebug() << "==== Select default Mode ====";
        ui->stackedWidget->setCurrentIndex(0);
        ui->stackedWidget_tail->setCurrentIndex(0);
        m_pCurrMgr = pMgrModbusSlaver;
    }
    qDebug() << "==== return ====";
    return;
}

/*
 * Step2: 配置文件加载
 * 选择模式: 1."*.xlsx" | 2."*.csv" | 3."*.txt"
 */
void MainWindow::on_title_Btn_SelectFile_clicked()
{
    trigger_count = 1;    
    this->doPrintWithColor(pickTextBrowser(m_pCurrMgr), e_DarkGreen, "#1.Open Configure File...");
    QString full_name_filepath;

    // 选择加载模式
    if((m_pCurrMgr == pMgrModbusSlaver) || (m_pCurrMgr == pMgrPowerDevice) ||
       (m_pCurrMgr == pMgrSnmpServer)   || (m_pCurrMgr == pMgrModbusTcpServer))
    {
        full_name_filepath = QFileDialog::getOpenFileName(this, "Open File", "C:/", "*.xlsx"); // => "*.xlsx"
    }
    else if(m_pCurrMgr == pMgrMibMapping)
    {
        full_name_filepath = QFileDialog::getOpenFileName(this, "Open File", "C:/", "*.csv"); // => "*.csv"
    }
    else if(m_pCurrMgr == pMgrModbusASCII)
    {
        full_name_filepath = QFileDialog::getOpenFileName(this, "Open File", "C:/", "*.txt"); // => "*.txt"
    }
    else
    {
        full_name_filepath = QFileDialog::getOpenFileName(this, "Open File", "C:/", "*.*");
    }

    if(full_name_filepath == "")
    {
        doPrintWithColor(pickTextBrowser(m_pCurrMgr), e_DarkRed, "Loading File Error!");
        return;
    }
    doPrintWithColor(pickTextBrowser(m_pCurrMgr), e_DarkGreen, "#2.Loading File Path: " + full_name_filepath);

    ui->title_txt_filepath->setText(full_name_filepath);
    QStringList list_file_path = full_name_filepath.split("/");
    QString file_name = list_file_path.at(list_file_path.size()-1);

    doPrintWithColor(pickTextBrowser(m_pCurrMgr), e_DarkGreen, "  file_name: " + file_name);

    int sub = full_name_filepath.size() - file_name.size();
    file_path = full_name_filepath.mid(0, sub);
    qDebug() <<  "file_name: " << file_name;

    doPrintWithColor(pickTextBrowser(m_pCurrMgr), e_DarkGreen, "  file_path: " + file_path);
    doPrintWithColor(pickTextBrowser(m_pCurrMgr), e_DarkGreen, "#3.Parse File \"" + file_name + "\" now...");

    //文件解析
    int iPageNum = m_pCurrMgr->parseCfgFile(ui->title_txt_filepath->text());
    if(iPageNum == _ERROR)
    {
        doPrintWithColor(pickTextBrowser(m_pCurrMgr),e_DarkRed, "#3.Loading File Error!");
        return;
    }
    doPrintWithColor(pickTextBrowser(m_pCurrMgr),e_DarkGreen, "page count " + QString::number(iPageNum));

    if((m_pCurrMgr == pMgrModbusSlaver) || (m_pCurrMgr == pMgrPowerDevice) ||
       (m_pCurrMgr == pMgrSnmpServer)   || (m_pCurrMgr == pMgrModbusTcpServer))
    {
        if (iPageNum > 0)
        {
            this->displayContent_1();
        }
        if (iPageNum > 1)
        {
            this->displayContent_2();
        }
    }
    else if(m_pCurrMgr == pMgrMibMapping)
    {
        if(iPageNum > 0)
        {
            this->displayContent_1_mibmapping(type_select_all);
        }
    }
    //doPrintWithColor((m_pCurrMgr == pMgrMibMapping)?ui->tail_txt_log_2:ui->tail_txt_log,
    //                 e_DarkGreen, "# Data Finshed, UI Loading start...");
}


//UI[TAB]数值修改
void MainWindow::onValueChanged(int iItemIndex, int iRegAddr, float fValue)
{    
    qDebug("--- SheetIndex=%d, RegAddr=%04d, ChangeValue=%6.2f ---", iItemIndex, iRegAddr, fValue);

    ST_MODIFY_VALUE_OF_MODBUS pt;
    pt.iPageIndex = iItemIndex;
    pt.index = iRegAddr;
    pt.fValue = fValue;

    if (m_pCurrMgr->modifyValue((void*)&pt))
    {
        //layout_1 更新
    }
    else
    {
        //err log
    }
}

//UI[TAB]数值修改(仅Mapping)
void MainWindow::onValueChanged2(int iEquipId, int iSigType, int iSigId, int iEntry, char* pOid)
{
    qDebug("=== === === === === === === ===");
    qDebug("%04d, %s Value Changed to %04d, %04d", iEquipId, pOid, iSigType, iSigId);
    qDebug("=== === === === === === === ===");

    ST_MODIFY_VALUE_OF_STD_MIB stModify;
    memset(&stModify, 0, sizeof(ST_MODIFY_VALUE_OF_STD_MIB));

    stModify.iEquipId = iEquipId;
    stModify.iSigType = iSigType;
    stModify.iSigId   = iSigId;
    stModify.iEntry   = iEntry;

    memcpy(stModify.pOid, pOid, strlen(pOid));

    bool bRet = m_pCurrMgr->modifyValue((void*)&stModify);
    if (!bRet)
    {
        qDebug("modifyValue error!");
    }
}


//COM端口打开
void MainWindow::on_Btn_openPort_clicked()
{
    if (ui->Btn_openPort->text() == "打开端口")
    {
        if(openMgrPort())
        {
            ui->Btn_openPort->setText("关闭端口");
            this->doPrintWithColor(ui->tail_txt_log, e_DarkGreen, ">>>>>>>>>>>>>>>>>>>>>> Open Port");
            QIcon icon;
            icon.addFile(tr(":/img/NavOverFlow_Break.png"));
            ui->Btn_openPort->setIcon(icon);
            ui->Btn_openPort->setText("关闭端口");
        }
    }
    else
    {
        ui->Btn_openPort->setText("打开端口");
        this->doPrintWithColor(ui->tail_txt_log, e_DarkRed, "Close Port");
        QIcon icon;
        icon.addFile(tr(":/img/NavOverFlow_Start.png"));
        ui->Btn_openPort->setIcon(icon);
        ui->Btn_openPort->setText("打开端口");
        closeMgrPort();
    }
}


//TCP服务打开
void MainWindow::on_Btn_openNetPort_clicked()
{
    if (ui->Btn_openNetPort->text() == "打开服务")
    {
        if (ui->txt_net_port->text() == "") //正则 判断port是否合理
        {
            qDebug("--err-");
        }

        qDebug("---on_Btn_openNetPort_clicked---");
        if(openMgrPort())
        {
            QIcon icon;
            icon.addFile(tr(":/img/NavOverFlow_Break.png"));
            ui->Btn_openNetPort->setIcon(icon);
            ui->Btn_openNetPort->setText("关闭服务");
            this->doPrintWithColor(ui->tail_txt_log, e_DarkGreen, "Modbus-TCP Server Open");
        }
    }
    else
    {
        qDebug("关闭server服务");
        ui->cmb_ListClientIP->clear(); //连接断开，清除客户端Socket显示
        QIcon icon;
        icon.addFile(tr(":/img/NavOverFlow_Start.png"));
        ui->Btn_openNetPort->setIcon(icon);
        ui->Btn_openNetPort->setText("打开服务");
        this->doPrintWithColor(ui->tail_txt_log, e_DarkRed, "Close Net Server");
        closeMgrPort();
    }
}


//打开通用端口
bool MainWindow::openMgrPort()
{    
    ST_PORT_PARAM stParam;
    ST_NETWORK_PORT_PARAM stNetParam;
    bool bRet = false;
    qDebug("MainW打开端口 index %d", ui->title_cmb_workmode->currentIndex());

    switch (ui->title_cmb_workmode->currentIndex())
    {
    case 0: //Modbus-RTU
    case 1: //Modbus-ASCII
    case 3: //YDN23
    {
        qDebug() << "open me!!!";
        stParam.strPortName = ui->cmb_SerialPort->currentText();
        stParam.iBaudRate = ui->cmb_SerialBaudRate->currentText().toInt();
        stParam.iStopBit = ui->cmb_SerialStopBits->currentIndex();
        stParam.iDeviceAddr = ui->txt_com_addr->text().toInt();
        bRet = m_pCurrMgr->openPort((void *)&stParam);
        if(bRet)
        {
            ui->lbl_mid_page1_8->setStyleSheet("color:green");
            ui->lbl_mid_page1_8->setText(ui->cmb_SerialPort->currentText() + " open");
            this->setSerialsDisplay(false);
        }
        else
        {
            ui->lbl_mid_page1_8->setStyleSheet("color:red");
            ui->lbl_mid_page1_8->setText(ui->cmb_SerialPort->currentText() + " open failed!");
        }        
    }
    break;
    case 2: //Modbus-TCP
    case 4: //SNMP
    {
        stNetParam.IP = ui->txt_net_ip->text().toStdString().c_str();
        stNetParam.nPort = ui->txt_net_port->text().toInt();
        qDebug() << "ui获取IP:" << stNetParam.IP
                 << "获取Port:" << stNetParam.nPort;

        bRet = m_pCurrMgr->openPort((void *)&stNetParam);
        if (bRet)
        {
            qDebug("网口打开成功!");
            ui->lbl_mid_page2_status->setStyleSheet("color:green");
            ui->lbl_mid_page2_status->setText("The socket server is listening.");
            //this->setNetworkDisplay(false);
        }
        else
        {
            qDebug("网口打开失败!");
            ui->lbl_mid_page2_status->setStyleSheet("color:red");
            ui->lbl_mid_page2_status->setText("The socket server is listening failed!");
        }
    }
    break;
    case 5: //Mib-Mapping
    default:
        return false;
    }

    return bRet;
}


//关闭通用端口
bool MainWindow::closeMgrPort()
{
    bool bRet = false;

    switch (ui->title_cmb_workmode->currentIndex())
    {
    case 0: //Modbus-RTU
    case 1: //Modbus-ASCII
    case 3: //YDN23
    {
        m_pCurrMgr->closePort();
        ui->lbl_mid_page1_8->setStyleSheet("color:red");
        ui->lbl_mid_page1_8->setText(ui->cmb_SerialPort->currentText() + " close.");
        this->setSerialsDisplay(true);
    }
    break;
    case 2: //Modbus-TCP
    case 4: //SNMP
    {
        m_pCurrMgr->closePort();
        ui->lbl_mid_page1_8->setStyleSheet("color:red");
        ui->lbl_mid_page1_8->setText("The socket server is listening close.");
        this->setNetworkDisplay(true);
    }
    break;
    case 5: //Mib-Mapping
    default:
        return false;
    }
    return bRet;
}



//UI清除日志
void MainWindow::on_Btn_ClearLog_clicked()
{
    ui->tail_txt_log->clear();
}

void MainWindow::on_Btn_ControlEnablePrintlog_clicked()
{
    if (ui->Btn_ControlEnablePrintlog->text() == "停止打印")
    {
        qDebug() << "Changed type: =========================== innnnnnnnnnnnnn";
        ui->Btn_ControlEnablePrintlog->setText("开启打印");
        doPrintWithColor(ui->tail_txt_log_2, e_DarkBlack, "关闭日志打印");
        b_EnablePrintlog = false;
        return;
    }

    if (ui->Btn_ControlEnablePrintlog->text() == "开启打印")
    {
        qDebug() << "Changed type: =========================== innnnnnnnnnnnnn2222";
        ui->Btn_ControlEnablePrintlog->setText("停止打印");
        doPrintWithColor(ui->tail_txt_log_2, e_DarkBlack, "开启日志打印");
        b_EnablePrintlog = true;
        return;
    }
}

//UI Sheet1 show
void MainWindow::displayContent_1()
{
    ui->lbl_Sheet1->show();
    ui->Btn_SelectSheet1->show();
    ui->txt_SelectSheet1->show();

    scroll_1 = new QScrollArea(this);
    scroll_1->setGeometry(30,190,305,205); //Sheet1 Position
    scroll_1->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    scroll_1->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);

    widget_1 = new QWidget(scroll_1);
    layout_1 = new QGridLayout(widget_1);

    QVector<ST_DISPLAY_STRUCT> vc = m_pCurrMgr->getDisplayData(0);
    for (int index = 0; index < vc.length(); index++)
    {
        namelabel *pname = new namelabel(vc.at(index).RegName);
        valuetext *pvalue = new valuetext(0, index, vc.at(index).value);
        layout_1->addWidget(pname, index, 0);
        layout_1->addWidget(pvalue, index, 1);

        connect(pvalue, SIGNAL(valueChanged(int, int, float)),
                this, SLOT(onValueChanged(int, int,float)));
    }

    scroll_1->setWidget(widget_1);
    scroll_1->show();
}


//UI Sheet2 show
void MainWindow::displayContent_2()
{
    ui->lbl_Sheet2->show();
    ui->Btn_SelectSheet2->show();
    ui->txt_SelectSheet2->show();

    scroll_2 = new QScrollArea(this);
    scroll_2->setGeometry(410,190,305,205);
    scroll_2->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    scroll_2->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);

    widget_2 = new QWidget(scroll_2);
    layout_2 = new QGridLayout(widget_2);

    QVector<ST_DISPLAY_STRUCT> vc = m_pCurrMgr->getDisplayData(1);
    for (int index = 0; index < vc.length(); index++)
    {
        valuetext *pvalue = new valuetext(1, index, vc.at(index).value);
        namelabel *pname = new namelabel(vc.at(index).RegName);
        layout_2->addWidget(pname, index, 0);
        layout_2->addWidget(pvalue, index, 1);

        connect(pvalue, SIGNAL(valueChanged(int, int, float)),
                this, SLOT(onValueChanged(int, int, float)));
    }

    scroll_2->setWidget(widget_2);
    scroll_2->show();
}


//查找mib文件中指定的typeId
void MainWindow::on_cmb_head3_type_currentIndexChanged(const QString &arg1)
{
    ui->cmb_head3_equipid->clear();
    qDebug() << "Changed type: " << arg1;

    if(arg1 == "全部")
    {
        int m = 0;
        while(this->szEquipListBuf[m][0] != 0){
            ui->cmb_head3_equipid->addItem(QString::number(this->szEquipListBuf[m][1]));
            m++;
        };
    }
    else
    {
        int k = arg1.mid(0,1).toInt();
        int m = 0;
        while(this->szEquipListBuf[m][0] != 0){
            if (k == this->szEquipListBuf[m][0])
            {
                ui->cmb_head3_equipid->addItem(QString::number(this->szEquipListBuf[m][1]));
            }
            m++;
        };
    }
}

void MainWindow::on_cmb_head3_equipid_currentTextChanged(const QString &arg1)
{
    qDebug(">>>>>>>>>>>>>> equipid_currentTextChanged ???");

    //if(trigger_count == 1)
    //{
    //    trigger_count++;
    //    return;
    //}
    int TypeId = ui->cmb_head3_type->currentIndex() + 1;
    int EquipId = arg1.toInt();
    qDebug("<<<<< typeId=%d, equipId=%d", TypeId, EquipId);
}

void MainWindow::loading_Mapping_TypeId_Style(SelectType type, int EquipIDBuf[][3])
{
    QIcon iconGreen;
    QIcon iconRed;
    iconGreen.addFile(tr(":/img/playbck_02.gif"));
    iconRed.addFile(tr(":/img/playbck_05.gif"));

    doPrintWithColor(ui->tail_txt_log_2, e_DarkBlack, "+=======================+");
    int index = 0;
    while(EquipIDBuf[index][0] != 0){
        doPrintWithColor(ui->tail_txt_log_2, e_DarkBlack,
            "+ " + QString::number(szEquipListBuf[index][1]) +  " | " + QString::number(szEquipListBuf[index][2]) + "  +");
        qDebug() << " == " << szEquipListBuf[index][1] << " == " << szEquipListBuf[index][2];
        index++;
    };
    qDebug() << "index: " << index;
    doPrintWithColor(ui->tail_txt_log_2, e_DarkBlack, "+=======================+");

    ui->cmb_head3_type->clear();

    if((type == type_init) || (index == 0))
    {
        ui->cmb_head3_type->addItem(iconRed, QString::asprintf("全部"));
        ui->cmb_head3_type->addItem(iconRed, QString::asprintf("2-PDU"));
        ui->cmb_head3_type->addItem(iconRed, QString::asprintf("3-AC"));
        ui->cmb_head3_type->addItem(iconRed, QString::asprintf("4-UPS"));
        ui->cmb_head3_type->addItem(iconRed, QString::asprintf("5-ENV"));
        ui->cmb_head3_type->addItem(iconRed, QString::asprintf("6-BAT"));
        ui->cmb_head3_type->addItem(iconRed, QString::asprintf("7-DACC"));
        ui->cmb_head3_type->addItem(iconRed, QString::asprintf("8-AMM"));
        ui->cmb_head3_type->addItem(iconRed, QString::asprintf("9-Other"));
    }
    else if(type == type_select_all)
    {
        const int num_type_list = 10;
        bool flag;

        for (int i = 0; i < num_type_list; i++)
        {
            switch (i) {
            case 0:
                ui->cmb_head3_type->addItem(iconGreen, QString::asprintf("全部"));
                break;
            case 2:
                flag = false;
                for(int j=0;j<index;++j)
                {
                    if(EquipIDBuf[j][0] == 2)
                       flag = true;
                }
                ui->cmb_head3_type->addItem((flag)?iconGreen:iconRed, QString::asprintf("2-PDU"));
                break;
            case 3:
                flag = false;
                for(int j=0;j<index;++j)
                {
                    if(EquipIDBuf[j][0] == 3)
                       flag = true;
                }
                ui->cmb_head3_type->addItem((flag)?iconGreen:iconRed, QString::asprintf("3-AC"));
                break;
            case 4:
                flag = false;
                for(int j=0;j<index;++j)
                {
                    if(EquipIDBuf[j][0] == 4)
                       flag = true;
                }
                ui->cmb_head3_type->addItem((flag)?iconGreen:iconRed, QString::asprintf("4-UPS"));
                break;
            case 5:
                flag = false;
                for(int j=0;j<index;++j)
                {
                    if(EquipIDBuf[j][0] == 5)
                       flag = true;
                }
                ui->cmb_head3_type->addItem((flag)?iconGreen:iconRed, QString::asprintf("5-ENV"));
                break;
            case 6:
                flag = false;
                for(int j=0;j<index;++j)
                {
                    if(EquipIDBuf[j][0] == 6)
                       flag = true;
                }
                ui->cmb_head3_type->addItem((flag)?iconGreen:iconRed, QString::asprintf("6-BAT"));
                break;
            case 7:
                flag = false;
                for(int j=0;j<index;++j)
                {
                    if(EquipIDBuf[j][0] == 7)
                       flag = true;
                }
                ui->cmb_head3_type->addItem((flag)?iconGreen:iconRed, QString::asprintf("7-DACC"));
                break;
            case 8:
                flag = false;
                for(int j=0;j<index;++j)
                {
                    if(EquipIDBuf[j][0] == 8)
                       flag = true;
                }
                ui->cmb_head3_type->addItem((flag)?iconGreen:iconRed, QString::asprintf("8-AMM"));
                break;
            case 9:
                flag = false;
                for(int j=0;j<index;++j)
                {
                    if(EquipIDBuf[j][0] == 9)
                       flag = true;
                }
                ui->cmb_head3_type->addItem((flag)?iconGreen:iconRed, QString::asprintf("9-Other"));
                break;
            default:
                break;
            }
        }
    }
}


//UI Sheet1 mib node mapping show
int MainWindow::displayContent_1_mibmapping(SelectType select_type)
{
    ui->stackedWidget_tail->setCurrentIndex(1);

    scroll_3 = new QScrollArea(this);
    scroll_3->setGeometry(19,188,720,270);
    scroll_3->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    scroll_3->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);

    widget_3 = new QWidget(scroll_3);
    layout_3 = new QGridLayout(widget_3);

    int nCount = 0; //get signal number
    void* pv;//get signal info

    if(select_type == type_select_all)
    {
        memset(this->szEquipListBuf, 0, sizeof(this->szEquipListBuf));
        pv = m_pCurrMgr->getDisplayData2(&nCount, szEquipListBuf); //获取下拉框列表数据
        doPrintWithColor(ui->tail_txt_log_2, e_DarkBlack, "Select All File Data");
        this->loading_Mapping_TypeId_Style(type_select_all, this->szEquipListBuf);
        ui->cmb_head3_equipid->setCurrentIndex(0);
    }
    else if(type_select_specify)
    {
        int TypeId = ui->cmb_head3_type->currentIndex() + 1;
        int EquipId = ui->cmb_head3_equipid->currentText().toInt();
        pv = m_pCurrMgr->getDisplayRequestList(&nCount, TypeId, 1, &EquipId);
    }

    if (pv == NULL)
    {
        qDebug() << "empty";
        return true;
    }

    ST_DISPLAY_MIB_NODE *pm = (ST_DISPLAY_MIB_NODE*)(pv);
    qDebug() << "加载信号数: " + QString::number(nCount) + " 个";
    doPrintWithColor(ui->tail_txt_log_2, e_DarkBlack, "  Loading Total " + QString::number(nCount) + " Main Signals");

    //    for(int k = 0; k < nCount; k++)
    //    {
    //        qDebug("__st[%d] strSigName=%s\toid=[%32s]\tEquipTypeId=%d\tEquipId=%d\tSigType=%d\tSigId=%d",
    //            k,
    //            pm[k].strSigENName,
    //            pm[k].oid,
    //            pm[k].EquipTypeId,
    //            pm[k].EquipId,
    //            pm[k].SigType,
    //            pm[k].SigId);
    //    }

    for (int index = 0; index < nCount; index++)
    {
        QCheckBox *p_chk_flag = new QCheckBox();
        namelabel *p_lbl_equipid = new namelabel(QString::number(pm[index].EquipId), 40, 25);
        //p_lbl_equipid->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

        namelabel *p_lbl_zh_name = new namelabel(pm[index].strSigCHName, 170, 25);
        namelabel *p_lbl_en_name = new namelabel(pm[index].strSigENName, 190, 25);
        namelabel *p_lbl_oid     = new namelabel(pm[index].oid, 95, 25);

        combox1 *p_cmb_sigtype   = new combox1(pm[index].EquipId, pm[index].oid, pm[index].SigType, pm[index].SigId, pm[index].Entry, p_chk_flag);
        p_cmb_sigtype->installEventFilter(this);

        valuetext2 *p_txt_sigid  = new valuetext2(pm[index].EquipId, pm[index].oid, pm[index].SigType, pm[index].SigId, pm[index].Entry, p_chk_flag);
        p_txt_sigid->installEventFilter(this);

        layout_3->addWidget(p_chk_flag,    index, 0);
        layout_3->addWidget(p_lbl_equipid, index, 1);
        layout_3->addWidget(p_lbl_zh_name, index, 2);
        layout_3->addWidget(p_lbl_en_name, index, 3);
        layout_3->addWidget(p_lbl_oid,     index, 4);
        layout_3->addWidget(p_cmb_sigtype, index, 5);
        layout_3->addWidget(p_txt_sigid,   index, 6);

        connect(p_cmb_sigtype, SIGNAL(comboxChanged(int, int, int, int, char*)),
                this, SLOT(onValueChanged2(int, int, int, int, char*)));

        connect(p_txt_sigid, SIGNAL(valueChanged2(int, int, int, int, char*)),
                this, SLOT(onValueChanged2(int, int, int, int, char*)));

        //qDebug() << index << "/" << nCount;//hmi显示

    }
    scroll_3->setWidget(widget_3);
    scroll_3->show();

    ui->lbl_page2_1->show();
    ui->lbl_page2_2->show();
    ui->lbl_page2_3->show();
    ui->lbl_page2_4->show();
    ui->lbl_page2_5->show();
    ui->lbl_page2_7->show();

    qDebug() << "::UI 完成";//hmi显示
    doPrintWithColor(ui->tail_txt_log_2, e_DarkBlack, "Loading Finshed");

    this->time_end = QDateTime::currentMSecsSinceEpoch();

    qint64 df = this->time_end - this->time_start;
    if (df > 0)
    {
        doPrintWithColor(ui->tail_txt_log_2, e_DarkBlack, "Total use time " + QString::number(df) + " ms");
    }
    doPrintWithColor(ui->tail_txt_log_2, e_DarkBlack, "======================================");

    delete(pv);
    return true;
}

/*
 * Select ValueName from Sheet1
 */
void MainWindow::on_Btn_SelectSheet1_clicked()
{
    int index = m_pCurrMgr->findByRegName(ui->txt_SelectSheet1->toPlainText(), m_iSamplerCurrIndex_sheet1 + 1, 0);

    qDebug() << index << " " << m_iSamplerCurrIndex_sheet1;

    if(-1 != m_iSamplerCurrIndex_sheet1)
    {        
        //Before Restore Sheet Style
        namelabel* pname = (namelabel *)layout_1->itemAtPosition(m_iSamplerCurrIndex_sheet1, 0)->widget();
        pname->setStyleSheet("background-color: rgb(255, 255, 255); \
                              border: 1px solid #000000; color:black");
    }

    m_iSamplerCurrIndex_sheet1 = index;

    if (-1 != index)
    {        
        namelabel* pname = (namelabel *)layout_1->itemAtPosition(index,0)->widget();
        pname->setStyleSheet("background-color: rgb(0, 255, 255); \
                              border: 1px solid #000000; color:black");

        scroll_1->verticalScrollBar()->setValue(index*31);
    }
}

/*
 * Select ValueName from Sheet2
 */
void MainWindow::on_Btn_SelectSheet2_clicked()
{
    int index = m_pCurrMgr->findByRegName(ui->txt_SelectSheet2->toPlainText(), m_iSamplerCurrIndex_sheet2 + 1, 1);

    if(-1 != m_iSamplerCurrIndex_sheet2)
    {

        namelabel* pname = (namelabel *)layout_2->itemAtPosition(m_iSamplerCurrIndex_sheet2, 0)->widget();
        pname->setStyleSheet("background-color: rgb(255, 255, 255); \
                             border: 1px solid #000000;color:black");
    }

    m_iSamplerCurrIndex_sheet2 = index;
    if (-1 != index)
    {        
        namelabel* pname = (namelabel *)layout_2->itemAtPosition(index,0)->widget();
        pname->setStyleSheet("background-color: rgb(0, 255, 255); \
                              border: 1px solid #000000;color:black");

        scroll_2->verticalScrollBar()->setValue(index*31);
    }
}

#define  MappingItemIndex   2

//查找mib文件中指定的: SigName信号名称
void MainWindow::on_Btn_head3_filter_clicked()
{
    this->time_start = QDateTime::currentMSecsSinceEpoch();
    static int TypeId = 0;
    static int EquipId = 0;
    if((TypeId != ui->cmb_head3_type->currentIndex() + 1) || (EquipId != ui->cmb_head3_equipid->currentText().toInt()))
    {
        TypeId  = ui->cmb_head3_type->currentIndex() + 1;
        EquipId = ui->cmb_head3_equipid->currentText().toInt();        
        doPrintWithColor(ui->tail_txt_log_2, e_DarkBlack,
            "### Reqest Select Data Type_" + QString::number(TypeId) + ", EquipId_" + QString::number(EquipId));
        this->displayContent_1_mibmapping(type_select_specify);
        m_iSamplerCurrIndex_sheet3 = -1;
        return;
    }
    int index = m_pCurrMgr->findByRegName(ui->txt_head3_selectsheet2->text().trimmed(), m_iSamplerCurrIndex_sheet3 + 1, 0);

    qDebug() << "================================================================";
    qDebug() << "index: " << index << " m_iSamplerCurrIndex_sheet3: " << m_iSamplerCurrIndex_sheet3;   

    if(m_iSamplerCurrIndex_sheet3 != -1)
    {
        //Before Restore Sheet Style
        namelabel* pname = (namelabel *)layout_3->itemAtPosition(m_iSamplerCurrIndex_sheet3, MappingItemIndex)->widget();
        pname->setStyleSheet("background-color: rgb(255, 255, 255); \
                             border: 1px solid #000000; color:black");
    }

    m_iSamplerCurrIndex_sheet3 = index;

    if (-1 != index)
    {
        namelabel* pname = (namelabel *)layout_3->itemAtPosition(index, MappingItemIndex)->widget();
        pname->setStyleSheet("background-color: rgb(0, 255, 255); \
                              border: 1px solid #000000; color:black");

        scroll_3->verticalScrollBar()->setValue(index*31);
    }
}

//查找mib文件中全部信息
void MainWindow::on_Btn_head3_selectAll_clicked()
{
    this->time_start = QDateTime::currentMSecsSinceEpoch();
    this->displayContent_1_mibmapping(type_select_all);
}


//refresh
void MainWindow::onRefreshDisplay(int iPageIndex, int iIndex, float fValue)
{
    if (iPageIndex == 0)
    {
        static int iPrevIndex = 0;
        valuetext* pValue = (valuetext *)layout_1->itemAtPosition(iIndex,1)->widget();
        //pValue->setText(QString("%1").arg(fValue));
        pValue->setValue(fValue);

        namelabel* pname = (namelabel *)layout_1->itemAtPosition(iPrevIndex,0)->widget();
        pname->setStyleSheet("background-color: rgb(255, 255, 255); \
                              border: 1px solid #000000;color:black");

        pname = (namelabel *)layout_1->itemAtPosition(iIndex,0)->widget();
        pname->setStyleSheet("background-color: rgb(255, 255, 255); \
                              border: 1px solid #000000;color:red");
        iPrevIndex = iIndex;

        scroll_1->verticalScrollBar()->setValue(iIndex*31);
    }
    else if(iPageIndex == 1)
    {
        static int iPrevIndex1 = 0;
        valuetext* pValue = (valuetext *)layout_2->itemAtPosition(iIndex,1)->widget();        
        //pValue->setText(QString("%1").arg(fValue));
        pValue->setValue(fValue);

        namelabel* pname = (namelabel *)layout_2->itemAtPosition(iPrevIndex1,0)->widget();
        pname->setStyleSheet("background-color: rgb(255, 255, 255); \
                              border: 1px solid #000000;color:black");

        pname = (namelabel *)layout_2->itemAtPosition(iIndex,0)->widget();
        pname->setStyleSheet("background-color: rgb(255, 255, 255); \
                              border: 1px solid #000000;color:red");
        iPrevIndex1 = iIndex;

        scroll_2->verticalScrollBar()->setValue(iIndex*31);
    }
}

void MainWindow::on_Btn_ShowMode_clicked()
{
    m_pCurrMgr->getDisplayData(0);

    if (ui->Btn_ShowMode->text() == "十六进制")
    {
        ui->Btn_ShowMode->setText("十进制");
        m_iDisplayType = _DEC_;
    }
    else
    {
        ui->Btn_ShowMode->setText("十六进制");
        m_iDisplayType = _HEX_;
    }
}


void MainWindow::on_Btn_head3_file_output_clicked()
{
    //默认初始化查找目录
    //QString strFilePath = QFileDialog::getExistingDirectory(this, "Open File", "C:/");
    //qDebug()<< "FilePath Output to: " << strFilePath;

    QString FileName = "";
    m_pCurrMgr->OutputFile(file_path, &FileName);

    QString TipFullFile = "文件: \"" + file_path + FileName + "\" 导出成功!!";
    QMessageBox::information(0, "tip", TipFullFile, QMessageBox::Ok);
}


void MainWindow::on_Btn_ClearLog_2_clicked()
{
    ui->tail_txt_log_2->clear();    
}


//保存日志(Page1)
void MainWindow::on_Btn_SaveLog_clicked()
{
    //选择目录 => 导出excel
    QString strFilePath = QFileDialog::getExistingDirectory(this, "Open File", "C:/");
    qDebug( )<< "FilePath Output to : " << strFilePath << "/smartQT_log.txt";
    QFile fileout(strFilePath + "/SmartQT_log.txt");

    //文件载入
    if(!fileout.open(QIODevice::ReadWrite))
    {
        return;
    }
    fileout.write("=================================================");
    fileout.flush();

    qDebug() << ui->tail_txt_log->toPlainText();
    fileout.write(ui->tail_txt_log->toPlainText().toStdString().c_str());
    fileout.flush();

    this->doPrintWithColor(ui->tail_txt_log, e_DarkGreen, "导出日志完成!");

    QString TipFullFile = "日志文件: \"" + strFilePath + "\SmartQT_log.txt" + "\" 导出成功!!";
    QMessageBox::information(0, "Tip", TipFullFile, QMessageBox::Ok);
}

//保存日志(Page2)
void MainWindow::on_Btn_SaveLog_2_clicked()
{
    //选择目录 => 导出excel
    QString strFilePath = QFileDialog::getExistingDirectory(this, "Open File", "C:/");
    qDebug( )<< "FilePath Output to : " << strFilePath << "/smartQT_log.txt";
    QFile fileout(strFilePath + "/smartQT_log.txt");

    //文件载入
    if(!fileout.open(QIODevice::ReadWrite))
    {
        return;
    }
    fileout.write("=================================================");
    fileout.flush();

    //fileout.write(QString("Num:%1-").arg(2222).toUtf8());
    qDebug() << ui->tail_txt_log_2->toPlainText();
    fileout.write(ui->tail_txt_log_2->toPlainText().toStdString().c_str());
    fileout.flush();

    this->doPrintWithColor(ui->tail_txt_log_2, e_DarkGreen, "导出日志完成!");
    QString TipFullFile = "日志文件: \"" + strFilePath + "\SmartQT_log.txt" + "\" 导出成功!!";
    QMessageBox::information(0, "Tip", TipFullFile, QMessageBox::Ok);
}


void MainWindow::on_chk_page2_Synchronize_clicked(bool checked)
{
    m_pCurrMgr->SyncmodifyValue(checked);
    if (checked == true)
    {
        this->doPrintWithColor(ui->tail_txt_log_2, e_DarkGreen, "Syncmodify Open");
    }
    else
    {
        this->doPrintWithColor(ui->tail_txt_log_2, e_DarkGreen, "Syncmodify Close");
    }
}
