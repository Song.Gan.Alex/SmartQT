#ifndef CSNMPDATAMANAGER_H
#define CSNMPDATAMANAGER_H

#include <QFile>

#include "include/snmpversion.h"
#include "include/snmpvardefine.h"
#include "include/snmperrcode.h"

#include "MgrDataBase.h"
#include "comm/icommSnmp.h"

#define EXCEL_SNMP_ROW_MODEL_TYPE   0
#define EXCEL_SNMP_ROW_VALUE_TYPE   1
#define EXCEL_SNMP_ROW_VALUE_NAME   2
#define EXCEL_SNMP_ROW_VALUE_INDEX  3

//0.OID结构树
//typedef struct _SNMP_MIB_TREE_NODE_ SNMP_MIB_TREE_NODE;
//struct _SNMP_MIB_TREE_NODE_
//{
//    SNMP_MIB_TREE_NODE *pNext;  // List of 'sibling' subtrees
//    SNMP_MIB_TREE_NODE *pChild; // List of 'child' subtrees
//    oid     ulOidValue;
//    void*   pContent;
//    QString szOidName;
//    int     TVflag; //T/V
//    int     RWflag; //R/W
//};


/* 三个结构体，
 * ST_SNMP_SHEET_CONTENT 用于读取excel的全字段数据
 * ST_DISPLAY_STRUCT 用于QT界面中的人机显示
 * 树结构 用于OID点的存储
*/
typedef struct
{
    QString strName;
    QString strOID;
    int     iScale;
    float   fValue;
    int     TVflag; //T/V
    int     RWflag; //R/W
}ST_SNMP_SHEET_CONTENT;


typedef struct
{
    struct {
        BYTE    asnType;
        BYTE    length; //len[4]
        union reqid
        {
            ulong ul;
            BYTE  szbyte[4];
        }obj;
    } stRequestId;

    BYTE   error_status; //len[1]
    BYTE   error_index;  //len[1]

    struct{

        BYTE   asnType; //len[1] BER_TYPE_SEQUENCE
        BYTE   length;  //len[1]

        struct{
            BYTE   asnType; //len[1] BER_TYPE_SEQUENCE
            BYTE   length;  //len[1]
            struct {
                BYTE    asnType; //
                BYTE    length; //OID的字符长度
                int     number; //OID的有效个数
                oid     szobj[MAX_OID_LEN]; //oid[MAX_OID_LEN]
            } stOid;

            struct {
                BYTE    asnType;
                BYTE    length;
                int     objValue; //:->
            } v;

        }stData; //oid + value variable_binding

    } st_variable_binding; //get_request

} varData;

typedef struct
{
    BYTE     asnType; // == asnType::BER_TYPE_SEQUENCE
    BYTE     length;

    struct {
        BYTE    asnType; //asnType::BER_TYPE_INTEGER
        BYTE    length;
        BYTE    obj; //SNMP版本 len[1]
    } stVersion;

    struct {
        BYTE    asnType; //共同体
        BYTE    length;
        QByteArray  obj; // ==> (pubic/private)
    } stCommunity;

    struct {
        BYTE    asnType;
        BYTE    length;
        varData   obj; // ==> _data
    } stVariable;

} pdu_snmp_protocal;

//type-length_value

class MgrSNMPServer: public MgrDataBase
{
    Q_OBJECT
public:
    explicit MgrSNMPServer(qint16 listenPort);
    ~MgrSNMPServer();

    QString getMgrName();

    int parseCfgFile(QString strFilePath);

    QVector<ST_DISPLAY_STRUCT> getDisplayData(int index);
    void* getDisplayData2(int* pCount);

    //void modifyValue(int iIndex, float fValue, int iItemIndex);
    //void modifyValue2(int nEquipId, int nSigType, int nSigId, char oid[]);
    bool modifyValue(void* pt);

    int findByRegName(QString strRegName, int iBeginIndex, int iPageIndex);

    int OutputFile(QString strFilePath, OUT QString *pFileName);

public slots:
    void processRequest(QByteArray RecvData);
    void notify(EMessageType type, QString str);

private:
    static MgrSNMPServer* m_instance_mgrsnmp;
    QString MgrName = "SNMP";

    //SNMP_MIB_TREE_NODE *g_MibTreeRoot;
    QVector<ST_SNMP_SHEET_CONTENT> m_SnmpWorkSheet;

    int getFileContent(QString strFilePath);
    int parseInfo(QVariant var);

    //void insertVertor(QVariantList rowData, QVector<ST_CFG_FILE_STRUCT> &vc);
    //int findByRegAddr(int iRegAddr, QVector<ST_CFG_FILE_STRUCT> &vcData);

    void clearDataBuff();

    QByteArray setWordData(int iRegAddr, int iValue, int iItemIndex);
    QByteArray getWordData(int iBegin, int iNum, int iItemIndex);

};

#endif // CSNMPDATAMANAGER_H
