/*******************************************
 *  parsefile/MgrSerialSlaver.cpp
 *
 *  (C) 2021  Vertiv.co
 *******************************************/

#include "MgrModbusASCII.h"
#include <QFile>

MgrModbusASCII::MgrModbusASCII()
{
    m_pCurrComm = new iCommSerial;

    connect(m_pCurrComm,SIGNAL(ePrintMessage(EMessageType,QString)), this,SLOT(notify(EMessageType, QString)));
    connect(m_pCurrComm,SIGNAL(ePrintData(QByteArray,QString)), this,SIGNAL(mgrReadWriteData(QByteArray,QString)));
    connect(m_pCurrComm,SIGNAL(eGetData(QByteArray)), this,SLOT(processRequest(QByteArray)));
}

MgrModbusASCII::~MgrModbusASCII()
{
    clearDataBuff();
}

QString MgrModbusASCII::getMgrName()
{
    return this->MgrName;
}

void MgrModbusASCII::clearDataBuff()
{
    for(int i = 0; i < m_ListPdu.length();i++)
    {
        m_ListPdu[i].requestPdu.clear();
        m_ListPdu[i].respondPdu.clear();
    }
    m_ListPdu.clear();
}

void MgrModbusASCII::notify(EMessageType type, QString str)
{
    qDebug() << "emit >>>";
    emit mgrMessageTip(mgr_type_ascii, type, str);
}

QVector<ST_DISPLAY_STRUCT> MgrModbusASCII::getDisplayData(int index)
{
//    QVector<ST_DISPLAY_STRUCT> qvRet;
//    qvRet.clear();

//    QVector<ST_CFG_MODBUS_RTU_SIG>::iterator iter = m_workSheet[index].vcData.begin();
//    for(;iter < m_workSheet[index].vcData.end();iter++)
//    {
//        ST_DISPLAY_STRUCT stTemp;
//        stTemp.RegName = iter->RegName;
//        stTemp.value = iter->value;
//        qDebug() << iter->RegName << "  " << iter->value;
//        qvRet.append(stTemp);
//    }
//    return qvRet;
}

void *MgrModbusASCII::getDisplayData2(int *pCount)
{
   Q_UNUSED(pCount);
   return nullptr;
}

void MgrModbusASCII::processRequest(QByteArray RecvData)
{
    qDebug() << "=============================================================";
}


int MgrModbusASCII::findByRegName(QString strRegName, int iBeginIndex, int iPageIndex)
{
//    for(int i = iBeginIndex; i < m_workSheet.at(iPageIndex).vcData.length();i++)
//    {
//        if (m_workSheet.at(iPageIndex).vcData.at(i).RegName.indexOf(strRegName) >= 0)
//        {
//            return i;
//        }
//    }
//    return -1;
}


int MgrModbusASCII::OutputFile(QString strFilePath, QString *pFileName)
{
    Q_UNUSED(strFilePath);
    return 0;
}


bool MgrModbusASCII::modifyValue(void *pt)
{
    return true;
}

// 1.parse file
int MgrModbusASCII::parseCfgFile(QString strFilePath)
{
    clearDataBuff();
    return getFileContent(strFilePath);
}

// 3.process txt line
int MgrModbusASCII::processLine(QString strLine, int index)
{
    QString strPrint = "[" + QString::number(index) + "] " + strLine + "\n";
    qDebug() << strPrint;
    notify(type_infoNotify, strPrint);

    ST_PDU_CONTENT pdu;
    int Rem = (index/2);
    int Mod = (index%2);

//    if(Mod == 1) //请求帧
//    {
//        pdu.index = 1;
//        pdu.total++;
//        pdu.requestPdu = strLine;
//    }
//    else
//    {

//    }

}

// 2.loading txt file
int MgrModbusASCII::getFileContent(QString strFilePath)
{
    qDebug() << "#2.txt File Loading...";
    int pageCount = _ERROR;

    QFile file(strFilePath);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        return pageCount;
    }

    QTextStream in(&file);
    QString line = in.readLine();

    int i = 0;
    while (!line.isNull())
    {
        this->processLine(line, ++i);
        line = in.readLine();
    }

    pageCount = 1; // only one page
    return pageCount;
}




