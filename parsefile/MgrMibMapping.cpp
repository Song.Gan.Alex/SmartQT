/*******************************************
 *  parsefile/MgrMibMapping.cpp
 *
 *  (C) 2022  Vertiv.co
 *******************************************/

#include "MgrMibMapping.h"
#include <QtAlgorithms>
#include <QDateTime>
#include <QTextCodec>
#include "./include/transferstdmib.h"

MgrMibMapping::MgrMibMapping()
{
    this->clearDataBuff();
    this->b_Sync = true;
}

MgrMibMapping::~MgrMibMapping()
{
    this->clearDataBuff();
}


QVector<ST_COMM_PORT_INFO> MgrMibMapping::getPortInfo()
{
    QVector<ST_COMM_PORT_INFO> VRet;
    VRet.clear();
    ST_COMM_PORT_INFO tmp = {"",""};
    VRet.append(tmp);
    return VRet;
}

bool MgrMibMapping::openPort(void *pPortParam)
{
    Q_UNUSED(pPortParam);
    return false;
}

void MgrMibMapping::closePort()
{
    return;
}

void MgrMibMapping::notify(EMessageType type, QString str)
{
    emit mgrMessageTip(mgr_type_mib_mapping, type, str);
}

/*=======================================================================*
 * FUNCTION : trimWholeLine
 * PURPOSE  : 去除字符串空格和TAB键
 * ARGUMENTS:
 * RETURN   :
 * COMMENTS :
 * CREATOR  : SG.Alex                          DATE: 2022.01.18 22:59
 *=======================================================================*/
void MgrMibMapping::trimWholeLine(char *strIn, char *strOut)
{
    int i = 0;
    int j = 0;

    for(;i<strlen(strIn);++i)
    {
        if((strIn[i] == '\t') || (strIn[i] == ' '))
        {
            continue;
        }
        else
        {
            strOut[j++] = strIn[i];
        }
    }
    strOut[j] = '\0';

    return;
}


QString MgrMibMapping::getMgrName()
{
    qDebug() << "MgrName:" << this->MgrName;

    return this->MgrName;
}


int MgrMibMapping::parseCfgFile(QString strFilePath)
{
    this->clearDataBuff();

    return this->getFileContent(strFilePath);
}

int MgrMibMapping::EquipId2EquipTypeId(int EquipId)
{
    QVector<ST_DISPLAY_MIB_NODE>::iterator iter = m_MibNodeMappingWorkSheet.begin();

    for(int i=0; iter != m_MibNodeMappingWorkSheet.end(); iter++, i++)
    {
        if(EquipId == iter->EquipId)
        {
            return iter->EquipTypeId;
        }
        continue;
    }
    return _ERROR;
}


bool MgrMibMapping::modifyValue(void *pt)
{
    ST_MODIFY_VALUE_OF_STD_MIB* p = (ST_MODIFY_VALUE_OF_STD_MIB*)pt;

    qDebug("Fix Mapping EquipId=%d SigType=%d SigId=%d Entry=%d oid[%s]",
           p->iEquipId,
           p->iSigType,
           p->iSigId,
           p->iEntry,
           p->pOid);

    int EquipTypeId = EquipId2EquipTypeId(p->iEquipId);

    qDebug() << "b_Synchronize " << this->b_Sync
             << "EquipTypeId " << EquipTypeId;

    QVector<ST_DISPLAY_MIB_NODE>::iterator iter = m_MibNodeMappingWorkSheet.begin();

    if (this->b_Sync) //根据equiptypeid同步
    {
        for(int i=0; iter != m_MibNodeMappingWorkSheet.end(); iter++, i++)
        {
            if (iter->EquipTypeId == EquipTypeId)
            {
                if(iter->Entry == p->iEntry) //主路和分支
                {
                    if(!memcmp(iter->oid, p->pOid, sizeof(iter->oid)))
                    {
                        if(p->iSigId == 0)
                        {
                            //emit -> ui显示
                            char szInfo[254] = {0};
                            sprintf(szInfo, "[Info] Finished Fix EquipId[%d] SigName[%s] sigType from %d to %d",
                                        iter->EquipId, iter->strSigENName, iter->SigType, p->iSigType);
                            notify(type_infoNotify, szInfo);

                            iter->SigType = p->iSigType;
                        }
                        else
                        {
                            //emit -> ui显示
                            char szInfo[254] = {0};
                            sprintf(szInfo, "[Info] Finished Fix EquipId[%d] SigName[%s] SigId from %d to %d",
                                        iter->EquipId, iter->strSigENName, iter->SigId, p->iSigId);
                            notify(type_infoNotify, szInfo);

                            iter->SigId = p->iSigId;
                        }

                        iter->Flag = 1;

                        //emit +++ ui显示
                    }
                }
            }
        }
    }
    else //不同步
    {
        for(int i=0; iter != m_MibNodeMappingWorkSheet.end(); iter++, i++)
        {
            if (iter->EquipId == p->iEquipId)
            {
                if(iter->Entry == p->iEntry) //主路和分支
                {
                    if(!memcmp(iter->oid, p->pOid, sizeof(iter->oid)))
                    {
                        if(p->iSigId == 0)
                        {
                            //emit -> ui显示
                            char szInfo[254] = {0};
                            sprintf(szInfo, "[Info] Finished Fix EquipId[%d] SigName[%s] sigType from %d to %d",
                                        iter->EquipId, iter->strSigENName, iter->SigType, p->iSigType);
                            notify(type_infoNotify, szInfo);

                            iter->SigType = p->iSigType;
                        }
                        else
                        {
                            //emit -> ui显示
                            char szInfo[254] = {0};
                            sprintf(szInfo, "[Info] Finished Fix EquipId[%d] SigName[%s] SigId from %d to %d",
                                        iter->EquipId, iter->strSigENName, iter->SigId, p->iSigId);
                            notify(type_infoNotify, szInfo);

                            iter->SigId = p->iSigId;
                        }

                        iter->Flag = 1;

                        return true;
                    }
                }
            }
        }
    }

    //m_MibMappingView 也需要更新一下+++??

    return true;
}

void MgrMibMapping::SyncmodifyValue(bool b_Synchronize)
{
    this->b_Sync = b_Synchronize;
    qDebug() << "this->b_Sync " << this->b_Sync;
    return;
}



/**
 * @brief MgrMibMapping::getDisplayRequestList 根据筛选获取数组[a]中数组[b]和键值数组[c],作用于返回UI搜索の显示
 *
 * !!注意：由于UI界面加载的性能,获取全部数据和设备类型列表函数不返回分支的加载,分支的加载将在getDisplayRequestList()中实现!!
 * !! 同时不对分支的 TableIndex EquipId BranchIndex 加载!!
 *
 * @param  int* pCount
 * @param  int request_type
 * @param  int request_equip_num
 * @param  void* request_equip_list
 *
 * @return
 */
void *MgrMibMapping::getDisplayRequestList(int* pCount, int request_type, int request_equip_num, void* request_equip_list)
{    
    m_KeyValueSheet.clear();
    m_MibMappingView.clear();

    //出现要求信号的个数
    int nCount = 0;
    int* plist = (int*)request_equip_list;

    QVector<ST_DISPLAY_MIB_NODE>::iterator itor = m_MibNodeMappingWorkSheet.begin();

    for(int i=0; itor != m_MibNodeMappingWorkSheet.end(); itor++, i++) //遍历全部的信号
    {
        if ((request_type != itor->TypeId) && (request_type != 1))
        {
            continue;
        }

        for(int j=0; j < request_equip_num; ++j)
        {
            qDebug() << "request number " << request_equip_num << " equipid " << *(plist+j)
                     << " -- search equipid " << itor->EquipId;

            if ((itor->TypeId == 2) && (itor->oid[4] == '2')) // =>fpdu
            {
                if ((itor->oid[8] == '1') || (itor->oid[8] == '2') || (itor->oid[8] == '3'))
                {
                    continue;
                }
            }

            if(itor->EquipId == *(plist+j))
            {
                QString str = QString::fromUtf8(itor->strSigCHName);

                ST_KEY_VALUE stOutput = {i, str};
                m_KeyValueSheet.append(stOutput);
                m_MibMappingView.push_back(*itor);
                nCount++;
            }
        }
    }
    qDebug() << "get count=" << nCount;

    ST_DISPLAY_MIB_NODE *pv = new ST_DISPLAY_MIB_NODE[nCount];
    memset(pv, 0, sizeof(ST_DISPLAY_MIB_NODE) * nCount);

    QVector<ST_DISPLAY_MIB_NODE>::iterator itorV = m_MibMappingView.begin();
    for(int i=0; itorV != m_MibMappingView.end(); itorV++, i++) //遍历选中的信号
    {        
        memcpy(&pv[i], itorV, sizeof(ST_DISPLAY_MIB_NODE));
        qDebug() << pv[i].EquipId << pv[i].strSigENName;
    }

    *pCount = nCount;
    return (void*)pv;
}


QVector<ST_DISPLAY_STRUCT> MgrMibMapping::getDisplayData(int index)
{
    Q_UNUSED(index);

    QVector<ST_DISPLAY_STRUCT> qvectRet;
    qvectRet.clear();

    return qvectRet;
}


/**
 * @brief MgrMibMapping::getDisplayData2 获取全部数据和设备类型列表
 *
 * !!注意：由于UI界面加载的性能,获取全部数据和设备类型列表函数不返回分支的加载,分支的加载将在getDisplayRequestList()中实现!!
 *
 * @param pCount     信号总行数
 * @param EquipIDBuf 下拉框显示
 * @return
 */
void *MgrMibMapping::getDisplayData2(int* pCount, int EquipIDBuf[][3])
{
    m_KeyValueSheet.clear();
    m_MibMappingView.clear();

    int filterCount = 0;
    QVector<ST_DISPLAY_MIB_NODE>::iterator ftor = m_MibNodeMappingWorkSheet.begin();

    for(int i=0; ftor != m_MibNodeMappingWorkSheet.end(); ftor++, i++)
    {
        if(ftor->oid[4] != '2') //filter branch
        {
            filterCount++;
        }
    }

    qDebug() << "filterCount" << filterCount;

    *pCount = filterCount;
    if(*pCount == 0)
    {
        return NULL;
    }

    //    int nCount = m_MibNodeMappingWorkSheet.length();
    //    *pCount = nCount;
    //    if(*pCount == 0)
    //    {
    //        return NULL;
    //    }

    ST_DISPLAY_MIB_NODE *pv = new ST_DISPLAY_MIB_NODE[filterCount];
    memset(pv, 0, sizeof(ST_DISPLAY_MIB_NODE) * (filterCount));

    QVector<ST_DISPLAY_MIB_NODE>::iterator itor = m_MibNodeMappingWorkSheet.begin();

    int tc = 0;
    int index = 0;
    int pos = 0;
    for(int i=0; itor != m_MibNodeMappingWorkSheet.end(); itor++, i++)
    {
        if(itor->oid[4] == '2') //filter branch
        {
            //qDebug() << "branch continue " << i;
            continue;
        }
        qDebug() << " next: "  << pos
                 << " Ename: " << itor->strSigENName
                 << " Cname: " << itor->strSigCHName;

        QString strSigZHName = QString::fromUtf8(itor->strSigENName);

        //[列表] key-value
        ST_KEY_VALUE stOutput = {pos, strSigZHName};
        m_KeyValueSheet.append(stOutput);

        //[列表] 备份显示表
        m_MibMappingView.push_back(*itor);

        //[列表] UI显示表
        memcpy(&pv[pos], itor, sizeof(ST_DISPLAY_MIB_NODE));

        pos++;

        if(tc != itor->EquipId)
        {
            tc = itor->EquipId;

            EquipIDBuf[index][0] = itor->TypeId;
            EquipIDBuf[index][1] = itor->EquipId;
            EquipIDBuf[index][2] = itor->EquipTypeId;
            index++;
        }
    }

    qDebug() << "=== EquipIDBuf List ===";    
    int m = 0;
    while(EquipIDBuf[m][0] != 0)
    {
        qDebug() << EquipIDBuf[m][0] << " " << EquipIDBuf[m][1] << " " << EquipIDBuf[m][2];
        m++;
    };
    qDebug() << "========================";

    return (void*)pv;
}

int MgrMibMapping::findByRegName(QString strRegAddr, int iBeginIndex, int iItemIndex)
{
    Q_UNUSED(iItemIndex);

    qDebug() << strRegAddr << " // " << iBeginIndex;

    for(int i = iBeginIndex; i < m_KeyValueSheet.length(); i++)
    {
        if (m_KeyValueSheet.at(i).strSigName.indexOf(strRegAddr) >= 0)
        {
            qDebug() <<  "findByRegName return " << i;
            return i;
        }
    }

    qDebug() <<  "findByRegName return " << -1;
    return -1;
}


void MgrMibMapping::processRequest(QByteArray RecvData)
{
    return;
}


void MgrMibMapping::parseInfo(QStringList varList, ST_DISPLAY_MIB_NODE* pSigTmp)
{
    pSigTmp->EquipTypeId = varList.at(0).toInt();
    pSigTmp->TypeId = GET_STD_MIB_TYPEID(pSigTmp->EquipTypeId);

    QString strTmpName = varList.at(1);
    int len = strlen(strTmpName.trimmed().toStdString().c_str());
    memcpy(pSigTmp->strSigENName, strTmpName.trimmed().toStdString().c_str(), len);

    //translate en2ch
    int index = 0;
    QString str = "--";
    do
    {
        str = pSigTmp->strSigENName;
        if(std_mib_name_trans[index][1] == str)
        {
            memcpy(pSigTmp->strSigCHName, std_mib_name_trans[index][2].toStdString().c_str(),
                   strlen(std_mib_name_trans[index][2].toStdString().c_str()));
            break;
        }
        index++;
    }while( std_mib_name_trans[index][1] != "end" );

    QString strTmpOid = varList.at(2);
    len = strlen(strTmpOid.trimmed().toStdString().c_str());
    memcpy(pSigTmp->oid,strTmpOid.trimmed().toStdString().c_str(),len);

    // *.CSV
    //0 . EquipTypeId
    //1 . szSignalName,
    //2 . szOid,
    //3 . iEntry, //new add
    //4 . iEquipId,
    //5 . iSigType,
    //6 . iSigId,
    //7 . bflag);

    pSigTmp->Entry        = varList.at(3).toInt();
    pSigTmp->EquipId      = varList.at(4).toInt();    
    pSigTmp->SigType      = varList.at(5).toInt();
    pSigTmp->SigId        = varList.at(6).toInt();
    pSigTmp->Flag         = varList.at(7).toInt();; //默认全是0

    if(pSigTmp->Entry != 0) //属于分支
    {
        strcat(pSigTmp->strSigENName, "_");
        strcat(pSigTmp->strSigENName, QString::number(pSigTmp->Entry).toStdString().c_str());

        strcat(pSigTmp->strSigCHName, "_");
        strcat(pSigTmp->strSigCHName, QString::number(pSigTmp->Entry).toStdString().c_str());
    }

    qDebug("getLine(%d\t%32s\t%s\t%d\t%d\t%d\t%d);",
             pSigTmp->TypeId,
             pSigTmp->strSigENName,
             pSigTmp->oid,
             pSigTmp->EquipId,
             pSigTmp->EquipTypeId,
             pSigTmp->SigType,
             pSigTmp->SigId);

    return;
}


int CompareStdMappingList(const ST_DISPLAY_MIB_NODE &p1, const ST_DISPLAY_MIB_NODE &p2)
{
    if (p1.TypeId == p2.TypeId)
    {
        if (p1.EquipId == p2.EquipId)
        {
            if (strlen(p1.oid) == strlen(p2.oid))
            {                 
                int index = 0;
                while(p1.oid[strlen(p1.oid)-index] != '.')
                {
                    index++;
                }
                int a = index;

                index = 0;
                while(p2.oid[strlen(p2.oid)-index] != '.')
                {
                    index++;
                }
                int b = index;

                if(a == b)
                {
                    for(int i=0; i < a; ++i)
                    {
                        if(p1.oid[strlen(p1.oid)-a+i] != p2.oid[strlen(p2.oid)-a+i])
                        {
                            return p1.oid[strlen(p1.oid)-a+i] < p2.oid[strlen(p2.oid)-a+i];
                        }
                    }
                    //qDebug() << "last oid: " << p1.right(str.size() - (p1.oid.lastIndexOf(".")+1));
                    //return p1.oid[strlen(p1.oid)-1] < p2.oid[strlen(p2.oid)-1];
                }
                else
                {
                    return a < b;
                }
            }
            else
            {
                return strlen(p1.oid) < strlen(p2.oid);
            }
        }
        else
        {
            return p1.EquipId < p2.EquipId;
        }
    }
    else
    {
        return p1.TypeId < p2.TypeId;
    }
}

//CSV文件解析
int MgrMibMapping::getFileContent(QString strFilePath)
{
    qDebug() << "read File Path = " << strFilePath;
    QFile file(strFilePath);

    if (!file.open(QFile::ReadOnly | QFile::Text))
    {
        qDebug() << "File not exists" << file.errorString();
        return _ERROR;
    }
    else
    {        
        QTextStream textStream (&file);
        textStream.setCodec("utf8");

        int index = 0;
        //read the data up to the end of file
        while (!textStream.atEnd())
        {
            QStringList strInput = textStream.readLine().split(",");
            ST_DISPLAY_MIB_NODE stOutput;
            memset(&stOutput, 0, sizeof(ST_DISPLAY_MIB_NODE));

            if (index == 0)
            {
                index++;                
                continue;
            }
            //qDebug("getLine:(%s)", strInput.at(1).toStdString().c_str());

            this->parseInfo(strInput, &stOutput);
            this->m_MibNodeMappingWorkSheet.append(stOutput);

            index++;
        }
        qDebug() << "=============================================== CSV PARSE END";;

        file.close();
    }

    /* sort the m_MibNodeMappingWorkSheet
    */
    //    qDebug("==============================================");
    //    QVector<ST_DISPLAY_MIB_NODE>::iterator iter = m_MibNodeMappingWorkSheet.begin();
    //    for(int i=0; iter != m_MibNodeMappingWorkSheet.end(); iter++, i++)
    //    {
    //        qDebug("before fix %d - %d - %s", iter->TypeId, iter->EquipId, iter->oid);
    //    }
    //    qDebug("==============================================");
    //    qSort(m_MibNodeMappingWorkSheet.begin(), m_MibNodeMappingWorkSheet.end(), CompareStdMappingList);
    //    qDebug("==============================================");
    //    iter = m_MibNodeMappingWorkSheet.begin();
    //    for(int i=0; iter != m_MibNodeMappingWorkSheet.end(); iter++, i++)
    //    {
    //        qDebug("after fix %d - %d - %s", iter->TypeId, iter->EquipId, iter->oid);
    //    }
    //    qDebug("==============================================");

    return (m_MibNodeMappingWorkSheet.length() > 0) ? 1:0;
}



void MgrMibMapping::clearDataBuff()
{
    this->m_MibNodeMappingWorkSheet.clear();
}

QByteArray MgrMibMapping::setWordData(int iRegAddr, int iValue, int iItemIndex)
{
    Q_UNUSED(iRegAddr);
    Q_UNUSED(iValue);
    Q_UNUSED(iItemIndex);

    return nullptr;
}

QByteArray MgrMibMapping::getWordData(int iBegin, int iNum, int iItemIndex)
{
    Q_UNUSED(iBegin);
    Q_UNUSED(iNum);
    Q_UNUSED(iItemIndex);

    return nullptr;
}


//write to file
int MgrMibMapping::OutputFile(QString strFilePath, QString *pFileName)
{
    qDebug() << "=====文件导出=====";

    QDateTime local(QDateTime::currentDateTime());
    QDateTime UTC(local.toUTC());
    qDebug() << "Local time is:" << local;
    qDebug() << "UTC time is:" << UTC;
    qDebug() << "No difference between times:" << local.secsTo(UTC);
    QDateTime dateTime(QDateTime::currentDateTime());
    QString qDate = dateTime.toString("yyyyMMdd_hhmmss");

    *pFileName = "output_" + qDate + ".csv";
    strFilePath = strFilePath + *pFileName;
    qDebug() << "FilePath:" << strFilePath;

    QFile csvFile(strFilePath);

    //以只写方式打开，完全重写数据
    if (csvFile.open(QIODevice::WriteOnly))
    {
        QVector<ST_DISPLAY_MIB_NODE>::iterator itor;

        QTextStream out_stream(&csvFile);

        out_stream.setCodec(QTextCodec::codecForName("utf-8"));

        out_stream << "Type,SigName,OID,Entry,EquipId,SigType,SigId,Flag" << "\n";

        for(itor = m_MibNodeMappingWorkSheet.begin(); itor != m_MibNodeMappingWorkSheet.end(); itor++)
        {
            if (itor->Entry != 0)
            {
                char *pch = strchr(itor->strSigENName, '_');
                char tmp[254] = {0};
                memcpy(tmp, itor->strSigENName, pch-itor->strSigENName);
                out_stream << itor->EquipTypeId  << ","
                           << tmp << ","
                           << itor->oid << ","
                           << itor->Entry << ","
                           << itor->EquipId << ","
                           << itor->SigType << ","
                           << itor->SigId << ","
                           << itor->Flag << "\n";
            }
            else
            {
                out_stream << itor->EquipTypeId  << ","
                           << itor->strSigENName << ","
                           << itor->oid << ","
                           << itor->Entry << ","
                           << itor->EquipId << ","
                           << itor->SigType << ","
                           << itor->SigId << ","
                           << itor->Flag << "\n";
            }
        }
        csvFile.close();
    }
    qDebug() << "=====导出完成=====";

    return 0;
}



