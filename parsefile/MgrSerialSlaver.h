#ifndef CMODBUSDATAMANAGER_H
#define CMODBUSDATAMANAGER_H

#include "MgrDataBase.h"
#include "comm/icommSerial.h"


#define EXCEL_RIGISTER_ROW_MODEL_TYPE   0
#define EXCEL_RIGISTER_ROW_READCODE     1
#define EXCEL_RIGISTER_ROW_WRITECODE    2
#define EXCEL_RIGISTER_ROW_DATATYPE     3
#define EXCEL_RIGISTER_ROW_DESCRIBLE    4
#define EXCEL_RIGISTER_ROW_START        5   //根据excel确定,目前Modbus-RTU寄存器从第5行开始计算

typedef struct{
    QString RegName;
    int RegAddr;
    int iScale;
    float value;
}ST_CFG_MODBUS_RTU_SIG;

typedef struct{
    int iReadCode;
    int iWriteCode;
    int iResponseType;   //0:按字节（WORD） 1：按bit（BIT） 2: 按float型(FLOAT) 大小端 => emun
    QVector<ST_CFG_MODBUS_RTU_SIG> vcData;
}ST_RTU_WORK_SHEET_CONTENT;

typedef struct{
    int iDevAddr;
    int iFuncCode;
    int iStartReg;
    int iLen;
    BYTE iCRC[2];
}ST_MODBUS_RTU_PDU;


class MgrModbusSlaver: public MgrDataBase
{
    Q_OBJECT

public:
    explicit MgrModbusSlaver();
    ~MgrModbusSlaver();
    QString getMgrName();

    int parseCfgFile(QString strFilePath);

    QVector<ST_CFG_MODBUS_RTU_SIG> getSamplerSig(int index);
    QVector<ST_DISPLAY_STRUCT> getDisplayData(int index);
    void* getDisplayData2(int* pCount);
    bool modifyValue(void* pt);
    int findByRegName(QString strRegName, int iBeginIndex, int iItemIndex);
    int OutputFile(QString strFilePath, OUT QString *pFileName);

public slots:    
    void processRequest(QByteArray RecvData);
    void notify(EMessageType type, QString str);

private:
    QString MgrName = "Modbus-RTU";

    QVector<ST_RTU_WORK_SHEET_CONTENT> m_workSheet;

    int getFileContent(QString strFilePath);
    int findByRegAddr(int iRegAddr, QVector<ST_CFG_MODBUS_RTU_SIG> &vcData);

    void clearDataBuff();
    void parseVariantContent(QVariant var, ST_RTU_WORK_SHEET_CONTENT &stSheet);
    void insertVertor(QVariantList rowData, QVector<ST_CFG_MODBUS_RTU_SIG> &vc);

    //R/W
    QByteArray setWordData(int iRegAddr, int iValue, int iPageIndex);
    QByteArray getWordData(int iBegin, int iNum, int iPageIndex);
    QByteArray getWordDataLittleEnd(int iBegin, int iNum, int iPageIndex);
    QByteArray getWordL2Data(int iBegin, int iNum, int iPageIndex);
    QByteArray getByteData(int iBegin, int iNum, int iPageIndex);
    QByteArray getByteDataLittleEnd(int iBegin, int iNum, int iPageIndex);
    QByteArray getFloatDataLittleEnd(int iBegin, int iNum, int iPageIndex);
    QByteArray getFloatDataBigEnd(int iBegin, int iNum, int iPageIndex);
    QByteArray getFloatDataSW(int iBegin, int iNum, int iPageIndex);

};

#endif // CMODBUSDATAMANAGER_H
