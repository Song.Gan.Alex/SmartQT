#ifndef MGR_POWER_DEVICE_H
#define MGR_POWER_DEVICE_H

#include <QFile>
#include "MgrDataBase.h"
#include "comm/icommPower.h"

#define INVALID_DATA 0x20

typedef struct
{
    int iCID1;
    int iCID2;
    QString strName;
    int iByte;
    int iGain;
    float fValue;
    bool isFloat;
}ST_POWER_SAMPLER_SHEET;

typedef struct
{
    int iCID1;
    int iCID2;
    QString strName;
    int iCmdTyte;
    int iByte;
    int iGain;  //gain
    float fValue;
    bool isFloat;
}ST_POWER_SET_SHEET;

typedef struct
{
    int iCID1;
    int iCID2;
    int iFlag;
}ST_POWER_INFO;

class MgrPowerDevice : public MgrDataBase
{
    Q_OBJECT

public:
    explicit MgrPowerDevice();
    ~MgrPowerDevice();

    QString getMgrName();    

    int getFileContent(QString strFilePath);

    int parseCfgFile(QString strFilePath);

    QVector<ST_DISPLAY_STRUCT> getDisplayData(int index);

    void* getDisplayData2(int* pCount);

    bool interactData(QByteArray szRecv, QByteArray& szRet);

    int findByRegName(QString strRegAddr, int iBeginIndex, int iItemIndex);

    bool modifyValue(void* pt);

    int OutputFile(QString strFilePath, OUT QString *pFileName);

public slots:
    //virtual void readData(QByteArray RecvData);
    void processRequest(QByteArray RecvData);

private:
    QString MgrName = "Power";

    QVector<ST_POWER_SAMPLER_SHEET> m_qvSamplerData;
    QVector<ST_POWER_SET_SHEET> m_qvSetData;

    //QVector<ST_POWER_INFO> m_qvInfo;

    int ChangeHexStringToInt(QString str);
    float ChangeHexStringToFloat(QString str);
    QString ChangeFloatToHexString(float fval);
    void parseVariantContent(QVariant var);
    void insertVertor(QVariantList rowData);
    void clearDataBuff();
    int ChangeCharToInt(const char cSrc);
    void parseData(QByteArray szBuf, int &iCID1, int& iCID2, int &iLen, int &iChkSum);
    void parseInfo(QVariant var);
    bool isExistInfo(int iCID1, int iCID2);
    QString getData(int iCID1, int iCID2);
    void parseSetData(QVariant var);
    void setData(int iCID1, int iCID2, QByteArray szBuf);
};

#endif // MGR_POWER_DEVICE_H
