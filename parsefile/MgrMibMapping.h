#ifndef CMIBMAPPINGMANAGER_H
#define CMIBMAPPINGMANAGER_H

#include <QFile>
#include <QTextStream>
#include <QStandardItemModel>

#include "MgrDataBase.h"

struct _ST_KEY_VALUE
{
    int  nIndex;
    QString strSigName; //ZH
};
typedef struct _ST_KEY_VALUE  ST_KEY_VALUE;


class MgrMibMapping: public MgrDataBase
{
    Q_OBJECT

public:
    explicit MgrMibMapping();
    ~MgrMibMapping();

    QString getMgrName();
    QVector<ST_COMM_PORT_INFO> getPortInfo();

    bool openPort(void* pPortParam);
    void closePort();

    int parseCfgFile(QString strFilePath);
    //不使用
    QVector<ST_DISPLAY_STRUCT> getDisplayData(int index);
    //显示全部数据
    void* getDisplayData2(int* pCount, int EquipIDBuf[][3]);

    //bool func_cmp1(const ST_DISPLAY_MIB_NODE &p1, const ST_DISPLAY_MIB_NODE &p2);
    //根据筛选获取数组[a]中数组[b]和键值数组[c],作用于返回UI显示
    void* getDisplayRequestList(int *pCount, int request_type, int request_equip_num, void* request_equip_list);
    //数据[b]
    int findByRegName(QString strRegAddr, int iBeginIndex, int iItemIndex);
    //修改数据[a][b]
    bool modifyValue(void* pt);

    void SyncmodifyValue(bool b_Synchronize);

    int OutputFile(QString strFilePath, OUT QString *pFileName);

public slots:    
    void processRequest(QByteArray RecvData);
    void notify(EMessageType type, QString str);

private:
    QString MgrName = "Mib-Mapping";
    bool b_Sync; //modify Synchronize

    QList<ST_TRANSLATE_INFO_STRUCT> m_resource_list; //翻译列表 ==> TODO 做成db数据库查询读取++    

    QVector<ST_KEY_VALUE> m_KeyValueSheet; //二维数组用于显示 (序列号,信号名)
    QVector<ST_DISPLAY_MIB_NODE> m_MibMappingView; //用于UI显示筛选数据
    QVector<ST_DISPLAY_MIB_NODE> m_MibNodeMappingWorkSheet; //csv全部文件数据

    void trimWholeLine(char *strIn, char *strOut);
    int getFileContent(QString strFilePath);
    int EquipId2EquipTypeId(int EquipId);
    void parseInfo(QStringList varList, ST_DISPLAY_MIB_NODE* pSigTmp);
    void clearDataBuff();

    //???
    QByteArray setWordData(int iRegAddr, int iValue, int iItemIndex);
    //???
    QByteArray getWordData(int iBegin, int iNum, int iItemIndex);

};


#endif // CMIBMAPPINGMANAGER_H
