/*******************************************
 *  parsefile/MgrTcpServer.cpp
 *
 *  (C) 2021  Vertiv.co
 *******************************************/

#include "MgrTcpServer.h"

MgrModbusTcpServer::MgrModbusTcpServer(qint16 listenPort)
{    
    m_pCurrComm = new iCommTCP(listenPort);

    connect(m_pCurrComm,SIGNAL(ePrintMessage(EMessageType,QString)), this,SLOT(notify(EMessageType,QString)));
    connect(m_pCurrComm,SIGNAL(ePrintData(QByteArray,QString)), this,SIGNAL(mgrReadWriteData(QByteArray,QString)));
    connect(m_pCurrComm,SIGNAL(eGetData(QByteArray)), this,SLOT(processRequest(QByteArray)));

    m_workSheet.clear();
}

MgrModbusTcpServer::~MgrModbusTcpServer()
{
    //~m_pCurrComm();
    clearDataBuff();
}

QString MgrModbusTcpServer::getMgrName()
{
    return this->MgrName;
}


void MgrModbusTcpServer::notify(EMessageType type, QString str)
{
    emit mgrMessageTip(mgr_type_tcp, type, str);
}


int MgrModbusTcpServer::parseCfgFile(QString strFilePath)
{
    clearDataBuff();
    return getFileContent(strFilePath);
}

QVector<ST_DISPLAY_STRUCT> MgrModbusTcpServer::getDisplayData(int index)
{
    QVector<ST_DISPLAY_STRUCT> qvRet;
    qvRet.clear();

    QVector<ST_CFG_MODBUS_TCP_SIG>::iterator iter = m_workSheet[index].vcData.begin();
    for(;iter < m_workSheet[index].vcData.end();iter++)
    {
        ST_DISPLAY_STRUCT stTemp;
        stTemp.RegName = iter->RegName;
        stTemp.value = iter->value;
        qDebug() << iter->RegName << "  " << iter->value;
        qvRet.append(stTemp);
    }
    return qvRet;
}

void *MgrModbusTcpServer::getDisplayData2(int *pCount)
{
    Q_UNUSED(pCount);
    return nullptr;
}

int MgrModbusTcpServer::findByRegName(QString strRegName, int iBeginIndex, int iPageIndex)
{
    for(int i = iBeginIndex; i < m_workSheet.at(iPageIndex).vcData.length();i++)
    {
        if (m_workSheet.at(iPageIndex).vcData.at(i).RegName.indexOf(strRegName) >= 0)
        {
            return i;
        }
    }
    return -1;
}

bool MgrModbusTcpServer::modifyValue(void *pt)
{
    ST_MODIFY_VALUE_OF_MODBUS* p = (ST_MODIFY_VALUE_OF_MODBUS*)pt;
    qDebug() << "Page:" << p->iPageIndex << "Index:" << p->index << "Set Value:" << p->fValue;

    m_workSheet[p->iPageIndex].vcData[p->index].value = p->fValue;
    qDebug() << m_workSheet[p->iPageIndex].vcData[p->index].value;

    return true;
}

/**
 * @brief MgrModbusTcpServer::processRequest
 * @param RecvData : dealwith RAW Modbus-TCP protocal data[]
 */
void MgrModbusTcpServer::processRequest(QByteArray RecvData)
{
    QByteArray szResponse;
    QByteArray szData;
    bool bRet = true;

    //00 01 | 00 00 | 00 06 | 01 - 03 - 00 00 - 00 03
    ST_MODBUS_TCP_PDU_REQUEST Request;
    Request.iTransID      = MAKE_WORD(RecvData[0], RecvData[1]);
    Request.iLength       = MAKE_WORD(RecvData[4], RecvData[5]);
    Request.pdu.iAddr     = RecvData[6];
    Request.pdu.iFuncCode = RecvData[7];
    Request.pdu.iStartReg = MAKE_WORD(RecvData[8], RecvData[9]);
    Request.pdu.iReqLen   = MAKE_WORD(RecvData[10],RecvData[11]);
    qDebug() << "=============================================================|||";
    qDebug() << "iTransID     : " << Request.iTransID;
    qDebug() << "iDentify     : " << Request.iDentify;
    qDebug() << "iLength      : " << Request.iLength;
    qDebug() << "------------------------------";
    qDebug() << "pdu.iAddr    : " << Request.pdu.iAddr;
    qDebug() << "pdu.iFuncCode: " << Request.pdu.iFuncCode;
    qDebug() << "pdu.iStartReg: " << Request.pdu.iStartReg;
    qDebug() << "pdu.iReqLen  : " << Request.pdu.iReqLen;
    qDebug() << "=============================================================|||";

    int nSheetIndex = -1;
    if(m_workSheet.length() == 0)
    {
        qDebug() << "没有加载配置文件";
        bRet = false;
    }
    else
    {

        for(int i = 0; i < m_workSheet.length(); i++) //workSheet.length is sheet pages
        {
            if((Request.pdu.iFuncCode == m_workSheet.at(i).iReadCode)
                || (Request.pdu.iFuncCode == m_workSheet.at(i).iWriteCode))
            {
                if(this->findByRegAddr(Request.pdu.iStartReg, m_workSheet[i].vcData) != -1)
                {
                    nSheetIndex = i;
                    break;
                }
            }
        }
    }

    if(nSheetIndex == -1)
    {
        //没找到寄存器首地址，返回
        qDebug() << "没找到寄存器首地址";
        bRet = false;
    }
    else
    {
        //找到寄存器首地址，按首地址解析
        qDebug() << "找到寄存器首地址，按首地址解析";
        ST_MODBUS_TCP_PDU pdu = Request.pdu;

        // Read register, Makeup protocol
        if(pdu.iFuncCode == m_workSheet.at(nSheetIndex).iReadCode)
        {
            switch(m_workSheet.at(nSheetIndex).iResponseType)
            {
            case MODBUS_VALUE_TYPE_WORD:
                szData = getTcpWordData(pdu.iStartReg, pdu.iReqLen, nSheetIndex); //使用函数指针形式 SRS +++
                break;
            case MODBUS_VALUE_TYPE_WORD_L2:
                szData = getTcpWordL2Data(pdu.iStartReg, pdu.iReqLen, nSheetIndex);
                break;
            case MODBUS_VALUE_TYPE_BYTE:
                szData = getTcpByteData(pdu.iStartReg, pdu.iReqLen, nSheetIndex);
                break;
            case MODBUS_VALUE_TYPE_BYTE_LE:
                szData = getTcpByteDataLittleEnd(pdu.iStartReg, pdu.iReqLen, nSheetIndex);
                break;
            case MODBUS_VALUE_TYPE_FLOAT_LSW: //4Byte
                szData = getTcpFloatDataSW(pdu.iStartReg, pdu.iReqLen, nSheetIndex);
                break;
            case MODBUS_VALUE_TYPE_FLOAT_LE: //4Byte
                szData = getTcpFloatDataLittleEnd(pdu.iStartReg, pdu.iReqLen, nSheetIndex);
                break;
            case MODBUS_VALUE_TYPE_FLOAT_BE: //4Byte
                szData = getTcpFloatDataBigEnd(pdu.iStartReg, pdu.iReqLen, nSheetIndex);
                break;
            }
        }

        // Write register, Makeup protocol
        if(pdu.iFuncCode == m_workSheet.at(nSheetIndex).iWriteCode)
        {
            switch(m_workSheet.at(nSheetIndex).iResponseType)
            {
            case MODBUS_VALUE_TYPE_WORD:
                //szData = setWordData(pdu.iStartReg, pdu.iReqLen, nSheetIndex);
                break;
            default:
                bRet = false;
            }
        }
    }

    /*
    如：起始地址是0x0000，寄存器数量是 0x0003
    00 01 00 00 00 06 01 03 00 00 00 03
    ----------------------------------
    00 01
    00 00
    00 06
    01
    03
    00 00 00 03

    回：数据长度为0x06，第一个寄存器的数据为0x21，其余为0x00
    00 01 00 00 00 09 01 03 06 00 21 00 00 00 00
    --------------------------------------------
    00 01
    00 00
    00 09
    01
    03
    06 00 21 00 00 00 00

    如：向地址是0x0000的寄存器写入数据0x000A
    00 01 00 00 00 06 01 06 00 00 00 0A

    00 01
    00 00
    00 06
    01
    06
    00 00
    00 0A

    回：写入成功
    00 01 00 00 00 06 01 06 00 00 00 0A
    */

    if (bRet == false)
    {
        qDebug() << "!make up error!";
        szData[0] = 0xFF; //Not Found
        szData[1] = 0xFF; //Not Found
    }

    szResponse[0] = Request.iTransID >> 8 & 0xFF;
    szResponse[1] = Request.iTransID & 0xFF;

    szResponse[2] = Request.iDentify >> 8 & 0xFF;
    szResponse[3] = Request.iDentify & 0xFF;

    szResponse[4] = HIBYTEOFWORD(szData.length() + 2);
    szResponse[5] = LOBYTEOFWORD(szData.length() + 2);

    szResponse[6] = Request.pdu.iAddr & 0xFF;
    szResponse[7] = Request.pdu.iFuncCode & 0xFF;

    szResponse += szData;

    this->feedbackResponse(szResponse);
}



int MgrModbusTcpServer::findByRegAddr(int iRegAddr, QVector<ST_CFG_MODBUS_TCP_SIG> &vcData)
{
    for(int i = 0; i < vcData.length();i++)
    {
        if (vcData.at(i).RegAddr == iRegAddr)
        {
            return i;
        }
    }

    return -1;
}



void MgrModbusTcpServer::clearDataBuff()
{
    for(int i = 0; i < m_workSheet.length();i++)
    {
        m_workSheet[i].vcData.clear();
    }

    m_workSheet.clear();
}


QByteArray MgrModbusTcpServer::getTcpWordData(int iBegin, int iNum, int iPageIndex)
{
    QByteArray ret;
    ret.clear();
    ret[0] = iNum*2;

    qDebug()<< "Page Index     : " << iPageIndex;
    qDebug()<< "Start Register : " << iBegin;
    qDebug()<< "Register Len   : " << iNum;

    //iNum is Totally register count.
    for(int i=0; i < iNum; i++)
    {
        int index = findByRegAddr(iBegin+i, m_workSheet[iPageIndex].vcData);
        if (index == -1)
        {
            qDebug()<< "find nothing.";
            ret[i*2+1] = 0;
            ret[i*2+2] = 0;
            continue;
        }

        int value = m_workSheet[iPageIndex].vcData[index].value * m_workSheet[iPageIndex].vcData[index].iScale;
        qDebug()<< "find value[" << i << "]: " << value;

        ret[i*2+1] = HIBYTEOFWORD(value);
        ret[i*2+2] = LOBYTEOFWORD(value);
    }

    return ret;
}

QByteArray MgrModbusTcpServer::getTcpWordL2Data(int iBegin, int iNum, int iPageIndex)
{
    QByteArray ret;
    ret.clear();

    ret[0] = HIBYTEOFWORD(iNum*2);
    ret[1] = LOBYTEOFWORD(iNum*2);

    for(int i = 0;i < iNum;i++)
    {
        int index = findByRegAddr(iBegin+i, m_workSheet[iPageIndex].vcData);
        if(-1 == index)
        {
            ret[i*2+2] = 0;
            ret[i*2+3] = 0;
            continue;
        }

        //qDebug()<<"iRegAddr:"<<(iBegin+i)<<",:"<<m_workSheet[iPageIndex].vcData[index].DefaultValue * m_workSheet[iPageIndex].vcData[index].multiple
        //       <<"iIndex:"<<index<<", :"<<iPageIndex;

        int value = m_workSheet[iPageIndex].vcData[index].value * m_workSheet[iPageIndex].vcData[index].iScale;
        ret[i*2+2] = HIBYTEOFWORD(value);
        ret[i*2+3] = LOBYTEOFWORD(value);
    }

    return ret;
}

QByteArray MgrModbusTcpServer::getTcpByteData(int iBegin, int iNum, int iPageIndex)
{
    QByteArray ret;
    ret.clear();

    ret[0] = (iNum+7)/8;
    unsigned char value = 0;
    unsigned char valueTmp = 0;

    for(int i=0; i < iNum; i++)
    {
        qDebug()<<"getByteData iNum:" << i;

        int index = findByRegAddr(iBegin+i, m_workSheet[iPageIndex].vcData);
        if(-1 == index)
        {
            valueTmp = 0;
        }
        else
        {
            valueTmp = (((int)(m_workSheet[iPageIndex].vcData[index].value)) & 0x01);
        }
        qDebug()<<"getByteData value orgin:" << valueTmp;

        switch (i%8) {
            case 0:
                valueTmp = (valueTmp  << 7);
                break;
            case 1:
                valueTmp = (valueTmp  << 6);
                break;
            case 2:
                valueTmp = (valueTmp  << 5);
                break;
            case 3:
                valueTmp = (valueTmp  << 4);
                break;
            case 4:
                valueTmp = (valueTmp  << 3);
                break;
            case 5:
                valueTmp = (valueTmp  << 2);
                break;
            case 6:
                valueTmp = (valueTmp  << 1);
                break;
            case 7:
            default:
                break;
        }

        value += valueTmp;
        ret[(i/8)+1] = value;
        if((i+1)%8 == 0)
        {
            value = 0;
        }

        qDebug()<<"getByteData value:" << value << ", index:" << ((i/8)+1) << ", ret" << ret[(i/8)+1];
    }
    return ret;
}

QByteArray MgrModbusTcpServer::getTcpByteDataLittleEnd(int iBegin, int iNum, int iPageIndex)
{
    qDebug() << "getByteDataLittleEnd start";
    QByteArray ret;
    ret.clear();

    ret[0] = (iNum + 7) / 8;

    unsigned char value = 0;
    unsigned char valueTmp = 0;
    for(int i = 0;i < iNum; i++)
    {
        qDebug()<<"getByteDataLittleEnd iNum: " << i;
        int index = findByRegAddr(iBegin + i, m_workSheet[iPageIndex].vcData);
        if(-1 == index)
        {
            valueTmp = 0;
        }
        else
        {
            valueTmp = (((int)(m_workSheet[iPageIndex].vcData[index].value)) & 0x01);
        }
        qDebug()<<"getByteDataLittleEnd value orgin:" << valueTmp;

        switch (i % 8) {
        case 0:
            break;
        case 1:
            valueTmp = (valueTmp  << 1);
            break;
        case 2:
            valueTmp = (valueTmp  << 2);
            break;
        case 3:
            valueTmp = (valueTmp  << 3);
            break;
        case 4:
            valueTmp = (valueTmp  << 4);
            break;
        case 5:
            valueTmp = (valueTmp  << 5);
            break;
        case 6:
            valueTmp = (valueTmp  << 6);
            break;
        case 7:
            valueTmp = (valueTmp  << 7);
            break;
        default:
            break;
        }

        value += valueTmp;
        ret[(i/8)+1] = value;
        if((i+1)%8 == 0)
        {
            value = 0;
        }

        qDebug()<<"getByteDataLittleEnd value:"<<value<<", index:"<<((i/8)+1)<<", ret"<<ret[(i/8)+1];
    }

    return ret;
}

QByteArray MgrModbusTcpServer::getTcpFloatDataLittleEnd(int iBegin, int iNum, int iPageIndex)
{
    qDebug()<<"getByteData start";
    QByteArray ret;
    ret.clear();

    int iRegNum = ((iNum % 2) == 0) ? (iNum / 2) : (iNum / 2 + 1);

    ret[0] = iRegNum * 4;

    for(int i = 0, j = 0; j < iRegNum; i += 2, j++)
    {
        int index = findByRegAddr(iBegin+i, m_workSheet[iPageIndex].vcData);
        if(-1 == index)
        {
            ret[j*4+1] = 0;
            ret[j*4+2] = 0;
            ret[j*4+3] = 0;
            ret[j*4+4] = 0;
            continue;
        }

        union STRTOFLOAT
        {
               float f_value;      // 浮点值
               unsigned char by_value[5];   // 16进制的字符串
        }floatvalue;

        floatvalue.f_value = m_workSheet[iPageIndex].vcData[index].value * m_workSheet[iPageIndex].vcData[index].iScale;

        qDebug() << "regId: " << iBegin+i
                 << "FloatO: " << m_workSheet[iPageIndex].vcData[index].value
                 << "FloatR: " << floatvalue.f_value
                 << "Float[1] : " <<  floatvalue.by_value[0]
                 << "Float[2] : " <<  floatvalue.by_value[1]
                 << "Float[3] : " <<  floatvalue.by_value[2]
                 << "Float[4] : " <<  floatvalue.by_value[3];

        ret[j*4+1] = floatvalue.by_value[0];
        ret[j*4+2] = floatvalue.by_value[1];
        ret[j*4+3] = floatvalue.by_value[2];
        ret[j*4+4] = floatvalue.by_value[3];
    }

    return ret;
}

QByteArray MgrModbusTcpServer::getTcpFloatDataBigEnd(int iBegin, int iNum, int iPageIndex)
{
    qDebug()<<"getByteData start";
    QByteArray ret;
    ret.clear();

    int iRegNum = ((iNum % 2) == 0) ? (iNum / 2) : (iNum / 2 + 1);

    ret[0] = iRegNum * 4;

    for(int i = 0, j = 0; j < iRegNum; i += 2, j++)
    {
        int index = findByRegAddr(iBegin+i, m_workSheet[iPageIndex].vcData);
        if(-1 == index)
        {
            ret[j*4+1] = 0;
            ret[j*4+2] = 0;
            ret[j*4+3] = 0;
            ret[j*4+4] = 0;
            continue;
        }

        union STRTOFLOAT
        {
               float f_value;      // 浮点值
               unsigned char by_value[5];   // 16进制的字符串
        }floatvalue;

        floatvalue.f_value = m_workSheet[iPageIndex].vcData[index].value * m_workSheet[iPageIndex].vcData[index].iScale;

        qDebug() << "regId: " << iBegin+i
                 << "FloatO: " << m_workSheet[iPageIndex].vcData[index].value
                 << "FloatR: " << floatvalue.f_value
                 << "Float[1] : " <<  floatvalue.by_value[0]
                 << "Float[2] : " <<  floatvalue.by_value[1]
                 << "Float[3] : " <<  floatvalue.by_value[2]
                 << "Float[4] : " <<  floatvalue.by_value[3];

        ret[j*4+1] = floatvalue.by_value[3];
        ret[j*4+2] = floatvalue.by_value[2];
        ret[j*4+3] = floatvalue.by_value[1];
        ret[j*4+4] = floatvalue.by_value[0];
    }

    return ret;
}

QByteArray MgrModbusTcpServer::getTcpFloatDataSW(int iBegin, int iNum, int iPageIndex)
{
    qDebug()<<"getByteData start";
    QByteArray ret;
    ret.clear();

    int iRegNum = ((iNum % 2) == 0) ? (iNum / 2) : (iNum / 2 + 1);

    ret[0] = iRegNum * 4;


    for(int i = 0, j = 0; j < iRegNum; i += 2, j++)
    {
        int index = findByRegAddr(iBegin+i, m_workSheet[iPageIndex].vcData);
        if(-1 == index)
        {
            ret[j*4+1] = 0;
            ret[j*4+2] = 0;
            ret[j*4+3] = 0;
            ret[j*4+4] = 0;
            continue;
        }

        union STRTOFLOAT
        {
               float f_value;  // 浮点值
               unsigned char by_value[5];   // 16进制的字符串
        }floatvalue;

        floatvalue.f_value = m_workSheet[iPageIndex].vcData[index].value * m_workSheet[iPageIndex].vcData[index].iScale;

        qDebug() << "regId: " << iBegin+i
                 << "FloatO: " << m_workSheet[iPageIndex].vcData[index].value
                 << "FloatR: " << floatvalue.f_value
                 << "Float[1] : " <<  floatvalue.by_value[0]
                 << "Float[2] : " <<  floatvalue.by_value[1]
                 << "Float[3] : " <<  floatvalue.by_value[2]
                 << "Float[4] : " <<  floatvalue.by_value[3];

        ret[j*4+1] = floatvalue.by_value[1];
        ret[j*4+2] = floatvalue.by_value[0];
        ret[j*4+3] = floatvalue.by_value[3];
        ret[j*4+4] = floatvalue.by_value[2];
    }

    return ret;
}


QByteArray MgrModbusTcpServer::setTcpWordData(int iRegAddr, int iValue, int iPageIndex)
{
    QByteArray ret;
    ret.clear();
    qDebug() << "index" << iRegAddr;
    qDebug() << "iRegAddr" << iValue;

    int index = findByRegAddr(iRegAddr, m_workSheet[iPageIndex].vcData);

    qDebug() << "index" << index;

    if (-1 != index)
    {
        qDebug() << m_workSheet.length();

        m_workSheet[iPageIndex].vcData[index].value = (float)iValue / m_workSheet[iPageIndex].vcData[index].iScale;

        qDebug() << "set data: " << m_workSheet[iPageIndex].vcData[index].value
                 << ", index: "  << index << ", iPageIndex: " << iPageIndex;

        emit mgrRefreshDisplay(iPageIndex, index, m_workSheet[iPageIndex].vcData[index].value);
    }

    ret[0] = HIBYTEOFWORD(iRegAddr);
    ret[1] = LOBYTEOFWORD(iRegAddr);

    ret[2] = HIBYTEOFWORD(iValue);
    ret[3] = LOBYTEOFWORD(iValue);

    return ret;
}


//初始化每个信号寄存器
void MgrModbusTcpServer::insertVertor(QVariantList rowData, QVector<ST_CFG_MODBUS_TCP_SIG> &vc)
{
    ST_CFG_MODBUS_TCP_SIG stTmp;
    stTmp.RegName.clear();
    stTmp.RegAddr = 0;
    stTmp.value = 0;
    stTmp.iScale = 0;

    switch(rowData.size())
    {
    case 4:
        stTmp.value = rowData[3].toFloat();
    case 3:
        stTmp.iScale = rowData[2].toDouble();
    case 2:
        stTmp.RegAddr = rowData[1].toDouble();
    case 1:
        stTmp.RegName = rowData[0].toString();
        //qDebug() << "RegName  " << stTmp.RegName;
        break;
    default:
        return;
    }

    if (stTmp.iScale == 0) stTmp.iScale = 1;

    stTmp.value /= stTmp.iScale;

    vc.append(stTmp);
}

void MgrModbusTcpServer::parseVariantContent(QVariant var, ST_TCP_WORK_SHEET_CONTENT &stSheet)
{
    QVariantList varRows = var.toList(); //:->
    if(varRows.isEmpty())
    {
        return;
    }
    const int rowCount = varRows.size();

    QVariantList rowData;
    bool ok;

    qDebug() << varRows[EXCEL_RIGISTER_ROW_MODEL_TYPE].toList()[1].toString();
    if(this->MgrName != varRows[EXCEL_RIGISTER_ROW_MODEL_TYPE].toList()[1].toString())
    {
        //emit err message
        return;
    }

    stSheet.iReadCode = varRows[EXCEL_RIGISTER_ROW_READCODE].toList()[1].toString().toUInt(&ok, 16);
    stSheet.iWriteCode = varRows[EXCEL_RIGISTER_ROW_WRITECODE].toList()[1].toString().toUInt(&ok, 16);
    QString strValueType = varRows[EXCEL_RIGISTER_ROW_DATATYPE].toList()[1].toString();
    if(strValueType == "BYTE")
    {
         stSheet.iResponseType = MODBUS_VALUE_TYPE_BYTE;
    }
    else if(strValueType == "BYTE-LE")
    {
         stSheet.iResponseType = MODBUS_VALUE_TYPE_BYTE_LE;
    }
    else if(strValueType == "WORD")
    {
         stSheet.iResponseType = MODBUS_VALUE_TYPE_WORD;
    }
    else if(strValueType == "WORD-L2")
    {
         stSheet.iResponseType = MODBUS_VALUE_TYPE_WORD_L2;
    }
    else if(strValueType == "FLOAT-LSW")
    {
        stSheet.iResponseType = MODBUS_VALUE_TYPE_FLOAT_LSW;
    }
    else if(strValueType == "FLOAT-LE")
    {
        stSheet.iResponseType = MODBUS_VALUE_TYPE_FLOAT_LE;
    }
    else if(strValueType == "FLOAT-BE")
    {
        stSheet.iResponseType = MODBUS_VALUE_TYPE_FLOAT_BE;
    }
    else
    {
        stSheet.iResponseType = MODBUS_VALUE_TYPE_WORD;  //default is word
    }

    qDebug("ReadFuncCode(0x%02X) | WriteCode(0x%02X) | DateType(0x%02X)",
           stSheet.iReadCode,
           stSheet.iWriteCode,
           stSheet.iResponseType);

    //+++ 做一个简单的判断 +++

    //foreach signal insert to st
    for(int i = EXCEL_RIGISTER_ROW_START; i < rowCount; ++i)
    {
        rowData = varRows[i].toList();
        insertVertor(rowData, stSheet.vcData);

        //加载中...进度条+++计算百分比
    }
}

//解析Excel
int MgrModbusTcpServer::getFileContent(QString strFilePath)
{
    qDebug() << "#2.Excel File Loading...";
    QAxObject *excel = new QAxObject("Excel.Application");  //建立excel操作对象
    if(!excel)
    {
        qDebug()<<"Failed to create Excel!";
    }
    excel->dynamicCall("SetVisible (bool Visible)","false");//设置窗体不可视
    excel->setProperty("DisplayAlerts", false);             //不显示任何警告信息，如关闭时的是否保存提示

    QAxObject *workbooks = excel->querySubObject("WorkBooks");//获取工作簿(excel文件)集合
    workbooks->dynamicCall("Open(const QString&)", strFilePath);

    QAxObject *workbook = excel->querySubObject("ActiveWorkBook");
    QAxObject *worksheets = workbook->querySubObject("Sheets");

    int iNum = worksheets->property("Count").toInt();
    qDebug() << QString("Excel sheet num is %1").arg(QString::number(iNum));
    for (int i = 1; i < iNum + 1; i++)
    {
        QAxObject *worksheet = worksheets->querySubObject("Item(int)",i); //获取第i个sheet
        QAxObject *usedRange = worksheet->querySubObject("UsedRange");    //获取该sheet的数据范围

        QAxObject *rows = usedRange->querySubObject("Rows");       //获取行数
        QAxObject *columns = usedRange->querySubObject("Columns"); //获取列数
        int iRows = rows->property("Count").toInt();
        int iColumns = columns->property("Count").toInt();
        qDebug("Excel %d [row %d , col %d]",i ,iRows - EXCEL_RIGISTER_ROW_START, iColumns);
        QVariant var = usedRange->dynamicCall("Value");

        ST_TCP_WORK_SHEET_CONTENT stTmp;
        parseVariantContent(var, stTmp);

        m_workSheet.append(stTmp);
    }
    qDebug() << "#3.Excel File Loading OK...";

    workbook->dynamicCall( "Close(Boolean)", false );

    //已加载,关闭excel程序,操作完成后记着关闭,由于是隐藏的看不到，不关闭进程会有很多excel.exe
    excel->dynamicCall("Quit(void)");
    delete excel;

    return iNum; //sheet page num
}


int MgrModbusTcpServer::OutputFile(QString strFilePath, QString *pFileName)
{
    Q_UNUSED(strFilePath);
    return 0;
}
