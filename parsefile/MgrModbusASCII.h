#ifndef DATAMANAGERASCIISTATION_H
#define DATAMANAGERASCIISTATION_H

#include "MgrDataBase.h"
#include "comm/icommSerial.h"

typedef struct{    
    QString requestPdu;
    int total;
    int index;
    QVector<QString> respondPdu;
}ST_PDU_CONTENT;

class MgrModbusASCII: public MgrDataBase
{
    Q_OBJECT

public:
    explicit MgrModbusASCII();
    ~MgrModbusASCII();
    QString getMgrName();

    int parseCfgFile(QString strFilePath);

    QVector<ST_DISPLAY_STRUCT> getDisplayData(int index);
    void* getDisplayData2(int* pCount);

    bool modifyValue(void* pt);

    int findByRegName(QString strRegName, int iBeginIndex, int iItemIndex);

    int OutputFile(QString strFilePath, OUT QString *pFileName);

public slots:
    void processRequest(QByteArray RecvData);
    void notify(EMessageType type, QString str);

private:
    QString MgrName = "Modbus-ASCII";

    QList<ST_PDU_CONTENT> m_ListPdu;

    int getFileContent(QString strFilePath);
    int processLine(QString strLine, int index);

    void clearDataBuff();
};

#endif // DATAMANAGERASCIISTATION_H
