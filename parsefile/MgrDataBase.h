#ifndef CDATAMANAGERBASE_H
#define CDATAMANAGERBASE_H

#include <QDebug>
#include <QObject>
#include <QVector>
#include <QString>
#include <QAxObject>

#include <minwindef.h>
#include "comm/icommBase.h"
#include "include/publicdatatype.h"


class MgrDataBase: public QObject
{
    Q_OBJECT

signals:
    //消息提示
    void mgrMessageTip(EMgrType, EMessageType, QString);
    //接受和发送
    void mgrReadWriteData(QByteArray, QString);
    //页面进行设置信号数值后刷新显示
    void mgrRefreshDisplay(int,int,float);

public:
    explicit MgrDataBase();
    ~MgrDataBase();

    /* 通讯信息交互 */

    //获取端口信息(用于显示下拉菜单)
    virtual QVector<ST_COMM_PORT_INFO> getPortInfo() {
         return m_pCurrComm->_getPortInfo(); }
    //打开端口
    virtual bool openPort(void* pPortParam) {
        return m_pCurrComm->_openPort(pPortParam); }
    //关闭端口
    virtual void closePort() {
        return m_pCurrComm->_closePort(); }
    //响应处理后的数据帧
    virtual void feedbackResponse(QByteArray SendData) {
        return m_pCurrComm->_writeData(SendData); }


    /*
     * 界面信息交互
     */
    //获取业务管理进程名(通用接口)
    virtual QString getMgrName() = 0;
    //解析Excel、csv等文件格式(通用接口)
    virtual int getFileContent(QString strFilePath) = 0;
    //解析配置文件(通用接口)
    virtual int parseCfgFile(QString strFilePath) = 0;
    //页面请求显示获取的数据(非Mib映射表管理使用)
    virtual QVector<ST_DISPLAY_STRUCT> getDisplayData(int index) = 0;
    //页面请求显示获取的数据(仅限Mib映射表管理使用)2
    virtual void* getDisplayData2(OUT int* pCount, OUT int EquipIDBuf[][3]) { return NULL; }
    //页面请求显示筛选后的数据(仅限Mib映射表管理使用)2
    virtual void* getDisplayRequestList(OUT int* pCnt, IN int s1, IN int s2, IN void* p) { return NULL; }
    //页面请求根据信号名称循环查找(通用接口)
    virtual int findByRegName(QString strRegName, int iBeginIndex, int iItemIndex) = 0;
    //页面请求修改数据(管理入参不同通用接口)
    virtual bool modifyValue(void* pt) = 0;
    //页面请求修改数据(仅限Mib映射表管理使用)2
    virtual void SyncmodifyValue(bool b_Synchronize) { return ; }
    //页面请求导出修改后的文件(仅限Mib映射表管理使用)2
    virtual int OutputFile(IN QString strFilePath, OUT QString *pFileName) { return 0; }

public slots:
    //根据请求数据帧响应处理(非Mib映射表管理使用)
    void processRequest(QByteArray RecvData);

protected:
    //从属的通讯接口
    iCommBase *m_pCurrComm;

};

#endif // CPARSEFILEBASE_H
