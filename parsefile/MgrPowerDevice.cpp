/*******************************************
 *  parsefile/MgrPowerDevice.cpp
 *
 *  (C) 2022  Vertiv.co
 *******************************************/

#include "MgrPowerDevice.h"

MgrPowerDevice::MgrPowerDevice()
{
    m_pCurrComm = new iCommPower;

    m_qvSamplerData.clear();

    //m_qvInfo.clear();
}

MgrPowerDevice::~MgrPowerDevice()
{
    clearDataBuff();
}

QString MgrPowerDevice::getMgrName()
{
    return this->MgrName;
}

void MgrPowerDevice::parseSetData(QVariant var)
{
    QVariantList varRows = var.toList();
    if(varRows.isEmpty())
    {
        return;
    }

    const int rowCount = varRows.size();
    qDebug() << "SetData rowCount:" << rowCount;
    qDebug() << "SetData ColCount:" << varRows[0].toList().length();
    for(int i=1;i<rowCount;++i) //从1开始，第一行为名称
    {
        ST_POWER_SET_SHEET stTmp;

        QVariantList rowData = varRows[i].toList();
        stTmp.iCID1 = ChangeHexStringToInt(rowData[0].toString());
        stTmp.iCID2 = ChangeHexStringToInt(rowData[1].toString());

        stTmp.strName = rowData[2].toString();
        stTmp.iCmdTyte = ChangeHexStringToInt(rowData[3].toString());
        stTmp.iByte = rowData[4].toDouble();
        stTmp.fValue = rowData[5].toFloat();

        if(rowData.size() > 6)
            stTmp.iGain = rowData[6].isNull() ? 1 : rowData[6].toInt();
        else
            stTmp.iGain = 1;
        //qDebug() << "set  gain: " << stTmp.iGain;

        if(rowData.size() > 7)
            stTmp.isFloat = (rowData[7].toString() == "FLOAT");
        else
            stTmp.isFloat = false;

        //qDebug() << "set  isFloat: " << stTmp.isFloat;

        m_qvSetData.append(stTmp);
    }

    qDebug() << "SetData end";
}


int MgrPowerDevice::parseCfgFile(QString strFilePath)
{
    //clearDataBuff(); 替换+++
    //return getFileContent(strFilePath);

    // 链接Excel控件（用Office的Excel打开Excel文档）
    QAxObject *excel = new QAxObject("Excel.Application");

    // 不显示窗体
    excel->dynamicCall("SetVisible (bool Visible)", "false");

    // 不显示任何警告信息，如关闭时的是否保存提示
    excel->setProperty("DisplayAlerts", false);

    // 获取工作簿(excel文件)集合
    QAxObject *workbooks = excel->querySubObject("WorkBooks");

    // 打开Excel文件
    workbooks->dynamicCall("Open(const QString&)", strFilePath);

    // 当前活动工作簿
    QAxObject *workbook = excel->querySubObject("ActiveWorkBook");

    if (workbook == nullptr)
    {
        // 返回异常（当前活动工作簿打开失败）
        return false;
    }

    // 工作表集
    // QAxObject *worksheets = workbook->querySubObject("WorkSheets");
    QAxObject *worksheets = workbook->querySubObject("Sheets");

    int WorkSheetCount = worksheets->property("Count").toInt();
    qDebug() << "WorkSheetCount: " << WorkSheetCount << " page.";

    //解析sheet1
    if (WorkSheetCount >= 1) {
        QAxObject *worksheet = worksheets->querySubObject("Item(int)",1);
        QAxObject *usedRange = worksheet->querySubObject("UsedRange");
        int rows = usedRange->querySubObject("Rows")->property("Count").toInt();
        qDebug() << "#Sheet1 rows: " << rows;
        QVariant var = usedRange->dynamicCall("Value"); // <--所有的内容
        parseVariantContent(var);
    }

    //解析sheet2
    if (WorkSheetCount >= 2) {
        QAxObject *worksheet = worksheets->querySubObject("Item(int)",2);
        QAxObject *usedRange = worksheet->querySubObject("UsedRange");
        int rows = usedRange->querySubObject("Rows")->property("Count").toInt();
        qDebug() << "#Sheet2 rows: " << rows;
        QVariant var = usedRange->dynamicCall("Value"); //这里是所有的内容
        parseSetData(var);
    }

    workbook->dynamicCall( "Close(Boolean)", false );
    excel->dynamicCall("Quit(void)");//关闭excel程序,操作完成后记着关闭，由于是隐藏的看不到，不关闭进程会有很多excel.exe
    delete excel;

    return WorkSheetCount;
}

QVector<ST_DISPLAY_STRUCT> MgrPowerDevice::getDisplayData(int index)
{
    QVector<ST_DISPLAY_STRUCT> qvRet;
    qvRet.clear();
    if(index == 0)
    {
        for(int i = 0; i < m_qvSamplerData.length();i++)
        {
            ST_DISPLAY_STRUCT stTmp;
            stTmp.RegName = m_qvSamplerData.at(i).strName;
            stTmp.value = m_qvSamplerData.at(i).fValue;

            qvRet.append(stTmp);
        }

    }
    else
    {
        for(int i = 0; i < m_qvSetData.length();i++)
        {
            ST_DISPLAY_STRUCT stTmp;
            stTmp.RegName = m_qvSetData.at(i).strName;
            stTmp.value = m_qvSetData.at(i).fValue;

            qvRet.append(stTmp);
        }

    }

    return qvRet;
}

void *MgrPowerDevice::getDisplayData2(int *pCount)
{

}

bool MgrPowerDevice::modifyValue(void *pt)
{
    //    ST_MODIFY_VALUE_OF_MODBUS* p = (ST_MODIFY_VALUE_OF_MODBUS*)pt;

    //    qDebug() << "Page:" << p->iPageIndex << "Index:" << p->index << "Set Value:" << p->fValue;

    //    m_workSheet[p->iPageIndex].vcData[p->index].value = p->fValue;

    //    qDebug() << m_workSheet[p->iPageIndex].vcData[p->index].value;

    //    return true;
}

//void MgrPowerDevice::modifyValue(int iIndex, float fValue, int iItemIndex)
//{
//    if (iItemIndex == 0)
//    {
//        m_qvSamplerData[iIndex].fValue = fValue;
//    }
//    else
//    {
//        m_qvSetData[iIndex].fValue = fValue;
//    }
//}


bool MgrPowerDevice::interactData(QByteArray szRecv, QByteArray &szRet)
{
    int iCID1 = 0;
    int iCID2 = 0;
    int iLen = 0;
    int iChkSum = 0;
    parseData(szRecv, iCID1, iCID2, iLen, iChkSum);

    for(int i = 0; i < m_qvSamplerData.length();i++)
    {
        if (m_qvSamplerData.at(i).iCID1 == iCID1
                && m_qvSamplerData.at(i).iCID2 == iCID2)
        {
            QString strRet = getData(iCID1, iCID2);
            szRet = strRet.toLatin1();

            return true;
        }
    }

    for(int i = 0; i < m_qvSetData.length();i++)
    {
        qDebug()<<"set data:"<<iCID1<<", "<<iCID2<<", "<<m_qvSetData.at(i).iCID1<<", "<<m_qvSetData.at(i).iCID2;
        if (m_qvSetData.at(i).iCID1 == iCID1
                && m_qvSetData.at(i).iCID2 == iCID2)
        {
            qDebug()<<"set data get"<<endl;
            setData(iCID1, iCID2, szRecv);

            return true;
        }
    }

    return false;
}

void MgrPowerDevice::parseVariantContent(QVariant var)
{
    int realRowsCount = 0;

    QVariantList varRows = var.toList();
    if(varRows.isEmpty())
    {
        return;
    }
    const int rowCount = varRows.size();    
    QVariantList rowData;

    for(int i=1; i < rowCount; ++i) //从1开始，第一行为名称
    {
        rowData = varRows[i].toList();

        QString strRegisterName = rowData[2].toString();
        if (strRegisterName == "") // !! excel strRegisterName null, jump next !!
        {
            continue;
        }
        else
        {
            insertVertor(rowData);
            realRowsCount++;
        }
    }

    qDebug() << "End realRowsCount:" << realRowsCount;
}

void MgrPowerDevice::insertVertor(QVariantList rowData)
{
    ST_POWER_SAMPLER_SHEET stTmp;
    stTmp.strName.clear();
    stTmp.iCID1 = 0;
    stTmp.iCID2 = 0;
    stTmp.iByte = 0;
    stTmp.iGain = 0;
    stTmp.fValue = 0;
    stTmp.isFloat = false;  //default is int

    switch(rowData.size())
    {
    case 7:
        stTmp.isFloat = (rowData[6].toString() == "FLOAT");
    case 6:
        stTmp.fValue = rowData[5].toFloat();
    case 5:
        stTmp.iGain = rowData[4].toDouble();
    case 4:
        stTmp.iByte = rowData[3].toDouble();
    case 3:
        stTmp.strName = rowData[2].toString();
    case 2:
        stTmp.iCID2 = ChangeHexStringToInt(rowData[1].toString());
    case 1:
        stTmp.iCID1 = ChangeHexStringToInt(rowData[0].toString());
        break;
    default:
        return;
    }

    //qDebug() << "set  isFloat: " << stTmp.isFloat;
    m_qvSamplerData.append(stTmp);
}

void MgrPowerDevice::clearDataBuff()
{
    m_qvSamplerData.clear();
}

int MgrPowerDevice::ChangeCharToInt(const char cSrc)
{
    if (cSrc >= '0' && cSrc <= '9')
    {
        return cSrc - '0';
    }
    else if (cSrc >= 'A' && cSrc <= 'F')
    {
        return cSrc - 'A' + 10;
    }
    else if (cSrc >= 'a' && cSrc <= 'f')
    {
        return cSrc - 'a' + 10;
    }

    return 0;
}

int MgrPowerDevice::ChangeHexStringToInt(QString str)
{

    int  hi = ChangeCharToInt(str.toStdString().c_str()[0]);
    int  lo = ChangeCharToInt(str.toStdString().c_str()[1]);

    return MAKE_BYTE(hi, lo);

}

float MgrPowerDevice::ChangeHexStringToFloat(QString str)
{
        union STRTOFLOAT
        {
            float f_value;      // 浮点值
            unsigned char by_value[4];   // 16进制的字符串
        }floatvalue;

        floatvalue.by_value[0] = str.toStdString().c_str()[3];
        floatvalue.by_value[1] = str.toStdString().c_str()[2];
        floatvalue.by_value[2] = str.toStdString().c_str()[1];
        floatvalue.by_value[3] = str.toStdString().c_str()[0];

        return floatvalue.f_value;
}

QString MgrPowerDevice::ChangeFloatToHexString(float fval)
{
        union STRTOFLOAT
        {
            float f_value;      // 浮点值
            unsigned char by_value[4];   // 16进制的字符串
        }floatvalue;


        floatvalue.f_value = fval;

        QString strTmp;
        strTmp.sprintf("%X%X%X%X",
                   floatvalue.by_value[3],
                   floatvalue.by_value[2],
                   floatvalue.by_value[1],
                   floatvalue.by_value[0]);

        return strTmp;
}



void MgrPowerDevice::parseData(QByteArray szBuf, int &iCID1, int &iCID2, int &iLen, int &iChkSum)
{
    char* pData = szBuf.data();

    int iTmp1 = ChangeCharToInt(pData[0]);
    int iTmp2 = ChangeCharToInt(pData[1]);
    iCID1 = MAKE_BYTE(iTmp1, iTmp2);

    iTmp1 = ChangeCharToInt(pData[2]);
    iTmp2 = ChangeCharToInt(pData[3]);
    iCID2 = MAKE_BYTE(iTmp1, iTmp2);

    iTmp1 = ChangeCharToInt(pData[4]);
    iTmp2 = ChangeCharToInt(pData[5]);
    int iTmp3 = ChangeCharToInt(pData[6]);
    int iTmp4 = ChangeCharToInt(pData[7]);
    iLen = MAKE_LONG(iTmp1, iTmp2, iTmp3, iTmp4);

    iTmp1 = ChangeCharToInt(pData[8]);
    iTmp2 = ChangeCharToInt(pData[9]);
    iTmp3 = ChangeCharToInt(pData[10]);
    iTmp4 = ChangeCharToInt(pData[11]);
    iChkSum = MAKE_LONG(iTmp1, iTmp2, iTmp3, iTmp4);
}

QString MgrPowerDevice::getData(int iCID1, int iCID2)
{
    QString ret;
    ret.clear();

    QVector<ST_POWER_SAMPLER_SHEET>::iterator iter = m_qvSamplerData.begin();
    for(; iter < m_qvSamplerData.end();iter++)
    {
        if (iter->iCID1 == iCID1 && iter->iCID2 == iCID2)
        {
            QString strTmp;
            strTmp.clear();

            if (iter->iGain == 0)
            {
                for(int i = 0; i < iter->iByte;i++)
                {
                    strTmp += "  ";
                }
            }
            else
            {
                if(iter->isFloat) //float value
                {
                    float fValue = (iter->fValue) * (iter->iGain);
                    strTmp = ChangeFloatToHexString(fValue);
                }
                else  //int value
                {
                    int iValue = (iter->fValue) * (iter->iGain);

                    //倍数为0，则表示此值不支持，赋值为无效值
                    if (iter->iGain == 0)
                    {
                        for(int i = 0; i < iter->iByte;i++)
                        {
                            strTmp += "  ";
                        }
                    }
                    else if (iter->iByte == 1)
                    {
                        unsigned char cValue = iValue;
                        strTmp.sprintf("%02X", cValue);
                    }
                    else if (iter->iByte == 2)
                    {
                        unsigned short sValue = iValue;
                        strTmp.sprintf("%04X", sValue);
                    }
                    else if (iter->iByte == 4)
                    {
                        strTmp.sprintf("%08X", iValue);
                    }
                    else
                    {
                        for (int j = 0; j < iter->iByte;j++)
                        {
                            strTmp += "  ";
                        }
                    }
                }
            }

            ret += strTmp;
        }
    }

    return ret;
}

void MgrPowerDevice::setData(int iCID1, int iCID2, QByteArray szBuf)
{
    char* pData = szBuf.data();

    int iTmp1 = ChangeCharToInt(pData[8]);
    int iTmp2 = ChangeCharToInt(pData[9]);

    int iCmdType = MAKE_BYTE(iTmp1, iTmp2);

    for(int i = 0; i < m_qvSetData.length();i++)
    {
        qDebug()<<"iCmdType data:"<<iCmdType<<", "<<m_qvSetData.at(i).iCmdTyte;
        if(iCID1 == m_qvSetData.at(i).iCID1
                && iCID2 == m_qvSetData.at(i).iCID2
                && iCmdType == m_qvSetData.at(i).iCmdTyte)
        {
            if(m_qvSetData.at(i).isFloat)
            {
                float fValue = 0.0;

                QString tmp;
                tmp.sprintf("%02X%02X%02X%02X",
                            MAKE_BYTE(ChangeCharToInt(pData[10]),ChangeCharToInt(pData[11])),
                            MAKE_BYTE(ChangeCharToInt(pData[12]),ChangeCharToInt(pData[13])),
                            MAKE_BYTE(ChangeCharToInt(pData[14]),ChangeCharToInt(pData[15])),
                            MAKE_BYTE(ChangeCharToInt(pData[16]),ChangeCharToInt(pData[17])));

                qDebug() << "tmp is: " << tmp;
                fValue = ChangeHexStringToFloat(tmp);
                m_qvSetData[i].fValue = fValue;
            }
            else
            {
                int iValue = 0;
                if (m_qvSetData.at(i).iByte == 1)
                {
                    iValue = MAKE_BYTE(ChangeCharToInt(pData[10]), ChangeCharToInt(pData[11]));
                }
                else
                {
                    iValue = MAKE_LONG(ChangeCharToInt(pData[10]), ChangeCharToInt(pData[11]),
                                       ChangeCharToInt(pData[12]), ChangeCharToInt(pData[13]));
                }

                 m_qvSetData[i].fValue = iValue;
            }


            qDebug()<<"set data:"<<m_qvSetData[i].fValue;
            //emit refreshDisplay(1, i, m_qvSetData[i].fValue);

            return;
        }
    }

}

void MgrPowerDevice::processRequest(QByteArray RecvData)
{

}

int MgrPowerDevice::findByRegName(QString strRegAddr, int iBeginIndex, int iItemIndex)
{
    if (iItemIndex == 0)
    {
        for(int i = iBeginIndex; i < m_qvSamplerData.length();i++)
        {
            if (m_qvSamplerData.at(i).strName.indexOf(strRegAddr) >= 0)
            {
                return i;
            }
        }

        return -1;
    }
    else
    {
        for(int i = iBeginIndex; i < m_qvSetData.length();i++)
        {
            if (m_qvSetData.at(i).strName.indexOf(strRegAddr) >= 0)
            {
                return i;
            }
        }

        return -1;
    }

}

//解析Excel
int MgrPowerDevice::getFileContent(QString strFilePath)
{

}


int MgrPowerDevice::OutputFile(QString strFilePath, QString *pFileName)
{

}





