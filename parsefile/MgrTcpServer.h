#ifndef MODBUSTCPSERVERMGR_H
#define MODBUSTCPSERVERMGR_H

#include <QFile>

#include "MgrDataBase.h"
#include "comm/icommTCP.h"

#define EXCEL_RIGISTER_ROW_MODEL_TYPE   0
#define EXCEL_RIGISTER_ROW_READCODE     1
#define EXCEL_RIGISTER_ROW_WRITECODE    2
#define EXCEL_RIGISTER_ROW_DATATYPE     3
#define EXCEL_RIGISTER_ROW_DESCRIBLE    4
#define EXCEL_RIGISTER_ROW_START        5   //根据Excel确定,目前Modbus寄存器从第5行开始计算

typedef struct{
    QString RegName;
    int RegAddr;
    int iScale;
    float value;
}ST_CFG_MODBUS_TCP_SIG;

typedef struct{
    int iReadCode;
    int iWriteCode;
    int iResponseType;   //0:按字节（WORD） 1：按bit（BIT） 2: 按float型(FLOAT) 大小端 => emun
    QVector<ST_CFG_MODBUS_TCP_SIG> vcData;
}ST_TCP_WORK_SHEET_CONTENT;

typedef struct{
    int iAddr;
    int iFuncCode;
    int iStartReg;
    int iReqLen;
}ST_MODBUS_TCP_PDU;

typedef struct{
    int iTransID;
    const int iDentify = 0x0000;
    int iLength;
    ST_MODBUS_TCP_PDU pdu;
}ST_MODBUS_TCP_PDU_REQUEST;


class MgrModbusTcpServer: public MgrDataBase
{
    Q_OBJECT

public:
    explicit MgrModbusTcpServer(qint16 listenPort);
    ~MgrModbusTcpServer();

    QString getMgrName();

    int parseCfgFile(QString strFilePath);

    QVector<ST_DISPLAY_STRUCT> getDisplayData(int index);
    void* getDisplayData2(int* pCount);

    int findByRegName(QString strRegName, int iBeginIndex, int iPageIndex);
    bool modifyValue(void* pt);
    int OutputFile(QString strFilePath, OUT QString *pFileName);

public slots:    
    void processRequest(QByteArray RecvData);
    void notify(EMessageType type, QString str);

private:
    QString MgrName = "Modbus-TCP";

    QVector<ST_TCP_WORK_SHEET_CONTENT> m_workSheet;

    int getFileContent(QString strFilePath);
    int findByRegAddr(int iRegAddr, QVector<ST_CFG_MODBUS_TCP_SIG> &vcData);

    void parseVariantContent(QVariant var, ST_TCP_WORK_SHEET_CONTENT &stSheet);
    void insertVertor(QVariantList rowData, QVector<ST_CFG_MODBUS_TCP_SIG> &vc);
    void clearDataBuff();

    //R/W
    QByteArray setTcpWordData(int iRegAddr, int iValue, int iPageIndex);
    QByteArray getTcpWordData(int iBegin, int iNum, int iPageIndex);
    QByteArray getTcpWordL2Data(int iBegin, int iNum, int iPageIndex);
    QByteArray getTcpByteData(int iBegin, int iNum, int iPageIndex);
    QByteArray getTcpByteDataLittleEnd(int iBegin, int iNum, int iPageIndex);
    QByteArray getTcpFloatDataLittleEnd(int iBegin, int iNum, int iPageIndex);
    QByteArray getTcpFloatDataBigEnd(int iBegin, int iNum, int iPageIndex);
    QByteArray getTcpFloatDataSW(int iBegin, int iNum, int iPageIndex);

};

#endif // MODBUSTCPSERVERMGR_H
