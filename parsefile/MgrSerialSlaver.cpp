/*******************************************
 *  parsefile/MgrSerialSlaver.cpp
 *
 *  (C) 2021  Vertiv.co
 *******************************************/

#include "MgrSerialSlaver.h"

MgrModbusSlaver::MgrModbusSlaver()
{
    m_pCurrComm = new iCommSerial;

    connect(m_pCurrComm,SIGNAL(ePrintMessage(EMessageType,QString)), this,SLOT(notify(EMessageType, QString)));
    connect(m_pCurrComm,SIGNAL(ePrintData(QByteArray,QString)), this,SIGNAL(mgrReadWriteData(QByteArray,QString)));
    connect(m_pCurrComm,SIGNAL(eGetData(QByteArray)), this,SLOT(processRequest(QByteArray)));

    m_workSheet.clear();
}

MgrModbusSlaver::~MgrModbusSlaver()
{
    clearDataBuff();
}

QString MgrModbusSlaver::getMgrName()
{
    return this->MgrName;
}

void MgrModbusSlaver::notify(EMessageType type, QString str)
{
    emit mgrMessageTip(mgr_type_serial, type, str);
}


int MgrModbusSlaver::parseCfgFile(QString strFilePath)
{
    clearDataBuff();
    return getFileContent(strFilePath);
}


QVector<ST_DISPLAY_STRUCT> MgrModbusSlaver::getDisplayData(int index)
{
    QVector<ST_DISPLAY_STRUCT> qvRet;
    qvRet.clear();

    QVector<ST_CFG_MODBUS_RTU_SIG>::iterator iter = m_workSheet[index].vcData.begin();
    for(;iter < m_workSheet[index].vcData.end();iter++)
    {
        ST_DISPLAY_STRUCT stTemp;
        stTemp.RegName = iter->RegName;
        stTemp.value = iter->value;
        qDebug() << iter->RegName << "  " << iter->value;
        qvRet.append(stTemp);
    }
    return qvRet;
}

void *MgrModbusSlaver::getDisplayData2(int *pCount)
{
   Q_UNUSED(pCount);
   return nullptr;
}


QVector<ST_CFG_MODBUS_RTU_SIG> MgrModbusSlaver::getSamplerSig(int index)
{
    return m_workSheet[index].vcData;
}

void MgrModbusSlaver::processRequest(QByteArray RecvData)
{    
    QByteArray szResponse;
    bool bRet = true;
    qDebug() << "=============================================================";

    //01 | 03 | 00 00 | 00 64 | 44 21
    ST_MODBUS_RTU_PDU pdu;
    pdu.iDevAddr = (int)RecvData[0];
    pdu.iFuncCode = (int)RecvData[1];
    pdu.iStartReg = MAKE_WORD(RecvData[2] ,RecvData[3]);
    pdu.iLen = MAKE_WORD(RecvData[4] ,RecvData[5]);
    pdu.iCRC[0] = RecvData[6];
    pdu.iCRC[1] = RecvData[7];

    int nSheetIndex = -1;
    for(int i = 0; i < m_workSheet.length(); i++)
    {
        if(pdu.iFuncCode == m_workSheet.at(i).iReadCode || pdu.iFuncCode == m_workSheet.at(i).iWriteCode)
        {
            if(this->findByRegAddr(pdu.iStartReg, m_workSheet[i].vcData) != -1)
            {
                nSheetIndex = i;
                break;
            }
        }
    }
    qDebug() << "request pdu <<< Addr=" << pdu.iDevAddr << " Func=" << pdu.iFuncCode << " register=" << pdu.iStartReg << " len=" << pdu.iLen;

    QByteArray szData;
    if(nSheetIndex == -1)
    {
        //没找到寄存器首地址，返回
        qDebug() << "没找到寄存器首地址，返回";
        bRet = false;
    }
    else
    {
        //找到寄存器首地址，按首地址解析
        // Read register, Makeup protocol
        qDebug() << "ReadCode " << m_workSheet.at(nSheetIndex).iReadCode;

        if(pdu.iFuncCode == m_workSheet.at(nSheetIndex).iReadCode)
        {
            qDebug() << "m_workSheet.at(nSheetIndex).iResponseType " << m_workSheet.at(nSheetIndex).iResponseType;
            switch(m_workSheet.at(nSheetIndex).iResponseType)
            {
            case MODBUS_VALUE_TYPE_BYTE:
                szData = getByteData(pdu.iStartReg, pdu.iLen, nSheetIndex);
                break;
            case MODBUS_VALUE_TYPE_BYTE_LE:
                szData = getByteDataLittleEnd(pdu.iStartReg, pdu.iLen, nSheetIndex);
                break;
            case MODBUS_VALUE_TYPE_WORD:
                szData = getWordData(pdu.iStartReg, pdu.iLen, nSheetIndex);
                break;
            case MODBUS_VALUE_TYPE_WORD_LE:
                szData = getWordDataLittleEnd(pdu.iStartReg, pdu.iLen, nSheetIndex);
                break;
            case MODBUS_VALUE_TYPE_WORD_L2:
                szData = getWordL2Data(pdu.iStartReg, pdu.iLen, nSheetIndex);
                break;
            case MODBUS_VALUE_TYPE_FLOAT_LSW: //4Byte
                szData = getFloatDataSW(pdu.iStartReg, pdu.iLen, nSheetIndex);
                break;
            case MODBUS_VALUE_TYPE_FLOAT_BE: //4Byte
                szData = getFloatDataBigEnd(pdu.iStartReg, pdu.iLen, nSheetIndex);
                break;
            case MODBUS_VALUE_TYPE_FLOAT_LE: //4Byte
                szData = getFloatDataLittleEnd(pdu.iStartReg, pdu.iLen, nSheetIndex);
                break;
            }
        }
        // Write register, Makeup protocol
        if(pdu.iFuncCode == m_workSheet.at(nSheetIndex).iWriteCode)
        {
            switch(m_workSheet.at(nSheetIndex).iResponseType)
            {
            case MODBUS_VALUE_TYPE_WORD:
                szData = setWordData(pdu.iStartReg, pdu.iLen, nSheetIndex);
                break;
            default:
                bRet = false;
            }
        }
    }

    qDebug() << " " << bRet;
    if(!bRet){
        szData[0] = 0xFF; //Not Found
        szData[1] = 0xFF; //Not Found
    }
    szResponse[0] = pdu.iDevAddr;
    szResponse[1] = pdu.iFuncCode;
    szResponse += szData;    

    this->feedbackResponse(szResponse); // CRC generated on the lower level interface
}


//匹配寄存器地址
int MgrModbusSlaver::findByRegAddr(int iRegAddr, QVector<ST_CFG_MODBUS_RTU_SIG>& vcData)
{
    for(int i = 0; i < vcData.length();i++)
    {
        if (vcData.at(i).RegAddr == iRegAddr)
        {
            return i;
        }
    }
    return -1;
}


int MgrModbusSlaver::findByRegName(QString strRegName, int iBeginIndex, int iPageIndex)
{
    for(int i = iBeginIndex; i < m_workSheet.at(iPageIndex).vcData.length();i++)
    {
        if (m_workSheet.at(iPageIndex).vcData.at(i).RegName.indexOf(strRegName) >= 0)
        {
            return i;
        }
    }
    return -1;
}


int MgrModbusSlaver::OutputFile(QString strFilePath, QString *pFileName)
{
    Q_UNUSED(strFilePath);
    return 0;
}


void MgrModbusSlaver::clearDataBuff()
{
    for(int i = 0; i < m_workSheet.length();i++)
    {
        m_workSheet[i].vcData.clear();
    }

    m_workSheet.clear();
}

QByteArray MgrModbusSlaver::getWordData(int iBegin, int iNum, int iPageIndex)
{    
    QByteArray ret;
    ret.clear();
    ret[0] = iNum*2;

    qDebug()<< "iBegin: " << iBegin << " iPageIndex: " << iPageIndex;

    //iNum is Totally register count.
    for(int i=0; i < iNum; i++)
    {
        int index = findByRegAddr(iBegin+i, m_workSheet[iPageIndex].vcData);        
        if (index == -1)
        {
            ret[i*2+1] = 0;
            ret[i*2+2] = 0;
            continue;
        }

        int value = m_workSheet[iPageIndex].vcData[index].value * m_workSheet[iPageIndex].vcData[index].iScale;

        ret[i*2+1] = HIBYTEOFWORD(value);
        ret[i*2+2] = LOBYTEOFWORD(value);
    }

    return ret;
}

QByteArray MgrModbusSlaver::getWordDataLittleEnd(int iBegin, int iNum, int iPageIndex)
{
    QByteArray ret;
    ret.clear();
    ret[0] = iNum*2;

    qDebug()<< "start:" << iBegin << "num:" << iNum << " page:" << iPageIndex;

    //iNum is Totally register count.
    for(int i=0; i < iNum; i++)
    {
        int index = findByRegAddr(iBegin+i, m_workSheet[iPageIndex].vcData);
        if (index == -1)
        {
            ret[i*2+1] = 0;
            ret[i*2+2] = 0;
            continue;
        }

        int value = m_workSheet[iPageIndex].vcData[index].value * m_workSheet[iPageIndex].vcData[index].iScale;

        ret[i*2+1] = LOBYTEOFWORD(value);
        ret[i*2+2] = HIBYTEOFWORD(value);
    }

    return ret;
}

QByteArray MgrModbusSlaver::getWordL2Data(int iBegin, int iNum, int iPageIndex)
{
    QByteArray ret;
    ret.clear();

    ret[0] = HIBYTEOFWORD(iNum*2);
    ret[1] = LOBYTEOFWORD(iNum*2);

    for(int i = 0;i < iNum;i++)
    {
        int index = findByRegAddr(iBegin+i, m_workSheet[iPageIndex].vcData);
        if(-1 == index)
        {
            ret[i*2+2] = 0;
            ret[i*2+3] = 0;
            continue;
        }

        //qDebug()<<"iRegAddr:"<<(iBegin+i)<<",:"<<m_workSheet[iPageIndex].vcData[index].DefaultValue * m_workSheet[iPageIndex].vcData[index].multiple
        //       <<"iIndex:"<<index<<", :"<<iPageIndex;

        int value = m_workSheet[iPageIndex].vcData[index].value * m_workSheet[iPageIndex].vcData[index].iScale;
        ret[i*2+2] = HIBYTEOFWORD(value);
        ret[i*2+3] = LOBYTEOFWORD(value);
    }

    return ret;
}

QByteArray MgrModbusSlaver::getByteData(int iBegin, int iNum, int iPageIndex)
{    
    QByteArray ret;
    ret.clear();

    ret[0] = (iNum+7)/8;
    unsigned char value = 0;
    unsigned char valueTmp = 0;

    for(int i = 0;i < iNum;i++)
    {
        qDebug()<<"getByteData iNum:" << i;
        int index = findByRegAddr(iBegin+i, m_workSheet[iPageIndex].vcData);
        if(-1 == index)
        {
            valueTmp = 0;
        }
        else
        {
            valueTmp = (((int)(m_workSheet[iPageIndex].vcData[index].value)) & 0x01);
        }

        qDebug()<<"getByteData value orgin:" << valueTmp;

        switch (i%8) {
            case 0:
                valueTmp = (valueTmp  << 7);
                break;
            case 1:
                valueTmp = (valueTmp  << 6);
                break;
            case 2:
                valueTmp = (valueTmp  << 5);
                break;
            case 3:
                valueTmp = (valueTmp  << 4);
                break;
            case 4:
                valueTmp = (valueTmp  << 3);
                break;
            case 5:
                valueTmp = (valueTmp  << 2);
                break;
            case 6:
                valueTmp = (valueTmp  << 1);
                break;
            case 7:
            default:
                break;
        }

        value += valueTmp;
        ret[(i/8)+1] = value;
        if((i+1)%8 == 0)
        {
            value = 0;
        }

        qDebug()<<"getByteData value:"<<value<<", index:"<<((i/8)+1)<<", ret"<< ret[(i/8)+1];
    }
    return ret;
}

QByteArray MgrModbusSlaver::getByteDataLittleEnd(int iBegin, int iNum, int iPageIndex)
{
    qDebug() << "getByteDataLittleEnd start";
    QByteArray ret;
    ret.clear();

    ret[0] = (iNum + 7) / 8;

    unsigned char value = 0;
    unsigned char valueTmp = 0;
    for(int i = 0;i < iNum; i++)
    {
        qDebug()<<"getByteDataLittleEnd iNum: " << i;
        int index = findByRegAddr(iBegin + i, m_workSheet[iPageIndex].vcData);
        if(-1 == index)
        {
            valueTmp = 0;
        }
        else
        {
            valueTmp = (((int)(m_workSheet[iPageIndex].vcData[index].value)) & 0x01);
        }
        qDebug()<<"getByteDataLittleEnd value orgin:" << valueTmp;

        switch (i % 8) {
        case 0:
            break;
        case 1:
            valueTmp = (valueTmp  << 1);
            break;
        case 2:
            valueTmp = (valueTmp  << 2);
            break;
        case 3:
            valueTmp = (valueTmp  << 3);
            break;
        case 4:
            valueTmp = (valueTmp  << 4);
            break;
        case 5:
            valueTmp = (valueTmp  << 5);
            break;
        case 6:
            valueTmp = (valueTmp  << 6);
            break;
        case 7:
            valueTmp = (valueTmp  << 7);
            break;
        default:
            break;
        }

        value += valueTmp;
        ret[(i/8)+1] = value;
        if((i+1)%8 == 0)
        {
            value = 0;
        }

        qDebug()<<"getByteDataLittleEnd value:"<<value<<", index:"<<((i/8)+1)<<", ret"<<ret[(i/8)+1];
    }

    return ret;
}

QByteArray MgrModbusSlaver::getFloatDataLittleEnd(int iBegin, int iNum, int iPageIndex)
{
    qDebug()<<"getByteData start";
    QByteArray ret;
    ret.clear();

    int iRegNum = ((iNum % 2) == 0) ? (iNum / 2) : (iNum / 2 + 1);

    ret[0] = iRegNum * 4;

    for(int i = 0, j = 0; j < iRegNum; i += 2, j++)
    {
        int index = findByRegAddr(iBegin+i, m_workSheet[iPageIndex].vcData);
        if(-1 == index)
        {
            ret[j*4+1] = 0;
            ret[j*4+2] = 0;
            ret[j*4+3] = 0;
            ret[j*4+4] = 0;
            continue;
        }

        union STRTOFLOAT
        {
               float f_value;      // 浮点值
               unsigned char by_value[5];   // 16进制的字符串
        }floatvalue;

        floatvalue.f_value = m_workSheet[iPageIndex].vcData[index].value * m_workSheet[iPageIndex].vcData[index].iScale;

        qDebug() << "regId: " << iBegin+i
                 << "FloatO: " << m_workSheet[iPageIndex].vcData[index].value
                 << "FloatR: " << floatvalue.f_value
                 << "Float[1] : " <<  floatvalue.by_value[0]
                 << "Float[2] : " <<  floatvalue.by_value[1]
                 << "Float[3] : " <<  floatvalue.by_value[2]
                 << "Float[4] : " <<  floatvalue.by_value[3];

        ret[j*4+1] = floatvalue.by_value[0];
        ret[j*4+2] = floatvalue.by_value[1];
        ret[j*4+3] = floatvalue.by_value[2];
        ret[j*4+4] = floatvalue.by_value[3];
    }

    return ret;
}

QByteArray MgrModbusSlaver::getFloatDataBigEnd(int iBegin, int iNum, int iPageIndex)
{
    qDebug()<<"getByteData start";
    QByteArray ret;
    ret.clear();

    int iRegNum = ((iNum % 2) == 0) ? (iNum / 2) : (iNum / 2 + 1);

    ret[0] = iRegNum * 4;

    for(int i = 0, j = 0; j < iRegNum; i += 2, j++)
    {
        int index = findByRegAddr(iBegin+i, m_workSheet[iPageIndex].vcData);
        if(-1 == index)
        {
            ret[j*4+1] = 0;
            ret[j*4+2] = 0;
            ret[j*4+3] = 0;
            ret[j*4+4] = 0;
            continue;
        }

        union STRTOFLOAT
        {
               float f_value;      // 浮点值
               unsigned char by_value[5];   // 16进制的字符串
        }floatvalue;

        floatvalue.f_value = m_workSheet[iPageIndex].vcData[index].value * m_workSheet[iPageIndex].vcData[index].iScale;

        qDebug() << "regId: " << iBegin+i
                 << "FloatO: " << m_workSheet[iPageIndex].vcData[index].value
                 << "FloatR: " << floatvalue.f_value
                 << "Float[1] : " <<  floatvalue.by_value[0]
                 << "Float[2] : " <<  floatvalue.by_value[1]
                 << "Float[3] : " <<  floatvalue.by_value[2]
                 << "Float[4] : " <<  floatvalue.by_value[3];

        ret[j*4+1] = floatvalue.by_value[3];
        ret[j*4+2] = floatvalue.by_value[2];
        ret[j*4+3] = floatvalue.by_value[1];
        ret[j*4+4] = floatvalue.by_value[0];
    }

    return ret;
}

QByteArray MgrModbusSlaver::getFloatDataSW(int iBegin, int iNum, int iPageIndex)
{
    qDebug()<<"getByteData start";
    QByteArray ret;
    ret.clear();

    int iRegNum = ((iNum % 2) == 0) ? (iNum / 2) : (iNum / 2 + 1);

    ret[0] = iRegNum * 4;


    for(int i = 0, j = 0; j < iRegNum; i += 2, j++)
    {
        int index = findByRegAddr(iBegin+i, m_workSheet[iPageIndex].vcData);
        if(-1 == index)
        {
            ret[j*4+1] = 0;
            ret[j*4+2] = 0;
            ret[j*4+3] = 0;
            ret[j*4+4] = 0;
            continue;
        }

        union STRTOFLOAT
        {
               float f_value;  // 浮点值
               unsigned char by_value[5];   // 16进制的字符串
        }floatvalue;

        floatvalue.f_value = m_workSheet[iPageIndex].vcData[index].value * m_workSheet[iPageIndex].vcData[index].iScale;

        qDebug() << "regId: " << iBegin+i
                 << "FloatO: " << m_workSheet[iPageIndex].vcData[index].value
                 << "FloatR: " << floatvalue.f_value
                 << "Float[1] : " <<  floatvalue.by_value[0]
                 << "Float[2] : " <<  floatvalue.by_value[1]
                 << "Float[3] : " <<  floatvalue.by_value[2]
                 << "Float[4] : " <<  floatvalue.by_value[3];

        ret[j*4+1] = floatvalue.by_value[1];
        ret[j*4+2] = floatvalue.by_value[0];
        ret[j*4+3] = floatvalue.by_value[3];
        ret[j*4+4] = floatvalue.by_value[2];
    }

    return ret;
}

//写寄存器
QByteArray MgrModbusSlaver::setWordData(int iRegAddr, int iValue, int iPageIndex)
{
    QByteArray ret;
    ret.clear();
    qDebug() << "index" << iRegAddr;

    qDebug() << "iRegAddr" << iValue;

    int index = findByRegAddr(iRegAddr, m_workSheet[iPageIndex].vcData);

    qDebug() << "index" << index;

    if (-1 != index)
    {
        qDebug() << m_workSheet.length();

        m_workSheet[iPageIndex].vcData[index].value = (float)iValue / m_workSheet[iPageIndex].vcData[index].iScale;

        qDebug() << "set data: " << m_workSheet[iPageIndex].vcData[index].value
                 << ", index: "  << index << ", iPageIndex: " << iPageIndex;

        emit mgrRefreshDisplay(iPageIndex, index, m_workSheet[iPageIndex].vcData[index].value);
    }

    ret[0] = HIBYTEOFWORD(iRegAddr);
    ret[1] = LOBYTEOFWORD(iRegAddr);

    ret[2] = HIBYTEOFWORD(iValue);
    ret[3] = LOBYTEOFWORD(iValue);

    return ret;
}

bool MgrModbusSlaver::modifyValue(void *pt)
{
    ST_MODIFY_VALUE_OF_MODBUS* p = (ST_MODIFY_VALUE_OF_MODBUS*)pt;
    qDebug() << "Page:" << p->iPageIndex << "Index:" << p->index << "Set Value:" << p->fValue;

    m_workSheet[p->iPageIndex].vcData[p->index].value = p->fValue;
    qDebug() << m_workSheet[p->iPageIndex].vcData[p->index].value;

    return true;
}


//初始化每个信号寄存器
void MgrModbusSlaver::insertVertor(QVariantList rowData, QVector<ST_CFG_MODBUS_RTU_SIG> &vc)
{
    ST_CFG_MODBUS_RTU_SIG stTmp;
    stTmp.RegName.clear();
    stTmp.RegAddr = 0;    
    stTmp.value = 0;
    stTmp.iScale = 0;

    //switch(rowData.size())
    //{
    //case 4:
    //    stTmp.value = rowData[3].toFloat();
    //case 3:
    //    stTmp.iScale = rowData[2].toDouble();
    //case 2:
    //    stTmp.RegAddr = rowData[1].toDouble();
    //case 1:
    //    stTmp.RegName = rowData[0].toString();
    //    break;
    //default:
    //    ;
    //}

    stTmp.RegName = rowData[0].toString();
    stTmp.RegAddr = rowData[1].toDouble();
    stTmp.iScale  = rowData[2].toDouble();
    stTmp.value   = rowData[3].toFloat();

    if (stTmp.iScale == 0)
    {
        stTmp.iScale = 1;
    }
    stTmp.value /= stTmp.iScale;

    qDebug() << ">> RegName  " << stTmp.RegName << "RegAddr  " << stTmp.RegAddr << "iScale  " << stTmp.iScale ;

    if ((stTmp.RegName =="") && (stTmp.RegAddr==0))
    {
        //return;
    }

    vc.append(stTmp);
}



//Excel赋值初始化
void MgrModbusSlaver::parseVariantContent(QVariant var, ST_RTU_WORK_SHEET_CONTENT &stSheet)
{    
    QVariantList varRows = var.toList(); //:->
    if(varRows.isEmpty())
    {
        return;
    }
    const int rowCount = varRows.size();

    QVariantList rowData;
    bool ok;

    qDebug() << varRows[EXCEL_RIGISTER_ROW_MODEL_TYPE].toList()[1].toString();

    stSheet.iReadCode = varRows[EXCEL_RIGISTER_ROW_READCODE].toList()[1].toString().toUInt(&ok, 16);
    stSheet.iWriteCode = varRows[EXCEL_RIGISTER_ROW_WRITECODE].toList()[1].toString().toUInt(&ok, 16);
    QString strValueType = varRows[EXCEL_RIGISTER_ROW_DATATYPE].toList()[1].toString();

    if(strValueType == "BYTE")
    {
         stSheet.iResponseType = MODBUS_VALUE_TYPE_BYTE;
    }
    else if(strValueType == "BYTE-LE")
    {
         stSheet.iResponseType = MODBUS_VALUE_TYPE_BYTE_LE;
    }
    else if(strValueType == "WORD")
    {
         stSheet.iResponseType = MODBUS_VALUE_TYPE_WORD;
    }
    else if(strValueType == "WORD-LE")
    {
         stSheet.iResponseType = MODBUS_VALUE_TYPE_WORD_LE;
    }
    else if(strValueType == "WORD-L2")
    {
         stSheet.iResponseType = MODBUS_VALUE_TYPE_WORD_L2;
    }
    else if(strValueType == "FLOAT-LSW")
    {
        stSheet.iResponseType = MODBUS_VALUE_TYPE_FLOAT_LSW;
    }
    else if(strValueType == "FLOAT-BE")
    {
        stSheet.iResponseType = MODBUS_VALUE_TYPE_FLOAT_BE;
    }
    else if(strValueType == "FLOAT-LE")
    {
        stSheet.iResponseType = MODBUS_VALUE_TYPE_FLOAT_LE;
    }
    else
    {
        stSheet.iResponseType = MODBUS_VALUE_TYPE_WORD;  //default is word
    }

    qDebug("ReadFuncCode(0x%02X) | WriteCode(0x%02X) | DateType(0x%02X)",
           stSheet.iReadCode,
           stSheet.iWriteCode,
           stSheet.iResponseType);

    //+++ 做一个简单的判断 +++

    //foreach signal insert to st
    qDebug() << "rowCount" << rowCount;
    for(int i = EXCEL_RIGISTER_ROW_START; i < rowCount; ++i)
    {
        rowData = varRows[i].toList();
        insertVertor(rowData, stSheet.vcData);
    }
}


//解析Excel
int MgrModbusSlaver::getFileContent(QString strFilePath)
{        
    qDebug() << "#2.Excel File Loading...";
    QAxObject *excel = new QAxObject("Excel.Application");  //建立excel操作对象
    if(!excel)
    {
        qDebug()<<"Failed to create Excel!";
    }
    excel->dynamicCall("SetVisible (bool Visible)","false");//设置窗体不可视
    excel->setProperty("DisplayAlerts", false);             //不显示任何警告信息，如关闭时的是否保存提示

    QAxObject *workbooks = excel->querySubObject("WorkBooks");//获取工作簿(excel文件)集合
    workbooks->dynamicCall("Open(const QString&)", strFilePath);

    QAxObject *workbook = excel->querySubObject("ActiveWorkBook");
    QAxObject *worksheets = workbook->querySubObject("Sheets");

    int SheetPageCount = worksheets->property("Count").toInt();
    qDebug() << QString("Excel sheet num is %1").arg(QString::number(SheetPageCount));

    for (int i = 1; i < SheetPageCount + 1; i++)
    {
        QAxObject *worksheet = worksheets->querySubObject("Item(int)",i); //获取第i个sheet
        QAxObject *usedRange = worksheet->querySubObject("UsedRange");    //获取该sheet的数据范围

        //https://zhidao.baidu.com/question/2057539371315827427.html
        //https://stackoverflow.com/questions/28671649/c-qt-range-of-excel-querysubobject
        QAxObject *rows = usedRange->querySubObject("Rows");       //获取行数
        QAxObject *columns = usedRange->querySubObject("Columns"); //获取列数
        int iRows = rows->property("Count").toInt();
        int iColumns = columns->property("Count").toInt();
        qDebug("#Excel %d [row %d , col %d]",i ,iRows - EXCEL_RIGISTER_ROW_START, iColumns);
        QVariant var = usedRange->dynamicCall("Value");

        ST_RTU_WORK_SHEET_CONTENT stTmp;
        parseVariantContent(var, stTmp);
        m_workSheet.append(stTmp);
    }
    qDebug() << "#3.Excel File Loading Serial OK...";

    workbook->dynamicCall( "Close(Boolean)", false );

    //已加载,关闭excel程序,操作完成后记着关闭,由于是隐藏的看不到，不关闭进程会有很多excel.exe
    excel->dynamicCall("Quit(void)");
    delete excel;

    return SheetPageCount;
}
