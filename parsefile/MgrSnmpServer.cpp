/*******************************************
 *  parsefile/MgrSnmpServer.cpp
 *
 *  (C) 2021  Vertiv.co
 *******************************************/

#include "MgrSnmpServer.h"

MgrSNMPServer::MgrSNMPServer(qint16 listenPort)
{
//    if (m_instance_mgrsnmp == nullptr)
//    {
        m_pCurrComm = new iCommSnmp(listenPort);

        connect(m_pCurrComm,SIGNAL(ePrintMessage(EMessageType,QString)), this,SLOT(notify(EMessageType,QString)));
        connect(m_pCurrComm,SIGNAL(ePrintData(QByteArray,QString)), this,SIGNAL(mgrReadWriteData(QByteArray,QString)));
        connect(m_pCurrComm,SIGNAL(eGetData(QByteArray)), this,SLOT(processRequest(QByteArray)));

        m_SnmpWorkSheet.clear();

        //m_instance_mgrsnmp = new MgrSNMPServer();
//    }
//    return m_instance_mgrsnmp;

}

MgrSNMPServer::~MgrSNMPServer()
{
    //delete tree
    clearDataBuff();
}

QString MgrSNMPServer::getMgrName()
{
    return this->MgrName;
}

void MgrSNMPServer::clearDataBuff()
{
    this->m_SnmpWorkSheet.clear();
}

int MgrSNMPServer::parseCfgFile(QString strFilePath)
{    
    clearDataBuff();
    return getFileContent(strFilePath);
}

QVector<ST_DISPLAY_STRUCT> MgrSNMPServer::getDisplayData(int index)
{
    QVector<ST_DISPLAY_STRUCT> qvRet;
    qvRet.clear();

    QVector<ST_SNMP_SHEET_CONTENT>::iterator iter = m_SnmpWorkSheet.begin();
    for(;iter < m_SnmpWorkSheet.end(); iter++)
    {
        ST_DISPLAY_STRUCT stTemp = {iter->strName, iter->fValue};
        //stTemp.RegName = iter->RegName;
        //stTemp.value   = iter->value;
        qDebug() << iter->strName << "  " << iter->fValue;
        qvRet.append(stTemp);
    }
    return qvRet;
}

void *MgrSNMPServer::getDisplayData2(int *pCount)
{
    Q_UNUSED(pCount);
    return nullptr;
}

bool MgrSNMPServer::modifyValue(void *pt)
{
    ST_MODIFY_VALUE_OF_MODBUS* p = (ST_MODIFY_VALUE_OF_MODBUS*)pt;
    qDebug() << "Page:" << p->iPageIndex << "Index:" << p->index << "Set Value:" << p->fValue;

    m_SnmpWorkSheet[p->index].fValue = p->fValue;
    qDebug() << "成功修改 : " << m_SnmpWorkSheet[p->index].fValue;

    return true;
}

int MgrSNMPServer::findByRegName(QString strRegName, int iBeginIndex, int iPageIndex)
{
    for(int i = iBeginIndex; i < m_SnmpWorkSheet.length();i++)
    {
        if (m_SnmpWorkSheet.at(i).strName.indexOf(strRegName) >= 0)
        {
            return i;
        }
    }
    return -1;
}

/**
 * @brief MgrSNMPServer::processRequest
 * @param RecvData : dealwith RAW SNMP protocal data[]
 */
void MgrSNMPServer::processRequest(QByteArray RecvData)
{
    pdu_snmp_protocal pdu;
    int errcode=0;

    unsigned int index = 0;
    pdu.asnType = RecvData[index++];
    pdu.length  = RecvData[index++];
    pdu.stVersion.asnType = RecvData[index++];
    pdu.stVersion.length  = RecvData[index++];
    pdu.stVersion.obj     = RecvData[index++];

    qDebug() << "pdu.asnType:" << pdu.asnType;
    qDebug() << "pdu.length:" << pdu.length;
    qDebug() << "pdu.stVersion.asnType:" << pdu.stVersion.asnType;
    qDebug() << "pdu.stVersion.length:" << pdu.stVersion.length;
    qDebug() << "pdu.stVersion.obj:" << pdu.stVersion.obj;

    if( pdu.asnType != asnType::BER_TYPE_SEQUENCE )
    {
        errcode = -1;
        qDebug() << "errcode " << errcode;
        return;
    }
    if( pdu.length != (RecvData.length()-2) )
    {
        errcode = -2;
        qDebug() << "errcode " << errcode;
        return;
    }

    if( pdu.stVersion.asnType != asnType::BER_TYPE_INTEGER )
    {
        errcode = -3;
        qDebug() << "errcode " << errcode;
        return;
    }
    if( pdu.stVersion.length != 0x01 )
    {
        errcode = -4;
        qDebug() << "errcode " << errcode;
        return;
    }
    qDebug() << "=== Version:" << pdu.stVersion.obj;
    //get string version func ++

    //==========================================================
    pdu.stCommunity.asnType = RecvData[index++];
    pdu.stCommunity.length  = RecvData[index++];
    qDebug() << "pdu.stCommunity.asnType:" << pdu.stCommunity.asnType;
    qDebug() << "pdu.stCommunity.length:" << pdu.stCommunity.length;

    if( pdu.stCommunity.asnType != asnType::BER_TYPE_OCTET_STRING )
    {
        errcode = -5;
        qDebug() << "errcode " << errcode;
        return;
    }

    for(int i=0; i < pdu.stCommunity.length; ++i)
    {
        pdu.stCommunity.obj[i] = RecvData[index++];
    }
    qDebug() << "=== Community:" << pdu.stCommunity.obj;
    //==========================================================

    pdu.stVariable.asnType = RecvData[index++]; // <==> 0xA0
    pdu.stVariable.length  = RecvData[index++];

    qDebug() << "pdu.stVariable.asnType" << pdu.stVariable.asnType;
    qDebug() << "pdu.stVariable.length"  << pdu.stVariable.length;
    //应对 'GET' 0xA0
//    if( pdu.stVariable.asnType != asnType::BER_TYPE_SNMP_GET ) //ONLY USE IN 'GET' 0xA0
//    {
//        errcode = -11;
//        qDebug() << "errcode " << errcode << "pdu.stVariable.asnType" << pdu.stVariable.asnType;
//        return;
//    }

    if( (pdu.stVariable.length + index) != (pdu.length + 2 * sizeof(BYTE)) )
    {
        errcode = -12;
        qDebug() << "errcode " << errcode;
        return;
    }
    //==========================================================

    /*
      30 2f 02 01 00 04 06 70 75 62 6c 69 63 a0 22 02 04 31 a2 1f 9e 02 01 00 02 01 00 30 14 30 12 06 0e 2b 06 01 04 01 83 5c 01 2a 03 04 03 05 00 05 00

      30 29 02 01 00 04 06 70 75 62 6c 69 63 a0 1c 02 04 52 2a 3b 7b 02 01 00 02 01 00 30 0e 30 0c 06 08 2b 06 01 02 01 0b 12 00 05 00
      30 29 02 01 01 04 06 70 75 62 6C 69 63 A0 1C 02 04 52 2A 3B 91 02 01 00 02 01 00 30 0E 30 0C 06 08 2B 06 01 02 01 0B 12 00 05 00

      a0 1c
      02 04 52 2a 3b 7b 02 01 00 02 01 00 30 0e 30 0c 06 08 2b 06 01 02 01 0b 12 00 05 00

      30 0C == 06 08 2B 06 01 02 01 0B 12 00 ==
      05 00

    */
    pdu.stVariable.obj.stRequestId.asnType = RecvData[index++];
    pdu.stVariable.obj.stRequestId.length  = RecvData[index++];
    pdu.stVariable.obj.stRequestId.obj.szbyte[0] = RecvData[index++];
    pdu.stVariable.obj.stRequestId.obj.szbyte[1] = RecvData[index++];
    pdu.stVariable.obj.stRequestId.obj.szbyte[2] = RecvData[index++];
    pdu.stVariable.obj.stRequestId.obj.szbyte[3] = RecvData[index++];

    qDebug("request_id  %ul", pdu.stVariable.obj.stRequestId.obj.ul);

    RecvData[index++]; // == 0x02
    RecvData[index++]; // == 0x01
    pdu.stVariable.obj.error_status = RecvData[index++];
    RecvData[index++]; // == 0x02
    RecvData[index++]; // == 0x01
    pdu.stVariable.obj.error_index = RecvData[index++];

    qDebug() << "error_status " << pdu.stVariable.obj.error_status;
    qDebug() << "error_index "  << pdu.stVariable.obj.error_index;

    //========================================================== get_request

    pdu.stVariable.obj.st_variable_binding.asnType = RecvData[index++];
    pdu.stVariable.obj.st_variable_binding.length  = RecvData[index++];

    qDebug() << "pdu.stVariable.obj.st_variable_binding.asnType "
             <<  pdu.stVariable.obj.st_variable_binding.asnType;
    qDebug() << "pdu.stVariable.obj.st_variable_binding.length "
             <<  pdu.stVariable.obj.st_variable_binding.length;

    if( pdu.stVariable.obj.st_variable_binding.asnType != asnType::BER_TYPE_SEQUENCE )
    {
        errcode = -20;
        qDebug() << "errcode " << errcode;
        return;
    }

    if( (pdu.stVariable.obj.st_variable_binding.length + index) != (pdu.length + 2 * sizeof(BYTE)) )
    {
        errcode = -21;
        qDebug() << "errcode " << errcode;
        return;
    }
    //========================================================== oid + value

    pdu.stVariable.obj.st_variable_binding.stData.asnType = RecvData[index++];
    pdu.stVariable.obj.st_variable_binding.stData.length  = RecvData[index++];

    qDebug() << "pdu.stVariable.obj.st_variable_binding.stData.asnType "
             <<  pdu.stVariable.obj.st_variable_binding.stData.asnType;
    qDebug() << "pdu.stVariable.obj.st_variable_binding.stData.length "
             <<  pdu.stVariable.obj.st_variable_binding.stData.length;

    if( pdu.stVariable.obj.st_variable_binding.stData.asnType != asnType::BER_TYPE_SEQUENCE )
    {
        errcode = -22;
        qDebug() << "errcode " << errcode;
        return;
    }

    if( (pdu.stVariable.obj.st_variable_binding.stData.length + index) != (pdu.length + 2 * sizeof(BYTE)) )
    {
        errcode = -23;
        qDebug() << "errcode " << errcode;
        return;
    }

    //========================================================== oid

    pdu.stVariable.obj.st_variable_binding.stData.stOid.asnType = RecvData[index++];
    pdu.stVariable.obj.st_variable_binding.stData.stOid.length  = RecvData[index++];

    qDebug() << "pdu.stVariable.obj.st_variable_binding.stData.stOid.asnType "
             <<  pdu.stVariable.obj.st_variable_binding.stData.stOid.asnType;
    qDebug() << "pdu.stVariable.obj.st_variable_binding.stData.stOid.length "
             <<  pdu.stVariable.obj.st_variable_binding.stData.stOid.length;

    if( pdu.stVariable.obj.st_variable_binding.stData.stOid.asnType != asnType::BER_TYPE_OID )
    {
        errcode = -30;
        qDebug() << "errcode " << errcode;
        return;
    }

    for(int i=0; i < pdu.stVariable.obj.st_variable_binding.stData.stOid.length; ++i)
    {
        pdu.stVariable.obj.st_variable_binding.stData.stOid.szobj[i] = RecvData[index++];
        qDebug("szoid[%d]: %d", i, pdu.stVariable.obj.st_variable_binding.stData.stOid.szobj[i]);
    }
    //========================================================== v

    pdu.stVariable.obj.st_variable_binding.stData.v.asnType = RecvData[index++];
    pdu.stVariable.obj.st_variable_binding.stData.v.length  = RecvData[index++];

    qDebug() << "pdu.stVariable.obj.st_variable_binding.stData.v.asnType "
             <<  pdu.stVariable.obj.st_variable_binding.stData.v.asnType;
    qDebug() << "pdu.stVariable.obj.st_variable_binding.stData.v.length "
             <<  pdu.stVariable.obj.st_variable_binding.stData.v.length;

    if( pdu.stVariable.obj.st_variable_binding.stData.v.asnType != asnType::BER_TYPE_NULL )
    {
        errcode = -31;
        qDebug() << "errcode " << errcode;
        return;
    }

    if( pdu.stVariable.obj.st_variable_binding.stData.v.length != asnType::BER_TYPE_EndMarker )
    {
        errcode = -32;
        qDebug() << "errcode " << errcode;
        return;
    }

    qDebug() << "get request pdu ok, now make up send pdu";

    //=============================================================================

    //=============================================================================
    pdu_snmp_protocal send_pdu;
    index = 0;

    QByteArray szBuffer;
    QByteArray szTmp;
    int IVaule = 1; //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< +++
    szBuffer.clear();

    //value
    szTmp[0] = asnType::BER_TYPE_INTEGER;
    //send_pdu.stVariable.obj.st_variable_binding.stData.v.objValue = IVaule;
    szTmp[2] = (IVaule >> 8) & 0xff;
    szTmp[3] = IVaule & 0xff;
    send_pdu.stVariable.obj.st_variable_binding.stData.v.length = 0x02;
    szTmp[1] = send_pdu.stVariable.obj.st_variable_binding.stData.v.length;
    szBuffer = szTmp + szBuffer;

    //oid
    QByteArray szTmp1;
    szTmp1[0] = asnType::BER_TYPE_OID;
    szTmp1[1] = pdu.stVariable.obj.st_variable_binding.stData.stOid.length;
    for(int i=0; i < szTmp1[1]; ++i)
    {
        szTmp1[2 + i] = pdu.stVariable.obj.st_variable_binding.stData.stOid.szobj[i];
    }
    szBuffer = szTmp1 + szBuffer;

    //items = value + oid
    QByteArray szTmp2;
    szTmp2[0] = asnType::BER_TYPE_SEQUENCE;
    szTmp2[1] = szBuffer.length();
    szBuffer = szTmp2 + szBuffer;

    //var-binding-items
    QByteArray szTmp22;
    szTmp22[0] = asnType::BER_TYPE_SEQUENCE;
    szTmp22[1] = szBuffer.length();
    szBuffer = szTmp22 + szBuffer;

    QByteArray szTmp3;
    BYTE szErrStatus[] = {asnType::BER_TYPE_INTEGER, 0x01, SNMP_ERR_NOERROR};
    BYTE szErrIndex[] =  {asnType::BER_TYPE_INTEGER, 0x01, SNMP_ERR_NOERROR};
    for(int i=0; i < sizeof(szErrStatus); ++i)
    {
        szTmp3[i] = szErrStatus[i];
    }
    szBuffer = szTmp3 + szBuffer;

    szTmp3.clear();
    for(int i=0; i < sizeof(szErrIndex); ++i)
    {
        szTmp3[i] = szErrIndex[i];
    }
    szBuffer = szTmp3 + szBuffer;

    QByteArray szTmp4;
    BYTE szRquestId[] = {asnType::BER_TYPE_INTEGER, 0x04, 0xFF, 0xFF, 0xFF, 0xFF};
    szTmp4[0] = szRquestId[0];
    szTmp4[1] = szRquestId[1];
    szTmp4[2] = pdu.stVariable.obj.stRequestId.obj.szbyte[0];
    szTmp4[3] = pdu.stVariable.obj.stRequestId.obj.szbyte[1];
    szTmp4[4] = pdu.stVariable.obj.stRequestId.obj.szbyte[2];
    szTmp4[5] = pdu.stVariable.obj.stRequestId.obj.szbyte[3];
    szBuffer = szTmp4 + szBuffer;

    QByteArray szTmp5;
    szTmp5[0] = asnType::BER_TYPE_SNMP_RESPONSE;
    szTmp5[1] = szBuffer.length();
    szBuffer = szTmp5 + szBuffer;

    QByteArray szTmp6;
    szTmp6[0] = pdu.stCommunity.asnType;
    szTmp6[1] = pdu.stCommunity.length;
    for(int i=0; i < pdu.stCommunity.length; ++i)
    {
        szTmp6[2 + i] = pdu.stCommunity.obj[i];
    }
    szBuffer = szTmp6 + szBuffer;

    QByteArray szTmp7;
    szTmp7[0] = pdu.stVersion.asnType;
    szTmp7[1] = pdu.stVersion.length;
    szTmp7[2] = pdu.stVersion.obj;
    szBuffer = szTmp7 + szBuffer;

    QByteArray szTmp8;
    szTmp8[0] = asnType::BER_TYPE_SEQUENCE;
    szTmp8[1] = szBuffer.length();
    szBuffer = szTmp8 + szBuffer;

    BYTE szSendData[]= { 0x30, 0x2e,                                      //2
                         0x02, 0x01, 0x00,                                //3
                         0x04, 0x06, 0x70, 0x75, 0x62, 0x6c, 0x69, 0x63,  //8
                         0xa2, 0x21,                                      //2
                         0x02, 0x04, 0xFF, 0xFF, 0xFF, 0xFF,              //6
                         0x02, 0x01, 0x00,                                //3
                         0x02, 0x01, 0x00,                                //3
                         0x30, 0x13};                                     //2

    //=======================================
    QString strPrint;
    QString data = szBuffer.toHex();
    for(int i = 0;i < data.length()/2;i++)
    {
        strPrint.append(data.at(i * 2).toUpper());
        strPrint.append(data.at(i * 2 + 1).toUpper());
        strPrint += " ";
    }
    qDebug() << "strPrint: " << strPrint;
    //=======================================

    this->feedbackResponse(szBuffer);

    return;
}

void MgrSNMPServer::notify(EMessageType type, QString str)
{
    emit mgrMessageTip(mgr_type_udp, type, str);
}

/**
 * @brief MgrSNMPServer::getFileContent 解析EXCEL
 * @param strFilePath
 * @return
 */
int MgrSNMPServer::getFileContent(QString strFilePath)
{
    qDebug() << "# inn " << strFilePath;
    QAxObject *excel = new QAxObject("Excel.Application"); //建立excel操作对象
    excel->dynamicCall("SetVisible (bool Visible)","false"); //设置为不显示窗体
    excel->setProperty("DisplayAlerts", false); //不显示任何警告信息，如关闭时的是否保存提示

    QAxObject *workbooks = excel->querySubObject("WorkBooks");//获取工作簿(excel文件)集合
    workbooks->dynamicCall("Open(const QString&)", strFilePath);

    QAxObject *workbook = excel->querySubObject("ActiveWorkBook");
    QAxObject *worksheets = workbook->querySubObject("Sheets");

    int iPageNum = worksheets->property("Count").toInt();

    qDebug() << "iPageNum = " << iPageNum;
    if (iPageNum >= 1)
    {
        //先解析第一个sheet页
        QAxObject *worksheet = worksheets->querySubObject("Item(int)",1);
        QAxObject *usedRange = worksheet->querySubObject("UsedRange");
        QVariant var = usedRange->dynamicCall("Value");
        int ret = parseInfo(var);
        if (ret == _ERROR)
        {
            iPageNum = -1;
        }
    }
    if (iPageNum >= 2)
    {
        //QAxObject *worksheet = worksheets->querySubObject("Item(int)",2);
        //QAxObject *usedRange = worksheet->querySubObject("UsedRange");
        //QVariant var = usedRange->dynamicCall("Value");//这里是所有的内容
        //parseSetData(var);
    }

    //关闭excel程序,操作完成后记着关闭;
    //由于是隐藏的看不到，不关闭进程会有很多excel.exe
    workbook->dynamicCall( "Close(Boolean)", false );
    excel->dynamicCall("Quit(void)");
    delete excel;

    return iPageNum;
}

//解析EXCEL->SHEET1
int MgrSNMPServer::parseInfo(QVariant var)
{
    QVariantList varRows = var.toList();
    if(varRows.isEmpty())
    {
        return _ERROR;
    }

    const int rowCount = varRows.size();
    qDebug() << "row count:" << rowCount;
    QVariantList rowData;

    for(int i = EXCEL_SNMP_ROW_MODEL_TYPE; i < rowCount; ++i) //从第一行开始
    {
        rowData = varRows[i].toList();

        if(i == EXCEL_SNMP_ROW_MODEL_TYPE)
        {
            if(rowData[1].toString() != this->MgrName)
            {
                qDebug() << "Value Type" << rowData[1].toString();
                qDebug() << "Value Type" << this->MgrName;
                return _ERROR;
            }
        }
        else if(i == EXCEL_SNMP_ROW_VALUE_TYPE)
        {
            qDebug() << "Value Type" << rowData[1].toString();
        }
        else if(i == EXCEL_SNMP_ROW_VALUE_NAME)
        {
            qDebug() << "rowData[" << i << "]"
                     << rowData[0].toString()  //strName
                     << rowData[1].toString()  //strOID
                     << rowData[2].toString()  //iScale
                     << rowData[3].toString(); //fValue
        }
        else
        {
            qDebug() << "rowData[" << i << "]"
                     << rowData[0].toString()  //strName
                     << rowData[1].toString()  //strOID
                     << rowData[2].toString()  //iScale
                     << rowData[3].toString(); //fValue

            ST_SNMP_SHEET_CONTENT stTmp;
            stTmp.strName = rowData[0].toString();
            stTmp.strOID  = rowData[1].toString();
            stTmp.iScale  = rowData[2].toInt();
            stTmp.fValue  = rowData[3].toFloat();

            m_SnmpWorkSheet.append(stTmp);
        }
    }

    return _OK;
}


int MgrSNMPServer::OutputFile(QString strFilePath, QString *pFileName)
{
    Q_UNUSED(strFilePath);
    return 0;
}
