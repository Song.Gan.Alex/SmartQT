#ifndef COMMSERIAL_H
#define COMMSERIAL_H

#include <QSerialPort>
#include <QSerialPortInfo>
#include <QtAlgorithms>

//#include <QModbusPdu>
//#include <QModbusRtuSerialSlave>
//#include <QModbusServer>
#include "icommBase.h"

class iCommSerial: public iCommBase
{
    Q_OBJECT

public:
    explicit iCommSerial();
    ~iCommSerial();

    QVector<ST_COMM_PORT_INFO> _getPortInfo();

    bool _openPort(void *pPortParam);
    void _closePort();
    void _writeData(QByteArray buf);

public slots:
    void _readData(); //get->emit->(pMgr->read->dealwith->write)->set

private:
    int m_iDeviceAddr;
    int m_iLastErrCode;
    QVector<ST_COMM_PORT_INFO> m_CommPortNameInfo;   //端口信息

    QSerialPort *m_pSerialPort;

    QVector<QSerialPortInfo> m_CommSerialInfo;

    bool filterData(QByteArray buf);
    int CheckSumSerial(BYTE *pSrc, INT32 iDataLen);
    bool CheckReceiveData(BYTE *pRecv, INT32 iRecv);
};


#endif // COMMSERIAL_H
