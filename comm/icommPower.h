#ifndef COMMPOWERTOTAL_H
#define COMMPOWERTOTAL_H

#include <QSerialPort>
#include <QSerialPortInfo>

#include "icommBase.h"

class iCommPower : public iCommBase
{
    Q_OBJECT

public:
    iCommPower();
    ~iCommPower();

    QVector<ST_COMM_PORT_INFO> _getPortInfo();

    bool _openPort(void *pPortParam);
    void _closePort();
    void _writeData(QByteArray SendData);

public slots:
    void _readData();

private:
    QSerialPort *m_pSerialPort;

    int m_iDeviceAddr;
    int m_iLastErrCode;

    QVector<QSerialPortInfo> m_CommSerialInfo;
    QVector<ST_COMM_PORT_INFO> m_CommPortNameInfo;

    int ChangeCharToInt(const char cSrc);
    bool filterData(QByteArray buf);
    unsigned short calcChkSum(char *str, INT32 len);
    unsigned short calcLengthChkSum(INT32 len);
};



#endif // COMMPOWERTOTAL_H
