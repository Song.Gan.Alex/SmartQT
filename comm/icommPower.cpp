#include "icommPower.h"

iCommPower::iCommPower()
{
    qDebug() << "CommPower ###";

    m_iDeviceAddr = 0;
    m_pSerialPort = NULL;

    //m_pDataMgr = new CPowerDataManager();
    //connect(m_pDataMgr, SIGNAL(refreshDisplay(int,int,float)),this, SIGNAL(refreshDisplay(int,int,float)));
}

iCommPower::~iCommPower()
{
    _closePort();
}


QVector<ST_COMM_PORT_INFO> iCommPower::_getPortInfo()
{
    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
    {
        QSerialPort tmpSerial;
        tmpSerial.setPort(info);

        if (tmpSerial.open(QIODevice::ReadWrite))
        {
            ST_COMM_PORT_INFO tmp;
            memcpy(tmp.PortName,info.portName().toStdString().c_str(),sizeof(tmp.PortName));
            memcpy(tmp.PortDescription,info.description().toStdString().c_str(),sizeof(tmp.PortDescription));

            m_CommPortNameInfo.append(tmp);
            tmpSerial.close();
        }
    }

    return m_CommPortNameInfo;
}

int iCommPower::ChangeCharToInt(const char cSrc)
{
    if (cSrc >= '0' && cSrc <= '9')
    {
        return cSrc - '0';
    }
    else if (cSrc >= 'A' && cSrc <= 'F')
    {
        return cSrc - 'A' + 10;
    }
    else if (cSrc >= 'a' && cSrc <= 'f')
    {
        return cSrc - 'a' + 10;
    }

    return 0;
}

//after receiving the data, check whether the message address is correct
bool iCommPower::filterData(QByteArray buf)
{
    char* pData = buf.data();

    int iTmp1 = ChangeCharToInt(pData[3]);
    int iTmp2 = ChangeCharToInt(pData[4]);
    int iAddr = MAKE_BYTE(iTmp1, iTmp2);

    if (this->m_iDeviceAddr != iAddr)
    {
        return false;
    }
    return true;
}

//校验校验位
unsigned short iCommPower::calcChkSum(char* str, INT32 len)
{
    unsigned short iRet = 0;
    for (int i = 0;i < len;i++)
    {
        iRet += (int)str[i];
    }

    iRet = (~iRet) + 1;
    return iRet;
}


//接收到数据正确性校验
unsigned short iCommPower::calcLengthChkSum(INT32 len)
{
    int iLen = (len & 0x000F) + ((len & 0x00F0)>>4) + ((len & 0x0F00)>>8);
    unsigned char cTmp = iLen%16;
    cTmp = ~cTmp + 1;

    unsigned short iRet = (unsigned short)(cTmp<<12)+len;
    return iRet;
}


//read + dealwith + send
void iCommPower::_readData()
{    
    static QByteArray buf;    

    //if(m_pSerialPort->bytesAvailable() >= 10 ) // n为一次需要读取的字节数
    //{
    //    buf += m_pSerialPort->readAll();
    //}
    buf += m_pSerialPort->readAll();

    qDebug() << "read request: " << buf;
    // ~21012A420000FDA3\r
    // ~21012A000000FDA9
    // ~21-01-2A-42-0000-FDA3
    // ver  21
    // addr 01
    // CID1 2A
    // CID2 42

    if(!buf.endsWith(0x0D))
    {
        return;
    }

    bool bRet = filterData(buf);
    if (!bRet)
    {
        buf.clear();
        qDebug() << " filterData err!!!!";
        this->m_pSerialPort->clear();
        return;
    }
    //emit PrintData(buf, "Recv");    

    QByteArray szRecv = buf.mid(5, buf.length() - 6);
    QByteArray szData;    

    QString strSendBuf;
    strSendBuf.clear();
    strSendBuf += QString(buf.left(7).data());
    strSendBuf += "00";  //数据正常发送
    strSendBuf += QString::asprintf("%04X", calcLengthChkSum(szData.length()));  //len
    strSendBuf += QString(szData); //数据

    int iCalc = calcChkSum(strSendBuf.toLatin1().data()+1, strSendBuf.length());

    strSendBuf += QString::asprintf("%02X", iCalc);
    strSendBuf += 0x0D;

    //emit PrintData(strSendBuf.toLatin1(), "Send");

    qDebug() << "send buff: " << strSendBuf.toLatin1().data();
    m_pSerialPort->write(strSendBuf.toLatin1().data(), strSendBuf.length());

    buf.clear();
}

void iCommPower::_writeData(QByteArray SendData)
{

}

bool iCommPower::_openPort(void *pPortParam)
{
    qDebug() << "### open port";

    if(m_pSerialPort != NULL)
    {
        return false;
    }

    ST_PORT_PARAM *pParam = (ST_PORT_PARAM *)pPortParam;

    //设备地址
    m_iDeviceAddr = pParam->iDeviceAddr;
    m_pSerialPort = new QSerialPort();

    //设置串口名
    m_pSerialPort->setPortName(pParam->strPortName);

    //打开串口
    bool bRet = m_pSerialPort->open(QIODevice::ReadWrite);
    if (!bRet)
    {
        qDebug() << "### open port failure";
        return false;
    }

    //设置波特率
    bRet = m_pSerialPort->setBaudRate(pParam->iBaudRate );
    if (!bRet)
    {
        m_pSerialPort->close();
        return false;
    }

    //设置数据位数
    bRet = m_pSerialPort->setDataBits(QSerialPort::Data8);
    if (!bRet)
    {
        m_pSerialPort->close();
        return false;
    }

     //设置奇偶校验
     bRet = m_pSerialPort->setParity(QSerialPort::NoParity);
     if (!bRet)
     {
         m_pSerialPort->close();
         return false;
     }

     //设置停止位
     switch(pParam->iStopBit)
     {
        case 0: bRet = m_pSerialPort->setStopBits(QSerialPort::OneStop); break;
        case 1: bRet = m_pSerialPort->setStopBits(QSerialPort::TwoStop); break;
        default: break;
     }
    if (!bRet)
    {
        m_pSerialPort->close();
        return false;
    }

    //设置流控制
    m_pSerialPort->setFlowControl(QSerialPort::SoftwareControl);

    QObject::connect(m_pSerialPort, &QSerialPort::readyRead, this, &iCommPower::_readData);

    qDebug() << "### open port succeed";
    return true;
}

void iCommPower::_closePort()
{
    if (m_pSerialPort == NULL)
    {
        return;
    }
    m_pSerialPort->clear();
    m_pSerialPort->close();
    m_pSerialPort->deleteLater();

    delete m_pSerialPort;
    m_pSerialPort = NULL;
}




