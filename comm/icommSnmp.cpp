#include <QHostInfo>

#include "icommSnmp.h"

#define DefaultHostIP  "127.0.0.1"

iCommSnmp::iCommSnmp(qint16 listenPort)
{
    //创建QSnmpSever对象
    m_UdpServer = new QUdpSocket(this);
    m_hostPort = listenPort;
}

iCommSnmp::~iCommSnmp()
{
    this->_closePort();
    m_UdpServer = NULL;
}

/**
 * @brief iCommSnmp::getLocalIP
 * @return 获取本地IP
 */
QString iCommSnmp::getLocalIP()
{
    qDebug() << "getLocalIP";
    QString hostName = QHostInfo::localHostName();
    QHostInfo hostInfo = QHostInfo::fromName(hostName);
    QString localIP = "";
    QList<QHostAddress> addList = hostInfo.addresses();

    if(!addList.isEmpty())
    {
        for (int i = 0; i < addList.count(); i++)
        {
            QHostAddress aHost = addList.at(i);
            if(QAbstractSocket::IPv4Protocol == aHost.protocol())
            {
                localIP = aHost.toString();
                break;
            }
        }
    }
    return localIP;
    //return DefaultHostIP; //127.0.0.1 use for local test
}

/**
 * @brief iCommSnmp::_getPortInfo
 * @return 用于UI界面显示
 */
QVector<ST_COMM_PORT_INFO> iCommSnmp::_getPortInfo()
{
    ST_COMM_PORT_INFO tmp;
    memcpy(tmp.PortName, this->getLocalIP().toStdString().c_str(), sizeof(tmp.PortName));
    memcpy(tmp.PortDescription, QNetworkInterface().name().toStdString().c_str(), sizeof(tmp.PortDescription));

    qDebug() << "tmp.PortName" << tmp.PortName << " tmp.PortDescription" << tmp.PortDescription;

    m_CommPortNameInfo.append(tmp);

    return m_CommPortNameInfo;
}



/*******************************************************************
 *
 *  For UDP sockets, after binding, the signal QUdpSocket::readyRead()
 * is emitted whenever a UDP datagram arrives on the specified address
 * and port. Thus, This function is useful to write UDP servers.
 *
 *******************************************************************/
bool iCommSnmp::_openPort(void *pPortParam)
{
    //根据UI的下发端口信息打开Socket服务端口
    m_hostPort = ((ST_NETWORK_PORT_PARAM*)pPortParam)->nPort;
    m_hostAddr = ((ST_NETWORK_PORT_PARAM*)pPortParam)->IP; //dfs

    qDebug() << "Bind网口 "    << m_hostAddr;
    qDebug() << "Bind监听端口 " << m_hostPort;

    //Binds this socket to the address address and the port port.
    //m_UdpServer->bind(QHostAddress("10.163.230.227"), m_hostPort);
    m_UdpServer->bind(QHostAddress(m_hostAddr), m_hostPort);

    connect(m_UdpServer, SIGNAL(readyRead()), this, SLOT(onNewConnection()));

    return true;
}


void iCommSnmp::onNewConnection()
{
    qDebug("发现新客户端正在接入...");
    return this->_readData();
}

void iCommSnmp::_readData()
{
    while(m_UdpServer->hasPendingDatagrams())
    {
        QHostAddress sender;
        QByteArray recvbuf;
        quint16 senderPort;

        //1.read Data
        //receives a datagram no larger than maxSize bytes and stores it in data.
        recvbuf.resize(m_UdpServer->pendingDatagramSize());

        //The sender's host address and port is stored in *address and *port.
        m_UdpServer->readDatagram(recvbuf.data(),
                                  recvbuf.size(),
                                  &sender,
                                  &senderPort);
        m_senderPort = senderPort;
        m_senderIP = sender.toString();

        qDebug() << "UDP receive size[" << recvbuf.size()
                 << "]addr:" << m_senderIP
                 << "data:" << recvbuf.toHex();

        //2.check Addr
        bool bRet = filterData(recvbuf);
        if (!bRet)
        {
            this->m_iLastErrCode = -err_recv_address;
            goto err;
        }

        //3.check CRC
        bRet = CheckReceiveData((BYTE*)recvbuf.data(), recvbuf.length());
        if (!bRet)
        {
            this->m_iLastErrCode = -err_recv_crc_check;
            goto err;
        }

        qDebug() << "emit ePrintData(recvbuf, Recv)" ;
        emit ePrintData(recvbuf, "Recv");
        emit eGetData(recvbuf);
    }
    return;

err:
    qDebug() << "m_iLastErrCode: " << this->m_iLastErrCode;
    return;
}

void iCommSnmp::_writeData(QByteArray buf)
{
    if(m_UdpServer == NULL || buf == NULL)
    {
        return;
    }
    emit ePrintData(buf, "Send");

    m_UdpServer->writeDatagram(buf, QHostAddress(m_senderIP), m_senderPort);
}

bool iCommSnmp::filterData(QByteArray pRecv)
{
    Q_UNUSED(pRecv);

    return true;
}

bool iCommSnmp::CheckReceiveData(BYTE *pRecv, INT32 iRecv)
{
    Q_UNUSED(pRecv);

    return (iRecv >= SNMP_MAX_LEN)? false:true;
}


void iCommSnmp::_closePort()
{
    if(m_UdpServer != NULL)
    {
        m_UdpServer->close();
    }

    qDebug() << "finished close Port";
}

