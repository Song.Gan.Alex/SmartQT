#ifndef COMMSNMP_H
#define COMMSNMP_H

#include <QUdpSocket>
#include <QNetworkInterface>
#include <QMessageBox>

#include "icommBase.h"
#include "include/snmpversion.h"
#include "include/snmpvardefine.h"
#include "include/snmperrcode.h"

//SNMP UDP Server
class iCommSnmp : public iCommBase
{
    Q_OBJECT

public:
    explicit iCommSnmp(qint16 listenPort);
    ~iCommSnmp();

    QVector<ST_COMM_PORT_INFO> _getPortInfo();

    bool _openPort(void *pPortParam);
    void _closePort();
    void _writeData(QByteArray buf);

private:
    QUdpSocket *m_UdpServer; //TCP服务器
    QString m_hostAddr;
    quint16 m_hostPort; //161

    QString m_senderIP;
    quint16 m_senderPort;

    //Port端口信息
    QVector<ST_COMM_PORT_INFO> m_CommPortNameInfo;

    int m_iLastErrCode;

    QString getLocalIP();  //获取本地IP
    bool filterData(QByteArray pRecv);
    bool CheckReceiveData(BYTE *pRecv, INT32 iRecv);

    QMetaObject::Connection conn1;
    QMetaObject::Connection conn2;
    QMetaObject::Connection conn3;

public slots:
    void onNewConnection();  //newConnection()信号槽函数
    //void onClientDisconnected();  //disconnected()信号槽函数

    //void onSocketStateChange(QAbstractSocket::SocketState socketState);  //socketStateChange()信号槽函数，socket状态的变化
    //void onSocketReadyRead();  //readyRead()信号槽函数，读取数据
    void _readData();

};



#endif // COMMSNMP_H
