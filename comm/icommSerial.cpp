#include "icommSerial.h"

static UINT crcvalue[] = {
    0x0,0xc1c0,0x81c1,0x4001,0x1c3,0xc003,0x8002,0x41c2,0x1c6,0xc006,
    0x8007,0x41c7,0x5,0xc1c5,0x81c4,0x4004,0x1cc,0xc00c,0x800d,0x41cd,
    0xf,0xc1cf,0x81ce,0x400e,0xa,0xc1ca,0x81cb,0x400b,0x1c9,0xc009,
    0x8008,0x41c8,0x1d8,0xc018,0x8019,0x41d9,0x1b,0xc1db,0x81da,0x401a,
    0x1e,0xc1de,0x81df,0x401f,0x1dd,0xc01d,0x801c,0x41dc,0x14,0xc1d4,
    0x81d5,0x4015,0x1d7,0xc017,0x8016,0x41d6,0x1d2,0xc012,0x8013,0x41d3,
    0x11,0xc1d1,0x81d0,0x4010,0x1f0,0xc030,0x8031,0x41f1,0x33,0xc1f3,
    0x81f2,0x4032,0x36,0xc1f6,0x81f7,0x4037,0x1f5,0xc035,0x8034,0x41f4,
    0x3c,0xc1fc,0x81fd,0x403d,0x1ff,0xc03f,0x803e,0x41fe,0x1fa,0xc03a,
    0x803b,0x41fb,0x39,0xc1f9,0x81f8,0x4038,0x28,0xc1e8,0x81e9,0x4029,
    0x1eb,0xc02b,0x802a,0x41ea,0x1ee,0xc02e,0x802f,0x41ef,0x2d,0xc1ed,
    0x81ec,0x402c,0x1e4,0xc024,0x8025,0x41e5,0x27,0xc1e7,0x81e6,0x4026,
    0x22,0xc1e2,0x81e3,0x4023,0x1e1,0xc021,0x8020,0x41e0,0x1a0,0xc060,
    0x8061,0x41a1,0x63,0xc1a3,0x81a2,0x4062,0x66,0xc1a6,0x81a7,0x4067,
    0x1a5,0xc065,0x8064,0x41a4,0x6c,0xc1ac,0x81ad,0x406d,0x1af,0xc06f,
    0x806e,0x41ae,0x1aa,0xc06a,0x806b,0x41ab,0x69,0xc1a9,0x81a8,0x4068,
    0x78,0xc1b8,0x81b9,0x4079,0x1bb,0xc07b,0x807a,0x41ba,0x1be,0xc07e,
    0x807f,0x41bf,0x7d,0xc1bd,0x81bc,0x407c,0x1b4,0xc074,0x8075,0x41b5,
    0x77,0xc1b7,0x81b6,0x4076,0x72,0xc1b2,0x81b3,0x4073,0x1b1,0xc071,
    0x8070,0x41b0,0x50,0xc190,0x8191,0x4051,0x193,0xc053,0x8052,0x4192,
    0x196,0xc056,0x8057,0x4197,0x55,0xc195,0x8194,0x4054,0x19c,0xc05c,
    0x805d,0x419d,0x5f,0xc19f,0x819e,0x405e,0x5a,0xc19a,0x819b,0x405b,
    0x199,0xc059,0x8058,0x4198,0x188,0xc048,0x8049,0x4189,0x4b,0xc18b,
    0x818a,0x404a,0x4e,0xc18e,0x818f,0x404f,0x18d,0xc04d,0x804c,0x418c,
    0x44,0xc184,0x8185,0x4045,0x187,0xc047,0x8046,0x4186,0x182,0xc042,
    0x8043,0x4183,0x41,0xc181,0x8180,0x4040
};

iCommSerial::iCommSerial()
{
    m_iDeviceAddr = 0;
    m_pSerialPort = NULL;
}

iCommSerial::~iCommSerial()
{
    _closePort();
}

QVector<ST_COMM_PORT_INFO> iCommSerial::_getPortInfo()
{
    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
    {
        QSerialPort tmpSerial;
        tmpSerial.setPort(info);

        if (tmpSerial.open(QIODevice::ReadWrite))
        {
            ST_COMM_PORT_INFO tmp;
            memcpy(tmp.PortName,info.portName().toStdString().c_str(),sizeof(tmp.PortName));
            memcpy(tmp.PortDescription,info.description().toStdString().c_str(),sizeof(tmp.PortDescription));

            m_CommPortNameInfo.append(tmp);
            tmpSerial.close();
        }
    }
    return m_CommPortNameInfo;
}

void iCommSerial::_writeData(QByteArray buf)
{
    QByteArray byteSend;
    byteSend.clear();
    byteSend += buf;

    int iCRC = CheckSumSerial((BYTE*)byteSend.data(), byteSend.length());
    byteSend += HIBYTE(iCRC);
    byteSend += LOBYTE(iCRC);
    emit ePrintData(byteSend, "Send");

    //++++++++++++ print
    qDebug() << byteSend.toHex();
    //QString strPrint;
    //strPrint.clear();
    //QString strAll = byteSend.toHex();
    //for(int i=0; i < strAll.length()/2; i++)
    //{
    //    strPrint.append(strAll.at(i*2).toUpper());
    //    strPrint.append(strAll.at(i*2 + 1).toUpper());
    //    strPrint += " ";
    //}
    //qDebug() << strPrint;
    //++++++++++++ print

    //>>>>>>>>>>>>>>>>>>>>> Send Data >>>>>>>>>>>>>>>>>>>>>
    m_pSerialPort->write(byteSend.data(), byteSend.length());

    buf.clear();
}

//after receiving the data, check whether the message address is correct
bool iCommSerial::filterData(QByteArray buf)
{
    int recvAddr = (int)(unsigned char)(buf.at(0));
    return (m_iDeviceAddr != recvAddr)? false : true;
}

//check CRC
int iCommSerial::CheckSumSerial(IN OUT BYTE* pSrc, IN INT32 iDataLen)
{
    INT32 iLoop;
    UCHAR uchCRCHi = 0xFF;  //high byte of CRC initialized
    UCHAR uchCRCLo = 0xFF;  //low  byte of CRC initialized
    UCHAR ulndex;           //will index into CRC lookup table

    iLoop = iDataLen;

    //calculate the CRC
    while(iLoop--)
    {
        ulndex = uchCRCHi ^ (*pSrc++);
        uchCRCHi = uchCRCLo ^ ( crcvalue[ulndex]>>8 );
        uchCRCLo = crcvalue[ulndex] & 0xFF;
    }
    //qDebug("checked >>> uchCRCHi 0x%02X | uchCRCLo 0x%02X", uchCRCHi, uchCRCLo);

    return composeHiLoByte(uchCRCHi, uchCRCLo);
}

//接收到数据正确性校验
bool iCommSerial::CheckReceiveData(IN BYTE* pRecv, IN INT32 iRecvDataNum)
{
    UCHAR ucCheckHi,ucCheckLo;
    //ASSERT((pRecvFrame != NULL) && (iRecv != 0));

    if(iRecvDataNum < 2)
    {
        return false;
    }
    ucCheckHi = pRecv[iRecvDataNum - 2];
    ucCheckLo = pRecv[iRecvDataNum - 1];
    //qDebug("origin >>> ucCheckHi 0x%02X | ucCheckLo 0x%02X", ucCheckHi, ucCheckLo);

    int iCRC = CheckSumSerial(pRecv, iRecvDataNum - 2);
    if(iCRC != composeHiLoByte(ucCheckHi, ucCheckLo))
    {
        return false;
    }
    return true;
}

//read + dealwith + write
void iCommSerial::_readData()
{
    //1.check recv request info length
    int iRecvLen = m_pSerialPort->bytesAvailable();
    if (iRecvLen < 8){
        qDebug("errrrr RecvLen < 8 iRecvLen = %d\n", iRecvLen);
        this->m_iLastErrCode = -err_recv_request_info_length;
        return;
    }

    QByteArray recvbuf = m_pSerialPort->readAll(); // <<<<<< Recving

    //++++++++++++ print
    QString strPrint;
    strPrint.clear();
    QString strAll = recvbuf.toHex();
    for(int i=0; i < strAll.length()/2; i++)
    {
        strPrint.append(strAll.at(i*2).toUpper());
        strPrint.append(strAll.at(i*2 + 1).toUpper());
        strPrint += " ";
    }
    qDebug() << "origin " << strPrint;
    //++++++++++++ print


    //2.check recv address
    bool bRet = filterData(recvbuf);
    if (!bRet){
        qDebug() << "not me!!!";
        this->m_iLastErrCode = -err_recv_address; //ignore
        return;
    }

    //3.check CRC
    bRet = CheckReceiveData((BYTE*)recvbuf.data(), recvbuf.length());
    if (!bRet)
    {
        qDebug() << "error crc!!!";
        this->m_iLastErrCode = -err_recv_crc_check;
        return;
    }

    emit ePrintData(recvbuf, "Recv");
    emit eGetData(recvbuf);
}


bool iCommSerial::_openPort(void *pPortParam)
{
    if(m_pSerialPort != NULL)
    {
        return false;
    }
    ST_PORT_PARAM* pParam = (ST_PORT_PARAM*)pPortParam;

    //设备地址
    m_iDeviceAddr = pParam->iDeviceAddr;
    m_pSerialPort = new QSerialPort();

    //设置串口名
    qDebug() << "Open PortName : " + pParam->strPortName;
    m_pSerialPort->setPortName(pParam->strPortName);

    //打开串口
    bool bRet = m_pSerialPort->open(QIODevice::ReadWrite);
    if (!bRet)
    {
        return false;
    }

    //设置波特率
    qDebug() << pParam->iBaudRate;
    bRet = m_pSerialPort->setBaudRate(pParam->iBaudRate);
    if (!bRet)
    {
        m_pSerialPort->close();
        return false;
    }

    //设置数据位数
    bRet = m_pSerialPort->setDataBits(QSerialPort::Data8);
    if (!bRet)
    {
        m_pSerialPort->close();
        return false;
    }

    //设置奇偶校验
    bRet = m_pSerialPort->setParity(QSerialPort::NoParity);
    if (!bRet)
    {
        m_pSerialPort->close();
        return false;
    }

    //设置停止位
    switch(pParam->iStopBit)
    {
        case 0: bRet = m_pSerialPort->setStopBits(QSerialPort::OneStop); break;
        case 1: bRet = m_pSerialPort->setStopBits(QSerialPort::TwoStop); break;
        default: break;
    }
    if (!bRet)
    {
        m_pSerialPort->close();
        return false;
    }

    //设置流控制
    m_pSerialPort->setFlowControl(QSerialPort::NoFlowControl);

    QObject::connect(m_pSerialPort, &QSerialPort::readyRead, this, &iCommSerial::_readData); //:->

    return true;
}

void iCommSerial::_closePort()
{
    if (m_pSerialPort == NULL)
        return;

    m_pSerialPort->clear();
    m_pSerialPort->close();
    m_pSerialPort->deleteLater();
    delete m_pSerialPort;
    m_pSerialPort = NULL;
}
