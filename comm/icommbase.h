#ifndef COMMBASE_H
#define COMMBASE_H

#include <QDebug>
#include <QObject>
#include <QVector>
#include <QString>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <minwindef.h>
#include "include/publicdatatype.h"

using namespace std;

class iCommBase: public QObject
{
    Q_OBJECT

signals:
    void ePrintMessage(EMessageType,QString);    //显示消息->mgr->mainwd.ui
    void ePrintData(QByteArray,QString);   //显示接受和发送数据日志->mgr->mainwd.ui
    void eGetData(QByteArray);      //通讯数据->mgr

public:
    explicit iCommBase();
    virtual ~iCommBase();

    virtual QVector<ST_COMM_PORT_INFO> _getPortInfo(){}

    virtual bool _openPort(void *pPortParam) = 0;
    virtual void _closePort() = 0;
    virtual void _writeData(QByteArray SendData) = 0;

public slots:
    void _readData(){}

protected:
    int composeHiLoByte(BYTE ucHi, BYTE ucLo)
    {
        return MAKE_WORD(ucHi, ucLo);
    }

};

#endif // COMMBASE_H
