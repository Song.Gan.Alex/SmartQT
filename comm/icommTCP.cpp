#include <QHostInfo>

#include "icommTCP.h"

#define DefaultHostIP  "127.0.0.1"

iCommTCP::iCommTCP(qint16 listenPort)
{
    //创建QTcpSever对象
    m_tcpServer = new QTcpServer(this);
    m_tcpSocket = NULL;
    m_Port = listenPort;
}

iCommTCP::~iCommTCP()
{
    this->_closePort();
    m_tcpSocket = NULL;
    m_tcpServer = NULL;
}


/**
 * @brief iCommTCP::getLocalIP
 * @return 获取本地IP
 */
QString iCommTCP::getLocalIP()
{
    QString hostName = QHostInfo::localHostName();
    QHostInfo hostInfo = QHostInfo::fromName(hostName);
    QString localIP = "";
    QList<QHostAddress> addList = hostInfo.addresses();
    if(!addList.isEmpty())
    {
        for (int i = 0; i < addList.count(); i++)
        {
            QHostAddress aHost = addList.at(i);
            if(QAbstractSocket::IPv4Protocol == aHost.protocol())
            {
                localIP = aHost.toString();
                break;
            }
        }
    }
    return localIP;
    //return DefaultHostIP; //127.0.0.1 use for local test
}


/**
 * @brief iCommTCP::_getPortInfo
 * @return 用于UI界面显示
 */
QVector<ST_COMM_PORT_INFO> iCommTCP::_getPortInfo()
{
    qDebug() << getLocalIP().toStdString().c_str();

    ST_COMM_PORT_INFO tmp;
    memcpy(tmp.PortName, getLocalIP().toStdString().c_str(), sizeof(tmp.PortName));
    memcpy(tmp.PortDescription, QNetworkInterface().name().toStdString().c_str(), sizeof(tmp.PortDescription));

    m_CommPortNameInfo.append(tmp);
    return m_CommPortNameInfo;
}


/*******************************************************************
 * Class QTcpServer的基本操作:
 * 1.调用listen监听端口
 *   Call listen() to have the server listen for incoming connections.
 *   The newConnection() signal is then emitted each time a client connects to the server.
 *   newConnection: This signal is emitted every time a new connection is available.
 *
 * 2.连接newConnection信号,在槽函数里调用nextPendingConnection获取连接进来的socket
 *   Call nextPendingConnection() to accept the pending connection as a connected QTcpSocket.
 *   The function returns a pointer to a QTcpSocket in QAbstractSocket::ConnectedState
 *   that you can use for communicating with the client.
 *
 *   (If an error occurs, serverError() returns the type of error,
 *   and errorString() can be called to get a human readable description of what happened.)
 *
 * 3.取出建立好连接的的套接字QTcpSocket类,完成其readyRead和本文的connect连接
 *   When listening for connections, the address and port on which the server is listening
 *   are available as serverAddress() and serverPort().
 *
 *******************************************************************/
bool iCommTCP::_openPort(void *pPortParam)
{
    //根据UI的下发端口信息打开Socket服务端口
    m_Port = ((ST_NETWORK_PORT_PARAM*)pPortParam)->nPort;

    qDebug() << "1.打开listen监听端口：" << m_Port;
    bool Ret = m_tcpServer->listen(QHostAddress::AnyIPv4, m_Port); //QHostAddress::LocalHost | QHostAddress::Any
    if (!Ret)
    {
        //若出错，则输出错误信息
        qDebug() << "打开listen监听端口(" << m_Port << ")失败! " << m_tcpServer->errorString();
        return false;
    }

    //优化成保持一个连接的形式，且只保留最后一个连接
    qDebug() << "2.use (signal)newConnection() get nextPendingConnection()";
    conn3 = connect(m_tcpServer, &QTcpServer::newConnection, this, &iCommTCP::onNewConnection);

    return true;
}

//https://www.freesion.com/article/9018448983/

/**
 * @brief iCommTCP::onNewConnection
 * trigger when TCP Client to access...
 */
void iCommTCP::onNewConnection()
{
    qDebug("发现新客户端正在接入...  ====================");
    if(m_tcpSocket != NULL)
    {        
        qDebug("注意：客户端[%p]已经存在-状态[%d]! 正在注销此客户端...", m_tcpSocket, m_tcpSocket->state());
        //m_tcpSocket->disconnectFromHost(); //主动和客户端端口断开连接
        m_tcpSocket->close();
        m_tcpSocket = NULL;        
    }

    m_tcpSocket = m_tcpServer->nextPendingConnection();  //获取通信socket
    if (m_tcpSocket == NULL)
    {
        qDebug() << "客户端接入失败!";
        return;
    }

    QString ip   = m_tcpSocket->peerAddress().toString(); //获取对方的IP和端口
    qint16  port = m_tcpSocket->peerPort();

    qDebug() << "IP Address:" << m_tcpSocket->peerAddress() << "Port:" << port;
    qDebug() << tr("%1:%2:成功连接").arg(ip).arg(port);
    emit ePrintMessage(type_connectNotify, tr("%1:%2").arg(ip).arg(port));
    //qDebug() << tr("%1:%2:成功连接").arg(ip.split("::ffff:")[1]).arg(port);
    //emit ePrintMessage(type_connectNotify, tr("%1:%2").arg(ip.split("::ffff:")[1]).arg(port));

    qDebug() << "1.create connect 获取客户端的端口状态...";
    conn1 = connect(m_tcpSocket, &QTcpSocket::stateChanged, this, &iCommTCP::onSocketStateChange);
    onSocketStateChange(m_tcpSocket->state());

    qDebug() << "2.create connect 等待客户端请求帧...";
    //注意: 最后做建立connect()，避免出现野指针的错误; 程序会先从构造函数执行的，还没有执行到定义的QTcpSocket *tcpSocket指针
    connect(m_tcpSocket, &QTcpSocket::readyRead, this, &iCommTCP::onSocketReadyRead);
    //connect(m_tcpSocket, &QTcpSocket::readyRead, this, &iCommTCP::_readData);  //:->

    qDebug() << "3.create connect 等待客户端关闭...";
    conn2 = connect(m_tcpSocket, SIGNAL(disconnected()), this, SLOT(onClientDisconnected()));

    qDebug("客户端[%p]接入完成 ====================", m_tcpSocket);
}


/**
 * @brief iCommTCP::onSocketStateChange
 * @param socketState of TCP Client
 */
void iCommTCP::onSocketStateChange(QAbstractSocket::SocketState socketState)
{
    switch(socketState)  //socket状态的变化
    {
    case QAbstractSocket::UnconnectedState:
        qDebug("客户端状态变化 => UnconnectedState (The socket is not connected).");
        emit ePrintMessage(type_infoNotify, "TCP Client Unconnected State");
        break;
    case QAbstractSocket::HostLookupState:
        //套接字正在执行主机名查找
        qDebug("客户端状态变化 => HostLookupState (The socket is performing a host name lookup).");
        emit ePrintMessage(type_infoNotify, "TCP Client HostLookup State");
        break;
    case QAbstractSocket::ConnectingState:
        qDebug("客户端状态变化 => ConnectingState (The socket has started establishing a connection).");
        emit ePrintMessage(type_infoNotify, "TCP Client Connecting State");
        break;
    case QAbstractSocket::ConnectedState:
        qDebug("客户端状态变化 => ConnectedState (A connection is established).");
        emit ePrintMessage(type_infoNotify, "TCP Client Connected State");
        break;
    case QAbstractSocket::BoundState:
        qDebug("客户端状态变化 => BoundState (The socket is bound to an address and port).");
        emit ePrintMessage(type_infoNotify, "TCP Client Bound State");
        break;
    case QAbstractSocket::ClosingState:
        qDebug("客户端状态变化 => ClosingState (The socket is about to close).");
        emit ePrintMessage(type_infoNotify, "TCP Client Closing State");
        break;
    case QAbstractSocket::ListeningState:
        qDebug("客户端状态变化 => UnconnectedState(For internal use only).");
        emit ePrintMessage(type_infoNotify, "TCP Client Listening State");
    }
}


void iCommTCP::onClientDisconnected()
{
    qDebug("socket disconnect event triger!!!");
    if(m_tcpSocket != NULL)
    {
        qDebug("销毁客户端[%p]!", m_tcpSocket);
        m_tcpSocket->disconnectFromHost(); //主动和客户端端口断开连接
        m_tcpSocket->close();
        m_tcpSocket = NULL;
    }

    //notify ui clean client socket show
    emit ePrintMessage(type_disconnectNotify, "");
}


//read + dealwith + write
//接收到数据正确性校验
void iCommTCP::onSocketReadyRead()
{
    return this->_readData();
}

void iCommTCP::_readData()
{
    bool bRet = false;

    //1.read Data
    QByteArray recvbuf = m_tcpSocket->readAll();

    if(recvbuf.isEmpty() || recvbuf.length()!= RequestSize)
    {
        this->m_iLastErrCode = -err_recv_request_info_length;
        goto err;
    }
    qDebug() << "recv: " << recvbuf;

    //2.check Addr
    bRet = filterData(recvbuf);
    if (!bRet)
    {
        this->m_iLastErrCode = -err_recv_address;
        goto err;
    }

    //3.check CRC
    bRet = CheckReceiveData((BYTE*)recvbuf.data(), recvbuf.length());
    if (!bRet)
    {
        this->m_iLastErrCode = -err_recv_crc_check;
        goto err;
    }

    emit ePrintData(recvbuf, "Recv");
    emit eGetData(recvbuf);

    return;

err:
    qDebug() << "m_iLastErrCode: " << this->m_iLastErrCode;
    return;
}

/*
如：起始地址是0x0000，寄存器数量是 0x0003
00 01 00 00 00 06 01 03 00 00 00 03

00 01
00 00
00 06
01
03
00 00 00 03

回：数据长度为0x06，第一个寄存器的数据为0x21，其余为0x00
00 01 00 00 00 09 01 03 06 00 21 00 00 00 00

如：向地址是0x0000的寄存器写入数据0x000A
00 01 00 00 00 06 01 06 00 00 00 0A

00 01
00 00
00 06
01
06
00 00
00 0A

回：写入成功
00 01 00 00 00 06 01 06 00 00 00 0A
*/


bool iCommTCP::filterData(QByteArray buf)
{
    int recvAddr = (int)(unsigned char)(buf.at(RequestAddr));
    return (recvAddr > QModbusPdu::Invalid)? true : false;
}

bool iCommTCP::CheckReceiveData(BYTE *pRecv, INT32 iRecv)
{
    register const int REQ_MASK = 0;
    register const int DATA_LEN = 6;

    Q_ASSERT((pRecv != NULL) && (iRecv != 0));

    unsigned char ucidentifyHigh = pRecv[2]; //协议标识符
    unsigned char ucidentifyLow  = pRecv[3];
    quint16  identify = (ucidentifyHigh << 8) | ucidentifyLow;
    qDebug() << "协议标识符: " << identify;
    if(identify != REQ_MASK)
    {
        return false;
    }

    unsigned char ucreqHigh = pRecv[4]; //报文长度
    unsigned char ucreqLow  = pRecv[5];
    quint16  iLen = (ucreqHigh << 8) | ucreqLow;
    qDebug() << "报文长度: " << iLen;
    if(iLen != DATA_LEN)
    {
        return false;
    }

    return true;
}


/**
 * @brief iCommTCP::_writeData
 * @param buf Send the buffer to TCP Cliennt (upper systems)
 */
void iCommTCP::_writeData(QByteArray buf)
{    
    if(m_tcpSocket == NULL || buf == NULL)
    {
        return;
    }

    qDebug() << buf;
    m_tcpSocket->write(buf);

    emit ePrintData(buf, "Send");
}


void iCommTCP::_closePort()
{
    disconnect(conn1);
    disconnect(conn2);
    disconnect(conn3);

    if(m_tcpSocket)
    {
        qDebug("关闭客户端[%p]-状态[%d]", m_tcpSocket, m_tcpSocket->state());
        m_tcpSocket->disconnectFromHost(); //主动和客户端端口断开连接
        m_tcpSocket->close();
        m_tcpSocket = NULL;       
    }

    if(m_tcpServer != NULL)
    {
        m_tcpServer->close();
    }

    qDebug() << "finished close Port";
}



