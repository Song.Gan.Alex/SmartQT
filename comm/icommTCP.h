#ifndef COMMTCP_H
#define COMMTCP_H

#include <QTcpSocket>
#include <QTcpServer>
#include <QNetworkInterface>
#include <QMessageBox>

#include <QModbusTcpServer>
#include <QModbusServer>
#include <QModbusPdu>

#include "icommBase.h"

//Modbus-Tcp Server
class iCommTCP : public iCommBase
{    
    Q_OBJECT

public:
    explicit iCommTCP(qint16 listenPort);
    ~iCommTCP();

    QVector<ST_COMM_PORT_INFO> _getPortInfo();

    bool _openPort(void *pPortParam);
    void _closePort();
    void _writeData(QByteArray buf);

private:
    qint16 m_Port;
    int m_iLastErrCode;

    //Port端口信息
    QVector<ST_COMM_PORT_INFO> m_CommPortNameInfo;

    QTcpServer *m_tcpServer; //TCP服务器
    QTcpSocket *m_tcpSocket; //TCP通信Socket

    QString getLocalIP();  //获取本地IP
    bool filterData(QByteArray buf);
    bool CheckReceiveData(BYTE *pRecv, INT32 iRecv);

    QMetaObject::Connection conn1;
    QMetaObject::Connection conn2;
    QMetaObject::Connection conn3;

public slots:
    void onNewConnection();  //newConnection()信号槽函数
    void onClientDisconnected();  //disconnected()信号槽函数

    void onSocketStateChange(QAbstractSocket::SocketState socketState);  //socketStateChange()信号槽函数，socket状态的变化
    void onSocketReadyRead();  //readyRead()信号槽函数，读取数据
    void _readData(); //get->emit->(pMgr->read->dealwith->write)->set

};


#endif // COMMTCP_H
